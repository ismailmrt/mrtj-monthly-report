I want to build an automation feature just like in monday.com platform, below i got the specification for the feature and my current code implementation. i need you to help me in suggesting what's the best approach especially concering to performance. provide detailed step-by-step instructions.

currently i have this code implemented as proof of concept

the automation is however applied specifically to each day rather to the whole board found in monday.com implementation

can you help me how can we achieve it dynamically like in monday. we can choose conditions based on the columns that are available in that board. then we can choose actions from what's available related to that row (for now stick to move item between days, and update the item value)

then for trigger detection, condition evaluation and action execution i want it all happens asynchronously meaning that it will be handled by another node service (create a new service) and to notify that service we need to use something like RabbitMQ. what do you think of this?

in the trigger detection all we need to do is just to pass all of the relevant data such as update, query, dayId, doc, newValue, columnId, previousValue

then the worker service will try to check if the trigger matches with the current active automations for that particular dayId

then it will check whether the condition is matched, if it is then execute (send notification email is also part of this action, user can choose list of actions that needed to be executed). once done it must notify that the job was finished then notify it.

the worker service is basically will just try to execute available board and row service. however since the worker service is in different service? how can they access it? should we create the same service there or communicate via gRPC? well suggest me for the best performance

i want the rabbitmq publishing implemented in the mongoose hook middleware? rather than in service because i want for the service to only do what they need to do (CRUD) and not publishing or doing other than logging. am i correct?

## current automation model schema

```js
import mongoose from "mongoose";
const { Schema } = mongoose;
import toJSON from "../plugins/toJSON.plugin.js";
import paginate from "../plugins/paginate.plugin.js";

const MODEL_NAME = "Automation";

/**
 * Schema definition for automation conditions.
 * @typedef {Object} AutomationCondition
 * @property {String} type - The type of event that triggers the condition.
 * @property {Schema.Types.Mixed} params - Specific conditions based on the event type.
 */
const conditionSchema = new Schema(
  {
    type: {
      type: String,
      required: true,
      enum: ["row_created", "row_updated", "cv_updated"],
    },
    params: {
      type: Schema.Types.Mixed,
      required: true,
    },
  },
  { _id: false },
);

/**
 * Schema definition for automation actions.
 * @typedef {Object} AutomationAction
 * @property {String} type - The type of action to perform.
 * @property {Schema.Types.Mixed} params - Parameters required for the action.
 */
const actionSchema = new Schema(
  {
    type: {
      type: String,
      required: true,
      enum: [
        "create_days",
        "update_days",
        "delete_days",
        "move_row",
        "create_row",
        "update_cv",
        "delete_row",
      ],
    },
    params: {
      type: Schema.Types.Mixed,
      required: true,
    },
  },
  { _id: false },
);

/**
 * Main schema for automations.
 * @typedef {Object} AutomationSchema
 * @property {Number} userId - User ID who created the automation.
 * @property {Schema.Types.ObjectId} boardId - Board ID this automation belongs to.
 * @property {Schema.Types.ObjectId} dayId - Day ID this automation belongs to.
 * @property {Number} state - State of the automation (0: active, 1: disabled, 2: archived).
 * @property {AutomationCondition[]} conditions - Array of conditions for the automation.
 * @property {AutomationAction[]} actions - Array of actions to perform.
 */
const schema = new Schema(
  {
    userId: {
      type: Number,
      required: true,
    },
    boardId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    dayId: {
      type: Number,
      required: true,
    },
    state: {
      type: Number,
      required: true,
      default: 0,
      enum: [0, 1, 2], // 0: active, 1: disabled, 2: archived
    },
    conditions: {
      type: [conditionSchema],
      validate: {
        validator: function (conditions) {
          return conditions.length > 0 && conditions.length <= 64;
        },
        message: "Conditions array must have between 1 and 64 elements",
      },
    }, // fist condition is always a trigger
    actions: {
      type: [actionSchema],
      validate: {
        validator: function (actions) {
          return actions.length > 0 && actions.length <= 8;
        },
        message: "Actions array must have between 1 and 8 elements",
      },
    },
    companyId: { type: Number, required: true },
  },
  {
    timestamps: true,
  },
);

// Indexes for efficient querying
schema.index({ companyId: 1 });
schema.index({ boardId: 1, state: 1 });
schema.index({ dayId: 1, state: 1 });
schema.index({ userId: 1 });

// add plugin
schema.plugin(toJSON);
schema.plugin(paginate);

/**
 * Pre-save middleware to validate conditions and actions.
 * @function
 * @param {Function} next - Callback to proceed to the next middleware.
 */
schema.pre("save", function (next) {
  // Ensure first condition is a trigger
  if (this.conditions.length === 0) {
    return next(new Error("At least one condition (trigger) is required"));
  }

  // Validate that actions array is not empty
  if (this.actions.length === 0) {
    return next(new Error("At least one action is required"));
  }

  next();
});

/**
 * Static method to find active automations for a board
 * @param {ObjectId} boardId - The board ID to find automations for
 * @returns {Promise<Array>} Array of active automations
 */
schema.statics.findActiveByBoard = function (boardId) {
  return this.find({
    boardId,
    state: 0,
  }).exec();
};

/**
 * Static method to find automations by event type
 * @param {String} eventType - The event type to find automations for
 * @returns {Promise<Array>} Array of matching automations
 */
schema.statics.findByEventType = function (eventType) {
  return this.find({
    state: 0,
    "conditions.event": eventType,
  }).exec();
};

const Automation = mongoose.model(MODEL_NAME, schema);

export default Automation;
```

## current board model schema

```js
import mongoose from "mongoose";
import { COLUMN_TYPES, ROLE_TYPES } from "../../utils/constants.js";
import toJSON from "../plugins/toJSON.plugin.js";
import paginate from "../plugins/paginate.plugin.js";

const { Schema } = mongoose;

const MODEL_NAME = "Board";

/**
 * Schema for representing a board member.
 * @typedef {Object} MemberReference
 * @property {Number} _id - The unique identifier for the member.
 * @property {Number} role - The role of the member.
 * @property {Date} joinedAt - The date when the member joined the board.
 */
const memberReferenceSchema = new Schema(
  {
    _id: { type: Number, required: true },
    role: {
      type: Number,
      enum: Object.values(ROLE_TYPES),
      required: true,
      default: ROLE_TYPES.VIEWER,
    },
    joinedAt: { type: Date, required: true, default: Date.now },
  },
  { _id: false },
);

/**
 * Schema for representing a day's configuration within a board.
 * @typedef {Object} DayConfigReference
 * @property {String} label - The label for the day configuration.
 * @property {String} [color] - The optional color for the day.
 * @property {Number} state - The state of the day (0 for active, 1 for archived).
 */
const dayConfigReferenceSchema = new Schema({
  _id: { type: Number, required: true },
  label: { type: String, required: true },
  color: { type: String, required: false },
  state: { type: Number, required: true, default: 0 },
});

/**
 * Schema for representing a column in the board.
 * @typedef {Object} Column
 * @property {String} label - The label for the column.
 * @property {String} [description] - The optional description for the column.
 * @property {String} type - The type of the column.
 * @property {Object} config - Configuration object for the column.
 * @property {Number} state - The state of the column (0 for active, 1 for archived).
 * @property {Number} position - The position of the column in the board.
 */
const columnSchema = new Schema({
  label: { type: String, required: true },
  type: {
    type: String,
    required: true,
    enum: {
      values: Object.keys(COLUMN_TYPES),
      message: "{VALUE} is not a valid column type",
    },
  },
  description: { type: String, required: false },
  config: { type: Object, required: true },
  state: { type: Number, required: true, default: 0 },
  position: { type: Number, required: true },
});

/**
 * Schema for representing a board.
 * @typedef {Object} Board
 * @property {String} name - The name of the board.
 * @property {String} [description] - Optional description of the board.
 * @property {Number} privacy - Privacy setting of the board (0 for closed, 1 for open).
 * @property {String} [itemTerminology="item"] - Optional terminology for items in the board.
 * @property {Number} state - The state of the board (0 for active, 1 for archived).
 * @property {Number} companyId - The ID of the company associated with the board.
 * @property {String} workspaceId - The ID of the workspace associated with the board.
 * @property {Number} creatorId - The user ID of the creator of the board.
 * @property {MemberReference[]} members - List of members associated with the board.
 * @property {TeamReference[]} teams - List of teams associated with the board.
 * @property {DayConfigReference[]} daysConfig - Configuration for days within the board.
 * @property {Column[]} mainColumns - List of main columns in the board.
 * @property {Column[]} subColumns - List of sub-columns in the board.
 */
const schema = new Schema(
  {
    name: { type: String, required: true },
    description: { type: String, required: false, default: null },
    privacy: { type: Number, required: true, default: 0 },
    itemTerminology: { type: String, required: false, default: "Item" },
    state: { type: Number, required: true, default: 0 },
    companyId: { type: Number, required: true },
    workspaceId: { type: String, required: true },
    creatorId: { type: Number, required: true },
    members: [memberReferenceSchema],
    daysConfig: {
      type: [dayConfigReferenceSchema],
      default: function () {
        return [
          { _id: 0, label: "Backlog", color: "#00ff00" },
          { _id: 1, label: "Sprint", color: "#ffff00" },
          { _id: 2, label: "Review", color: "#ff0000" },
        ];
      },
    },
    mainColumns: {
      type: [columnSchema],
      default: function () {
        return [
          { ...COLUMN_TYPES.TEXT, position: 0, type: "TEXT" },
          { ...COLUMN_TYPES.PEOPLE, position: 1, type: "PEOPLE" },
          { ...COLUMN_TYPES.STATUS, position: 2, type: "STATUS" },
        ];
      },
    },
    subColumns: {
      type: [columnSchema],
      default: function () {
        return [
          { ...COLUMN_TYPES.TEXT, position: 0, type: "TEXT" },
          { ...COLUMN_TYPES.STATUS, position: 1, type: "STATUS" },
        ];
      },
    },
  },
  { timestamps: true },
);

schema.index({ companyId: 1, state: 1 });
schema.index({ workspaceId: 1, state: 1 });
schema.index({ "members._id": 1, state: 1 });

// add plugin that converts mongoose to json
schema.plugin(toJSON);
schema.plugin(paginate);

//TODO: add statics

/**
 * Model for representing a board in MongoDB.
 * @type {mongoose.Model<Board>}
 */
const Board = mongoose.model(MODEL_NAME, schema);
export default Board;

export const COLUMN_TYPES = {
  CHECKBOX: {
    label: "Checkbox",
    description: "Simple true or false value",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.BOOLEAN,
      defaultValue: false,
    },
  },
  CURRENCY: {
    label: "Currency",
    description: "Monetary value with a specific currency",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.DOUBLE,
      defaultValue: 0.0,
      defaultPrefix: "Rp.",
    },
  },
  DATE: {
    label: "Date",
    description: "Represents a specific date (and potentially time)",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.DATE,
      defaultValue: new Date(), //TODO: check impl
      defaultFormat: "DD/MM/YYYY",
      defaultTimeFormat: "HH:mm",
    },
  },
  EMAIL: {
    label: "Email",
    description:
      "Allows to attach an email address to a row and send emails to that contact with a single click",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.STRING,
      defaultValue: "",
      defaultInteraction: "mailto:{value}",
    },
  },
  FILE: {
    label: "File",
    description: "Contains files attached to a board",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.OBJECT,
      defaultValue: null, // Represents a file object
    },
  },
  LINK: {
    label: "Link",
    description: "URL or hyperlink",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.STRING,
      defaultValue: "",
      defaultInteraction: 'href="{value}"',
    },
  },
  LOCATION: {
    label: "Location",
    description: "Stores a location with longitude and latitude precision",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.OBJECT,
      defaultValue: { lat: null, lng: null },
    },
  },
  NUMBERS: {
    label: "Numbers",
    description: "Holds number values (Integer)",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.INT,
      defaultValue: 0,
    },
  },
  PEOPLE: {
    label: "People",
    description: "Represents a person(s) who is assigned to the row",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.ARRAY, // refer to array of mysql id
      defaultValue: [], // Array of user IDs or objects
    },
  },
  PERCENT: {
    label: "Percent",
    description: "Percentage value",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.DOUBLE,
      defaultValue: 0.0,
      suffix: "%",
    },
  },
  STATUS: {
    label: "Status",
    description: "Represents a label designation for your row(s)",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.ARRAY,
      defaultValue: "Backlog", // Or other default status
      options: [
        {
          name: " ",
          color: "#cbcbcc",
        },
        {
          name: "Backlog",
          color: "#ff6666",
        },
        {
          name: "To Do",
          color: "#3f51b5",
        },
        {
          name: "In Progress",
          color: "#ffc107",
        },
        {
          name: "Done",
          color: "#8bc34a",
        },
      ], // Example status options
    },
  },
  TAGS: {
    label: "Tags",
    description:
      "Holds words and/or phrases that helps to group rows using these keywords.",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.ARRAY,
      defaultValue: [], // Array of tag strings
    },
  },
  TEXT: {
    label: "Text",
    description: "Simple text holder",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.STRING,
      defaultValue: "",
    },
  },
  TIMELINE: {
    label: "Timeline",
    description: "Timeline with start and end dates",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.OBJECT,
      defaultValue: { start: null, end: null }, // Represents start and end dates
    },
  },
  APPROVAL: {
    label: "Approval",
    description: "Represents a person(s) who should approve the row",
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.OBJECT,
      defaultValue: {
        method: 0, // 0 = serial, 1 = parallel
        approvers: [],
      },
    },
  },
};
```

## current row model schema

```js
import mongoose from "mongoose";
import { BSON } from "bson";
import toJSON from "../plugins/toJSON.plugin.js";
import paginate from "../plugins/paginate.plugin.js";
import rowService from "../../services/monday/row.service.js";
import { sendEmail } from "../../utils/mailer.js";

const { Schema } = mongoose;

const MODEL_NAME = "Row";
const COLLECTION_NAME = "martiday";

/**
 * Schema definition for column values in rows.
 * @typedef {Object} ColumnValueReference
 * @property {Schema.Types.ObjectId} columnId - MongoDB ID of the column (main or sub).
 * @property {Schema.Types.Mixed} value - The value associated with the column.
 * @property {Schema.Types.Mixed} [previousValue] - The previous value before the update.
 */
const columnValueReferenceSchema = new Schema(
  {
    value: { type: Schema.Types.Mixed, required: true },
    previousValue: { type: Schema.Types.Mixed, required: false, default: null },
  },
  { timestamps: true },
);

/**
 * Schema definition for child rows within a parent row.
 * @typedef {Object} RowChildReference
 * @property {String} name - Name of the child row.
 * @property {Number} creatorId - User ID of the creator.
 * @property {ColumnValueReference[]} columnValues - Array of column values in the child row.
 */
const rowChildReferenceSchema = new Schema(
  {
    name: { type: String, required: true },
    creatorId: { type: Number, required: true },
    columnValues: [columnValueReferenceSchema],
  },
  { timestamps: true },
);

/**
 * Main schema for rows, containing child rows and column values.
 * @typedef {Object} RowSchema
 * @property {String} name - Name of the row.
 * @property {Schema.Types.ObjectId} boardId - MongoDB ID of the board to which the row belongs.
 * @property {Number} companyId - Company ID associated with the row, referenced from MySQL DB.
 * @property {Number} creatorId - User ID of the creator, referenced from MySQL DB.
 * @property {ColumnValueReference[]} columnValues - Array of column values in the row.
 * @property {RowChildReference[]} children - Array of child rows.
 */
const schema = new Schema(
  {
    name: { type: String, required: true },
    boardId: { type: Schema.ObjectId, required: true },
    companyId: { type: Number, required: true },
    creatorId: { type: Number, required: true },
    columnValues: [columnValueReferenceSchema],
    children: [rowChildReferenceSchema],
  },
  { timestamps: true },
);

// Indexes for efficient querying
schema.index({ boardId: 1 });
schema.index({ companyId: 1 });

// add plugin that converts mongoose to json
schema.plugin(toJSON);
schema.plugin(paginate);

/**
 * Pre-save middleware to check document size before saving.
 * To handle the probability of this document being too large, since we includes children row in the parent document.
 * Previous design: https://gitlab.com/ismailmrt/mrtj-monthly-report/-/blob/main/analysis/martidocs-monday.md
 * @function
 * @param {Function} next - Callback to proceed to the next middleware.
 * @throws {Error} Throws an error if document size exceeds 16 MB.
 */
schema.pre("save", function (next) {
  const docSize = BSON.calculateObjectSize(this.toObject());

  if (docSize >= 16 * 1024 * 1024) {
    // 16 MB
    return next(new Error("Document size exceeds the 16 MB limit"));
  }

  next();
});

/**
 * Pre-save middleware for row schema.
 * @function
 * @param {Function} next - Callback to proceed to the next middleware.
 */
schema.pre("findOneAndUpdate", async function (next) {
  // NOTE: proof of concept of automations feature
  // user do something
  // detected by the hook middleware
  // sent to rabbitmq
  // validate whether it's a valid automation
  // checks the conditions
  // if it's true then execute the actions
  // and send the result to rabbitmq
  const currentCollectionName = this.model.collection.name;
  console.log("Collection:", currentCollectionName);
  console.log("Update Query:", this.getQuery());
  console.log("Update Body:", this.getUpdate());

  try {
    const update = this.getUpdate();
    const doc = await this.model.findOne(this.getQuery());
    console.log("Current Document:", doc);

    if (update.$set && update.$set["columnValues.$.value"] === "To Do" && doc) {
      const fromDayId = parseInt(currentCollectionName.split("_").pop());
      if (fromDayId !== 1) {
        const toDayId = 1;
        setImmediate(async () => {
          try {
            //TODO: detect race conditions
            //- add breakpoint for endless loop, configurable via env (if the loop is already at the n-th then break)
            // action sample
            // action 1: move row
            await rowService.moveRow(doc._id, fromDayId, toDayId);

            // action 2: send email
            await sendEmail(
              "martidocs.reviewer@yopmail.com",
              "automation test",
              `
      <div style="font-family: Arial, sans-serif; line-height: 1.6; color: #333; max-width: 600px; margin: 0 auto; padding: 20px; border: 1px solid #e0e0e0; border-radius: 10px;">
        <h2 style="background-color: #4CAF50; color: white; padding: 10px 20px; border-radius: 10px 10px 0 0; text-align: center;">Automation Notification🔥🔥🔥</h2>
        <p style="font-size: 16px;">Hello,</p>
        <p style="font-size: 16px;">
          Automation executed.
        </p>
        <p style="font-size: 16px;">
          Row ${doc._id} moved from day ${fromDayId} to day ${toDayId}.
        </p>
        <p style="font-size: 16px;">
          Best regards,<br>
          The Martidocs Team
        </p>
      </div>
    `,
            );
            console.log(
              `Row ${doc._id} moved from day ${fromDayId} to day ${toDayId}`,
            );
          } catch (moveError) {
            console.error("Error moving row:", moveError);
          }
        });
      }
    }

    next();
  } catch (error) {
    next(error);
  }
});

/**
 * Creates a Mongoose model for rows within a specific day.
 * @function
 * @param {Number} dayId - Identifier for the day, used to determine the collection name.
 * @returns {mongoose.Model} The Mongoose model for the specified day.
 */
const Row = function (dayId) {
  return mongoose.model(MODEL_NAME, schema, `${COLLECTION_NAME}_${dayId}`);
};

export default Row;
```

## current row.service.js implementation

```js
import Row from "../../models/mongo/row.model.js";
import ApiError from "../../utils/apiError.js";
import { HTTP_STATUS } from "../../utils/httpStatus.js";
import { logger } from "../../utils/logger.js";
import { toObjectId } from "../../utils/mongooseHelpers.js";
import mongoose from "mongoose";
import path from "path";
import { promises as fs } from "fs";
import { createReadStream } from "fs";

/**
 * Query for rows
 * @param {String} boardId - Board ID
 * @param {Number} dayId - Day ID
 * @returns {Promise<Array>}
 */
const queryRows = async (boardId, dayId) => {
  try {
    const RowModel = Row(dayId);
    const filter = { boardId: toObjectId(boardId) };

    const data = await RowModel.find(filter)
      .sort({ createdAt: -1 }) // Sort by createdAt descending
      .select({
        name: 1,
        columnValues: 1,
        children: 1,
      });

    return data;
  } catch (error) {
    logger.error(error);
    throw error;
  }
};

/**
 * Create a new row
 * @param {Object} rowData
 * @param {Object} user
 * @returns {Promise<Row>}
 */
const createRow = async (rowData, user) => {
  try {
    const { name, boardId, dayId } = rowData;
    const RowModel = Row(dayId);

    const row = await RowModel.create({
      name,
      boardId: toObjectId(boardId),
      companyId: user.companyId,
      creatorId: user.id,
      columnValues: [],
      children: [],
    });

    return row;
  } catch (error) {
    logger.error(error);
    throw error;
  }
};

/**
 * Update a row
 * @param {String} rowId
 * @param {Number} dayId
 * @param {Object} updateData
 * @param {Number} userId
 * @returns {Promise<Row>}
 */
const updateRow = async (rowId, dayId, updateData, userId) => {
  try {
    const RowModel = Row(dayId);
    const row = await RowModel.findOne({
      _id: rowId,
    });

    if (!row) {
      throw new ApiError(
        HTTP_STATUS.NOT_FOUND,
        "Row not found or unauthorized",
      );
    }

    Object.assign(row, updateData);
    await row.save();
    return row;
  } catch (error) {
    logger.error(error);
    throw error;
  }
};

/**
 * Delete a row
 * @param {String} rowId
 * @param {Number} dayId
 * @param {Number} userId
 * @returns {Promise<Row>}
 */
const deleteRow = async (rowId, dayId, userId) => {
  try {
    const RowModel = Row(dayId);
    const row = await RowModel.findOneAndDelete({
      _id: rowId,
    });

    if (!row) {
      throw new ApiError(
        HTTP_STATUS.NOT_FOUND,
        "Row not found or unauthorized",
      );
    }

    return row;
  } catch (error) {
    logger.error(error);
    throw error;
  }
};

/**
 * Move a row between days
 * @param {String} rowId
 * @param {Number} fromDayId
 * @param {Number} toDayId
 * @param {Number} userId
 * @returns {Promise<Row>}
 */
const moveRow = async (rowId, fromDayId, toDayId, userId) => {
  const session = await mongoose.startSession();
  try {
    session.startTransaction();

    const SourceModel = Row(fromDayId);
    const TargetModel = Row(toDayId);

    const sourceRow = await SourceModel.findOne({
      _id: rowId,
    });

    if (!sourceRow) {
      // data sudah berhasil diubah oleh user lain
      throw new ApiError(
        HTTP_STATUS.NOT_FOUND,
        "Row not found or unauthorized",
      );
    }

    const newRow = new TargetModel({
      _id: sourceRow._id,
      ...sourceRow.toObject(),
      createdAt: new Date(),
      updatedAt: new Date(),
    });

    await newRow.save({ session });
    await SourceModel.findOneAndDelete({ _id: rowId }, { session });

    await session.commitTransaction();
    return newRow;
  } catch (error) {
    await session.abortTransaction();
    logger.error(error);
    throw error;
  } finally {
    session.endSession();
  }
};

/**
 * Update or add column value
 * @param {String} rowId
 * @param {String} columnId
 * @param {Number} dayId
 * @param {*} value
 * @param {Number} userId
 * @returns {Promise<Row>}
 */
const updateColumnValue = async (rowId, columnId, dayId, value, userId) => {
  try {
    const RowModel = Row(dayId);
    const row = await RowModel.findOne({
      _id: rowId,
    });

    if (!row) {
      throw new ApiError(
        HTTP_STATUS.NOT_FOUND,
        "Row not found or unauthorized",
      );
    }

    const columnValue = row.columnValues.id(columnId);
    if (columnValue) {
      // Update existing column
      const updateFields = {
        "columnValues.$.value": value,
        "columnValues.$.previousValue": columnValue.value,
      };

      return await RowModel.findOneAndUpdate(
        { _id: rowId, "columnValues._id": columnId },
        { $set: updateFields },
        { new: true },
      );
    } else {
      // Add new column
      return await RowModel.findOneAndUpdate(
        { _id: rowId },
        {
          $push: {
            columnValues: {
              _id: toObjectId(columnId),
              value,
              previousValue: null,
            },
          },
        },
        { new: true },
      );
    }
  } catch (error) {
    logger.error(error);
    throw error;
  }
};

/**
 * Upload file for column value
 * @param {String} rowId
 * @param {String} columnId
 * @param {Number} dayId
 * @param {Object} file
 * @param {Number} userId
 * @returns {Promise<Row>}
 */
const uploadColumnFile = async (rowId, columnId, dayId, file, userId) => {
  try {
    if (!file) {
      throw new ApiError(HTTP_STATUS.BAD_REQUEST, "No file uploaded");
    }

    const RowModel = Row(dayId);
    const row = await RowModel.findOne({
      _id: rowId,
    });

    if (!row) {
      await fs.unlink(file.path);
      throw new ApiError(
        HTTP_STATUS.NOT_FOUND,
        "Row not found or unauthorized",
      );
    }

    const fileInfo = {
      originalName: file.originalname,
      path: file.path,
      mimetype: file.mimetype,
      size: file.size,
    };

    const columnValue = row.columnValues.id(columnId);
    if (columnValue) {
      // Delete old file if exists
      if (columnValue.value?.path) {
        try {
          await fs.unlink(columnValue.value.path);
        } catch (err) {
          logger.warn(`Failed to delete old file: ${err}`);
        }
      }

      return await RowModel.findOneAndUpdate(
        { _id: rowId, "columnValues._id": columnId },
        {
          $set: {
            "columnValues.$.value": fileInfo,
            "columnValues.$.previousValue": columnValue.value,
          },
        },
        { new: true },
      );
    } else {
      return await RowModel.findOneAndUpdate(
        { _id: rowId },
        {
          $push: {
            columnValues: {
              _id: toObjectId(columnId),
              value: fileInfo,
              previousValue: null,
            },
          },
        },
        { new: true },
      );
    }
  } catch (error) {
    if (file?.path) {
      try {
        await fs.unlink(file.path);
      } catch (err) {
        logger.error(`Failed to delete file after error: ${err}`);
      }
    }
    logger.error(error);
    throw error;
  }
};

/**
 * Get file download details
 * @param {String} filePath - Path to the file
 * @returns {Promise<Object>} File details including path, name, mime type
 */
const downloadColumnFile = async (filePath) => {
  try {
    if (!filePath) {
      throw new ApiError(HTTP_STATUS.BAD_REQUEST, "File path is required");
    }

    // Get the absolute path
    const absolutePath = path.resolve(filePath);

    // Security check to prevent directory traversal
    if (!absolutePath.startsWith(path.resolve("uploads"))) {
      throw new ApiError(HTTP_STATUS.FORBIDDEN, "Invalid file path");
    }

    // Check if file exists
    try {
      await fs.access(absolutePath);
    } catch (error) {
      throw new ApiError(HTTP_STATUS.NOT_FOUND, "File not found");
    }

    // Get file name and extension
    const fileName = path.basename(absolutePath);
    const fileExt = path.extname(fileName).toLowerCase();

    // Mime type mapping
    const mimeTypes = {
      ".pdf": "application/pdf",
      ".jpg": "image/jpeg",
      ".jpeg": "image/jpeg",
      ".png": "image/png",
      ".gif": "image/gif",
      ".doc": "application/msword",
      ".docx":
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
      ".xls": "application/vnd.ms-excel",
      ".xlsx":
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ".ppt": "application/vnd.ms-powerpoint",
      ".pptx":
        "application/vnd.openxmlformats-officedocument.presentationml.presentation",
      ".txt": "text/plain",
      ".zip": "application/zip",
      ".rar": "application/x-rar-compressed",
      ".7z": "application/x-7z-compressed",
    };

    return {
      absolutePath,
      fileName,
      contentType: mimeTypes[fileExt] || "application/octet-stream",
    };
  } catch (error) {
    logger.error(error);
    throw error;
  }
};

/**
 * Validate and prepare file for streaming
 * @param {String} filePath - Path to the file
 * @returns {Promise<Object>} Stream and file details
 */
const prepareFileStream = async (filePath) => {
  try {
    const fileDetails = await downloadColumnFile(filePath);
    const fileStream = createReadStream(fileDetails.absolutePath);

    return {
      stream: fileStream,
      ...fileDetails,
    };
  } catch (error) {
    logger.error(error);
    throw error;
  }
};

/**
 * Add column value to all rows in a board
 */
const addColumnToAllRows = async (boardId, columnId, dayId, value) => {
  try {
    const RowModel = Row(dayId);

    const result = await RowModel.updateMany(
      {
        boardId: toObjectId(boardId),
        "columnValues._id": { $ne: columnId },
      },
      {
        $push: {
          columnValues: {
            _id: toObjectId(columnId),
            value,
            previousValue: null,
          },
        },
      },
    );

    if (!result.matchedCount) {
      throw new ApiError(HTTP_STATUS.NOT_FOUND, "No rows found for this board");
    }

    return result;
  } catch (error) {
    logger.error(error);
    throw error;
  }
};

/**
 * Remove column from all rows in a board
 */
const removeColumnFromAllRows = async (boardId, columnId, dayId) => {
  try {
    const RowModel = Row(dayId);

    const result = await RowModel.updateMany(
      {
        boardId: toObjectId(boardId),
      },
      {
        $pull: {
          columnValues: {
            _id: toObjectId(columnId),
          },
        },
      },
    );

    if (!result.matchedCount) {
      throw new ApiError(HTTP_STATUS.NOT_FOUND, "No rows found for this board");
    }

    return result;
  } catch (error) {
    logger.error(error);
    throw error;
  }
};

export default {
  queryRows,
  createRow,
  updateRow,
  deleteRow,
  moveRow,
  updateColumnValue,
  uploadColumnFile,
  downloadColumnFile,
  prepareFileStream,
  addColumnToAllRows,
  removeColumnFromAllRows,
};
```

### Event Types

#### Value Based

- row_created
  - item created
  - item moved to another day
- row_updated
  - item name changed
  - thread changed
- cv_updated
  - status changes to something
  - column changes
  - button clicked
  - person assigned
  - status changes from someting to something
  - email changes
  - subitem status changes

#### Time Based

- date_arrives (on time, or H-/+days before + time)
- time_period (daily, weekly, monthly)

### Conditions: Rules that determine whether an action should be executed.

- column is empty (daysSerevice: getColumnValue)
- item is in a group (daysService: getItem)
- status is something (daysService: getColumnValue)
- dropdown meets condition (daysService: getColumnValue)
- number meets condition (daysService: getColumnValue)
- person is someone (daysService: getColumnValue)

## the first condition is always a trigger

## Actions: Tasks performed in response to a trigger and a matching condition.

### Action Types

- move item to days (daysService: deleteRow & createRow)
- notify (notificationService: sendGmail, sendWhatsapp)
- change status (days service: updateColumnValue)
- create subitem (days service: createSubItem)
- set date (days service: updateColumnValue)
- create item (days service: createItem)
- duplicate item (days service: createItem)
- archive item (days service: updateItem)
- delete item (days service: deleteItem)
- create thread (days service: createThread)
- clear column (days service: updateColumnValue)
- assign specified persion (days service: updateColumnValue)
- assign the item creator (days service: updateColumnValue)
- assign team member (days service: updateColumnValue)
- clear assignees (days service: updateColumnValue)
- push date to the next day (days service: updateColumnValue)
- start time tracking (days service: updateColumnValue)
- stop time tracking (days service: updateColumnValue)
- set hour to current time (days service: updateColumnValue)
- set number to current time (days service: updateColumnValue)
- increase/decrease number by x (days service: updateColumnValue)
- create days (board service: createDaysConfig)
- duplicate days (board service: createDaysConfig)

--

## Rules/Automations: Combination of triggers, conditions, and actions.

Simple Rules: A single trigger and action.
Complex Rules: Multiple triggers, conditions, and actions linked together.

### Time-Based Triggers

use a job scheduler (like node-cron) to periodically check for automations that need to be executed based on time conditions.

- Identify which automations are eligible for that day
- Schedule pending job to execute (based on the provided time)

### Implementation Considerations

- Asynchronous Tasks: Use a task queue or background job processor to handle time-consuming actions.
- Error Handling: Implement robust error handling to prevent automation failures.
- Testing: Thoroughly test automations to ensure they work as expected.
