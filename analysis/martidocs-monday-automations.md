# **PRD Outline for Re-engineering Monday Automation Work Management**

## Triggers: Events that initiate an automation.

### Event Types

#### Value Based

- row_created
  - item created
  - item moved to another day
- row_updated
  - item name changed
  - thread changed
- cv_updated
  - status changes to something
  - column changes
  - button clicked
  - person assigned
  - status changes from someting to something
  - email changes
  - subitem status changes

##### Implementation

```javascript
//rows
schema.pre("save", async function (next) {
  if (this.isNew) {
    // Trigger automation related to row creation
    const triggerEvent = {
      type: "row_created",
      boardId: this.boardId,
      dayId: this.dayId,
      row: this, // Pass the newly created row
    };
    await AutomationService.executeAutomation(triggerEvent);
  }
  next();
});

//columns
schema.pre("save", async function (next) {
  if (this.isModified("value")) {
    // Trigger automation related to column update
    const triggerEvent = {
      type: "cv_updated",
      boardId: this.boardId,
      dayId: this.dayId,
      row: this, // pass the row that was updated
    };
    await AutomationService.executeAutomation(triggerEvent);
  }
  next();
});
```

#### Time Based

- date_arrives (on time, or H-/+days before + time)
- time_period (daily, weekly, monthly)

### Conditions: Rules that determine whether an action should be executed.

- column is empty (daysSerevice: getColumnValue)
- item is in a group (daysService: getItem)
- status is something (daysService: getColumnValue)
- dropdown meets condition (daysService: getColumnValue)
- number meets condition (daysService: getColumnValue)
- person is someone (daysService: getColumnValue)

--

## Actions: Tasks performed in response to a trigger.

### Action Types

- move item to days (daysService: deleteRow & createRow)
- notify (notificationService: sendGmail, sendWhatsapp)
- change status (days service: updateColumnValue)
- create subitem (days service: createSubItem)
- set date (days service: updateColumnValue)
- create item (days service: createItem)
- duplicate item (days service: createItem)
- archive item (days service: updateItem)
- delete item (days service: deleteItem)
- create thread (days service: createThread)
- clear column (days service: updateColumnValue)
- assign specified persion (days service: updateColumnValue)
- assign the item creator (days service: updateColumnValue)
- assign team member (days service: updateColumnValue)
- clear assignees (days service: updateColumnValue)
- push date to the next day (days service: updateColumnValue)
- start time tracking (days service: updateColumnValue)
- stop time tracking (days service: updateColumnValue)
- set hour to current time (days service: updateColumnValue)
- set number to current time (days service: updateColumnValue)
- increase/decrease number by x (days service: updateColumnValue)
- create days (board service: createDaysConfig)
- duplicate days (board service: createDaysConfig)

--

## Rules/Automations: Combination of triggers, conditions, and actions.

Simple Rules: A single trigger and action.
Complex Rules: Multiple triggers, conditions, and actions linked together.

## Service Layer Design

### Automation Service Responsibilities

- Register, modify, and delete rules/automations.
- Process trigger events.
- Evaluate conditions before actions are executed.
  - Query the automations collections
  - Embed information on board collections
- Manage and execute actions when a trigger condition is met.
- Handle complex workflows that require chaining multiple triggers, conditions, and actions.

### Time-Based Triggers

use a job scheduler (like node-cron) to periodically check for automations that need to be executed based on time conditions.

- Identify which automations are eligible for that day
- Schedule pending job to execute (based on the provided time)

### Implementation Considerations

- Asynchronous Tasks: Use a task queue or background job processor to handle time-consuming actions.
- Error Handling: Implement robust error handling to prevent automation failures.
- Testing: Thoroughly test automations to ensure they work as expected.

# Architecture Overview

- Main Service (API): handle user requests (creating/updating rows, column values) and publishes events to RabbitMQ when a potential automation trigger occurs.
- Worker Service: consume events from RabbitMQ, checks for matching automations, evaluates conditions, executes actions, and notifies the main service of completion.
- RabbitMQ: message broker
- Database Access: both services need access to MongoDB (for Automation, Board, and row data).

Flow:

1. Main Service detects a change (e.g., cv_updated)
2. Publishes event to RabbitMQ
3. Worker Service picks it up
4. Processes automation
5. Executes actions
6. Publishes completion event back to RabbitMQ
7. Main Service logs

## Notes

- race conditions, setiap ada update, kirim param previous conditions
