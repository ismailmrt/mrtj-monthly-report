get All

```json
{
  "message": "Successfully retrieved all workspaces",
  "data": [
    {
      "imageConfig": {
        "color": "#FFFF00",
        "asset": "https://www.martidocs.com/storage/to-placeholder.png"
      },
      "coverConfig": {
        "color": "#FFFF00",
        "asset": "https://www.martidocs.com/storage/to-placeholder.png"
      },
      "_id": "67ad956050a658df81221a4f",
      "name": "SISKA SYS",
      "description": "Lorem ipsum",
      "privacy": 1,
      "state": 0,
      "companyId": 0,
      "creatorId": 22,
      "members": [
        {
          "_id": 22,
          "role": 0,
          "joinedAt": "2025-02-13T06:46:56.670Z"
        }
      ],
      "teams": []
    }
  ],
  "pagination": {
    "page": 1,
    "limit": 10,
    "totalPages": 1,
    "totalResults": 7
  }
}
```

```json
{
  "message": "Successfully retrieved all workspaces",
  "data": [
    {
      "_id": "67ad956050a658df81221a4f",
      "name": "SISKA SYS",
      "description": "Lorem ipsum",
      "privacy": 1,
      "imageConfig": {
        "color": "#FFFF00",
        "asset": "https://www.martidocs.com/storage/to-placeholder.png"
      }
    }
  ],
  "pagination": {
    "page": 1,
    "limit": 10,
    "totalData": 7,
    "totalPages": 1
  }
}
```
