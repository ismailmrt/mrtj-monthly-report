# Martidocs - Peruri Digital Signature Implementation

I need to write requirement analysis on implementing PERURI Digital Signature service to my web service.
The project is called Martidocs and one of the feature is Digital Signature or "Tanda Tangan Elektronik".
Currently the signing is done using BSrE-BSSN service. However due to business requirements, Dev Team Blue Squad need to
Implement PERURI Digital Signature onto the existing features and the future revamp features.

Business requirements:

- To implement PERURI Digital Signature Service
- To be confirmed:
  - User can either opt-in to use BSrE or PERURI digital signing service
  - System decide automatically which digital signing service to use (when one service is unavailable, heavy load, etc)

Current Business Process:

- Access Martidocs website
- Select "Tanda Tangan Elektronik" Menu on Dashboard or Sidenav
- Website will show the UI of "Tanda Tangan Elektronik" feature
- User can choose either to:
  - Request digital signature for a document
  - Approve/Sign documents that are assigned to the user
  - Monitor digital signature request
- First, user to request digital signature for a document
  - Click on "Minta Tanda Tangan Button"
  - Fill out the provided form
    - Perihal
    - Document State (Belum pernah di tandatangani, Sudah pernah di tandatangani)
    - PDF File (upload)
    - Approval order type (Parallel, Sirkuler)
    - Signer (Minimal to select 1 user)
    - Notes
    - Document No
    - Is Require Initials
    - Initials Approver (Minimal to select 1 user)
  - After filling out the provided form, the data will be saved in the DB
  - Next is to determine the signature placement, will be saved in the DB
    - User need to choose Page number
    - Select the signer and place the sign in the page by dragging it towards the desired place
  - Next is to clik on "Ajukan Tanda Tangan" Button to request the signing, the user that needs to sign this documents
    will be notified
- Second, user to approve/sign documents that are assigned to the user
  - On the "Tanda Tangan Elektronik" feature UI there's a view on the left of the document list for data filter,
    there they can filter the shown data on the list by clicking Tanda Tangan Saya>Belum to show the requested signing.
  - Upon clicking that, they will be shown only the documents that need their sign, to proceed click the desired documents
  - User can review the documents information and to view/download the PDF.
  - User then can Sign or reject the document by clicking "Tandatangani" button or "Tolak" button, they also can provide notes
  - Upon signing the documents, they are required to provide "eSign Client Service Passphrase"
  - After filling out the Passphrase the documents is finished and can be downloaded

Current Technical Process:

TLDR: all in one hit

## JWT

Acquiring JWT
there are 2 options:

- ON event
- ON server start

## Certificate

## Digital Signature

penjelasan masing masing API

### Default

1 document, stateless, OTP

### Berjenjang

1 document, stateful, OTP

### Paralel

1 document, stateful, OTP, setelah signing semua

### Bulk

n documents, placement sama

### Signing System

n documents, session key instead of OTP,

## Use Case

- user opt in to user BSrE or PERURI
- to use PERURI once BSrE down

# Requirements

- explain the current business process (customer actions)
  - akses menu tanda tangan elektronik
  - minta tanda tangan
    - form
  -
- explain technical process behind it (algorithm)
  - currently everything is stored on DB
  - then once everything is complete, proceed send to BSrE
- show how PERURI might fit the flow
- show what PERURI offers and possible implementation towards SaaS product
