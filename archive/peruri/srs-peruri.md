# Requirement Analysis for Implementing PERURI Digital Signature Service in Martidocs

## Overview

### Martidocs

Martidocs is an existing web service that includes a feature for digital signatures,
currently utilizing the BSrE-BSSN service. The new project requirement is to implement the PERURI
Digital Signature Service to enhance this feature. This implementation is driven by business needs
and aims to provide users with an additional option for digital signing.

### Business Requirements

1. Primary Requirement: Implement PERURI Digital Signature Service.
2. Optional Requirements (to be confirmed):
   - Allow users to opt-in to use either BSrE or PERURI digital signing service.
   - Enable the system to automatically decide which digital signing service to use
     based on service availability and load conditions.

### Current Business Process

![User Journey](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2406/martidocs-e-sign-user-journey.png)
Fig. 1: User Journey

#### Accessing the Digital Signature Feature

1. Users access the Martidocs website.
2. **Select Digital Signature Feature**: From the dashboard or side navigation, click on the "Tanda Tangan Elektronik" menu.

#### Requesting a Digital Signature

1. **Initiate Request**: Click the "Minta Tanda Tangan" button.
2. **Fill Out Request Form**:
   - Complete the form with the following fields:
     - **Perihal (Subject)**
     - **Document State** (Signed, Never Signed)
     - **PDF File** (upload)
     - **Approval Order Type** (Parallel, Circular)
     - **Signer** (Multiple user)
     - **Notes**
     - **Document Number**
     - **Is Require Initials**
     - **Initials Approver** (Multiple user)
3. **Submit Form**: Submit the form to save the data in the database.
4. **Determine Signature Placement**:
   - Select the page number.
   - Drag and place the signature on the desired location on the document.
5. **Submit Signature Request**:
   - Click "Ajukan Tanda Tangan" to request the signing.
   - Notifies the users who need to sign the document.

#### Approving or Signing Documents

1. **Filter Documents**: Filter the document list in the "Tanda Tangan Elektronik" UI to show only documents needing the user's signature.
2. **Review Document**:
   - Review the document's information.
   - View or download the PDF as needed.
3. **Sign or Reject Document**:
   - To sign, click "Tandatangani" and enter the "eSign Client Service Passphrase."
   - To reject, click "Tolak" and provide notes if necessary.
4. **Download Signed Document**: After signing, download the signed document.

### PERURI Digital Signature

The documents provided (Dokumen Web Service, Perisai Akses & Mekanisme Pelaksanaan Testing)
presents a comprehensive overview of the web services provided by Peruri Digital Signature,
offering a robust set of functionalities for seamless integration into partner systems.
It covers essential aspects such as Generating JSON, Certificate, and various Digital Signature services.

![Peruri - PERISAI](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2406/peruri-digital-signature.png)
Fig. 1: User Journey [Access Link]()

## Analysis

### Existing implementation comparison

1. All-in-One endpoint @ BSrE Implementation
2. Canvas signing formula

### Possible implementation approach

1. Service Wrapper
2. Signing System
