# Refactoring

## Model

### Current

```js
import mongoose from "mongoose";
import { ROLE_TYPES } from "../../utils/constants.js";

const { Schema } = mongoose;

const MODEL_NAME = "Workspace";

/**
 * Schema for representing a workspace member.
 * @typedef {Object} MemberReference
 * @property {Number} _id - The unique identifier for the member.
 * @property {Number} role - The role of the member.
 * @property {Date} joinedAt - The date when the member joined the workspace.
 */
const memberReferenceSchema = new Schema(
  {
    _id: { type: Number, required: true },
    role: {
      type: Number,
      enum: Object.values(ROLE_TYPES),
      required: true,
      default: ROLE_TYPES.VIEWER,
    },
    joinedAt: { type: Date, required: true, default: Date.now },
  },
  { _id: false },
);

/**
 * Schema for representing a team within a workspace.
 * @typedef {Object} TeamReference
 * @property {Number} role - The role of the team.
 * @property {Date} joinedAt - The date when the team was added to the workspace.
 */
const teamReferenceSchema = new Schema({
  role: {
    type: Number,
    enum: Object.values(ROLE_TYPES),
    required: true,
    default: ROLE_TYPES.VIEWER,
  },
  joinedAt: { type: Date, required: true, default: Date.now },
});

/**
 * Schema for representing a workspace.
 * @typedef {Object} Workspace
 * @property {String} name - The name of the workspace.
 * @property {String} [description] - Optional description of the workspace.
 * @property {Number} privacy - Privacy setting of the workspace (0 for closed, 1 for open).
 * @property {Object} imageConfig - Configuration for the workspace image.
 * @property {String} [imageConfig.color] - Optional color for the image.
 * @property {String} [imageConfig.asset] - Optional asset URL for the image.
 * @property {Object} coverConfig - Configuration for the workspace cover.
 * @property {String} [coverConfig.color] - Optional color for the cover.
 * @property {String} [coverConfig.asset] - Optional asset URL for the cover.
 * @property {Number} state - The state of the workspace (0 for active, 1 for archived, 2 for deleted).
 * @property {Number} companyId - The ID of the company associated with the workspace.
 * @property {Number} creatorId - The user ID of the creator of the workspace.
 * @property {MemberReference[]} members - List of members associated with the workspace.
 * @property {TeamReference[]} teams - List of teams associated with the workspace.
 */
const schema = new Schema(
  {
    name: { type: String, required: true },
    description: { type: String, required: false },
    privacy: {
      type: Number, // 0 (closed) or 1 (open)
      required: true,
      default: 0,
    },
    imageConfig: {
      color: { type: String, required: false },
      asset: { type: String, required: false },
    },
    coverConfig: {
      color: { type: String, required: false },
      asset: { type: String, required: false },
    },
    state: {
      type: Number, // 0 (active), 1 (archived), 2 (deleted)
      required: true,
      default: 0,
    },
    companyId: { type: Number, required: true },
    creatorId: { type: Number, required: true }, // user_id
    members: [memberReferenceSchema],
    teams: [teamReferenceSchema],
  },
  { timestamps: true },
);

schema.index({ companyId: 1 });

/**
 * Model for representing a workspace in MongoDB.
 * @type {mongoose.Model<Workspace>}
 */
const Workspace = mongoose.model(MODEL_NAME, schema);
export default Workspace;
```

### Reference

```js
const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const { toJSON, paginate } = require("./plugins");
const { roles } = require("../config/roles");

const userSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error("Invalid email");
        }
      },
    },
    password: {
      type: String,
      required: true,
      trim: true,
      minlength: 8,
      validate(value) {
        if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
          throw new Error(
            "Password must contain at least one letter and one number",
          );
        }
      },
      private: true, // used by the toJSON plugin
    },
    role: {
      type: String,
      enum: roles,
      default: "user",
    },
    isEmailVerified: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  },
);

// add plugin that converts mongoose to json
userSchema.plugin(toJSON);
userSchema.plugin(paginate);

/**
 * Check if email is taken
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
userSchema.statics.isEmailTaken = async function (email, excludeUserId) {
  const user = await this.findOne({ email, _id: { $ne: excludeUserId } });
  return !!user;
};

/**
 * Check if password matches the user's password
 * @param {string} password
 * @returns {Promise<boolean>}
 */
userSchema.methods.isPasswordMatch = async function (password) {
  const user = this;
  return bcrypt.compare(password, user.password);
};

userSchema.pre("save", async function (next) {
  const user = this;
  if (user.isModified("password")) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

/**
 * @typedef User
 */
const User = mongoose.model("User", userSchema);

module.exports = User;
```

## Routes

### Current

```js
import { Router } from "express";

import {
  createWorkspaceValidationRules,
  updateWorkspaceValidationRules,
  addMemberValidationRules,
  updateMemberValidationRules,
  addTeamValidationRules,
  updateTeamValidationRules,
} from "../../middlewares/validators/workspaceValidator.js";

import { validate } from "../../utils/validate.js";

import {
  addWorkspaceMember,
  addWorkspaceMembers,
  addWorkspaceTeam,
  createWorkspace,
  deleteWorkspace,
  getWorkspace,
  getWorkspaceById,
  getWorkspaceMembers,
  getWorkspaceTeams,
  removeWorkspaceMember,
  removeWorkspaceTeam,
  updateWorkspace,
  updateWorkspaceMemberRole,
  updateWorkspaceTeamRole,
} from "../../controllers/monday/workspaceController.js";
import { authenticate } from "../../middlewares/auth.js";

const router = Router();

router.get("/", authenticate, getWorkspace);

router.get("/:workspaceId", authenticate, getWorkspaceById);

router.post(
  "/",
  authenticate,
  validate(createWorkspaceValidationRules),
  createWorkspace,
);

router.put(
  "/:workspaceId",
  authenticate,
  validate(updateWorkspaceValidationRules),
  updateWorkspace,
);

router.delete("/:workspaceId", authenticate, deleteWorkspace);

router.get("/:workspaceId/members", authenticate, getWorkspaceMembers);

router.post(
  "/:workspaceId/member",
  authenticate,
  validate(addMemberValidationRules),
  addWorkspaceMember,
);

router.post("/:workspaceId/members", authenticate, addWorkspaceMembers);

router.put(
  "/:workspaceId/members/:memberId",
  authenticate,
  validate(updateMemberValidationRules),
  updateWorkspaceMemberRole,
);

router.delete(
  "/:workspaceId/members/:memberId",
  authenticate,
  removeWorkspaceMember,
);

router.get("/:workspaceId/teams", authenticate, getWorkspaceTeams);

router.post(
  "/:workspaceId/teams",
  authenticate,
  validate(addTeamValidationRules),
  addWorkspaceTeam,
);

router.put(
  "/:workspaceId/teams/:teamId",
  authenticate,
  validate(updateTeamValidationRules),
  updateWorkspaceTeamRole,
);

router.delete("/:workspaceId/teams/:teamId", authenticate, removeWorkspaceTeam);

export default router;
```

### Reference

```js
const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const userValidation = require("../../validations/user.validation");
const userController = require("../../controllers/user.controller");

const router = express.Router();

router
  .route("/")
  .post(
    auth("manageUsers"),
    validate(userValidation.createUser),
    userController.createUser,
  )
  .get(
    auth("getUsers"),
    validate(userValidation.getUsers),
    userController.getUsers,
  );

router
  .route("/:userId")
  .get(
    auth("getUsers"),
    validate(userValidation.getUser),
    userController.getUser,
  )
  .patch(
    auth("manageUsers"),
    validate(userValidation.updateUser),
    userController.updateUser,
  )
  .delete(
    auth("manageUsers"),
    validate(userValidation.deleteUser),
    userController.deleteUser,
  );

module.exports = router;
```

## Service

### Current

```js
currently there are no service impl
```

### Reference

```js
const httpStatus = require("http-status");
const { User } = require("../models");
const ApiError = require("../utils/ApiError");

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createUser = async (userBody) => {
  if (await User.isEmailTaken(userBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Email already taken");
  }
  return User.create(userBody);
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryUsers = async (filter, options) => {
  const users = await User.paginate(filter, options);
  return users;
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getUserById = async (id) => {
  return User.findById(id);
};

/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
  return User.findOne({ email });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User not found");
  }
  if (updateBody.email && (await User.isEmailTaken(updateBody.email, userId))) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Email already taken");
  }
  Object.assign(user, updateBody);
  await user.save();
  return user;
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User not found");
  }
  await user.remove();
  return user;
};

module.exports = {
  createUser,
  queryUsers,
  getUserById,
  getUserByEmail,
  updateUserById,
  deleteUserById,
};
```

## Controller

### Current

```js
import mongoose from "mongoose";
import Workspace from "../../models/mongo/workspace.js";
import { ROLE_OWNER, ROLE_TYPES } from "../../utils/constants.js";
import { logger } from "../../utils/logger.js";
import { toObjectId } from "../../utils/mongooseHelpers.js";
import { paginate, paginateArrayField } from "../../utils/pagination.js";
import models from "../../models/index.js";
const { User, User_info } = models;

export async function getWorkspace(req, res, next) {
  try {
    const { name, page, limit } = req.query;

    const result = await paginate({
      model: Workspace,
      pipeline: [
        {
          $match: {
            ...(name ? { name: { $regex: name, $options: "i" } } : {}),
            state: 0,
          },
        },
        { $sort: { createdAt: -1 } },
      ],
      page,
      limit,
      projection: {
        name: 1,
        description: 1,
        privacy: 1,
        imageConfig: 1,
      },
    });

    res.status(200).json({
      message: "Successfully retrieved all workspaces",
      data: result.data,
      pagination: result.pagination,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}

export async function getWorkspaceById(req, res, next) {
  try {
    const { workspaceId } = req.params;

    const data = await Workspace.findOne(
      { _id: toObjectId(workspaceId) },
      {
        name: 1,
        description: 1,
        imageConfig: 1,
        coverConfig: 1,
        membersCount: { $size: "$members" },
        teamsCount: { $size: "$teams" },
      },
    );

    if (!data) {
      const error = new Error(`Workspace with ID ${workspaceId} not found `);
      error.name = "notFound";
      throw error;
    }

    res.status(200).json({
      message: `Successfully retrieved workspace with id ${workspaceId}`,
      data: data,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}

export async function createWorkspace(req, res, next) {
  try {
    const { id: user_id } = req.user;
    const { name, description, privacy, imageConfig, coverConfig } = req.body;
    const members = [
      {
        _id: user_id,
        role: ROLE_TYPES.ADMIN,
        joinedAt: Date.now(),
      },
    ];

    const data = new Workspace({
      creatorId: user_id,
      companyId: 0,
      name,
      description,
      privacy: privacy || 0,
      imageConfig,
      coverConfig,
      members,
    });

    await data.save();

    res.status(201).json({
      message: "Successfully created a new workspace",
      data,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}

// TODO: validate the requesting user is admin within this team
export async function updateWorkspace(req, res, next) {
  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    const user_id = req.user.id;
    const { workspaceId } = req.params;
    const { name, description, privacy, imageConfig, coverConfig, state } =
      req.body;

    const data = await Workspace.findOne({
      _id: workspaceId,
      creatorId: user_id,
    });

    if (!data) {
      const error = new Error(
        `Workspace with ID ${workspaceId} not found or you don't have permission to update it`,
      );
      error.name = "notFound";
      throw error;
    }

    data.name = name || data.name;
    data.description = description || data.description;
    data.privacy = privacy || data.privacy;
    data.imageConfig = imageConfig || data.imageConfig;
    data.coverConfig = coverConfig || data.coverConfig;
    data.state = state || data.state;

    await data.save({ session });

    await session.commitTransaction();
    session.endSession();

    res.status(200).json({
      message: "Successfully updated workspace",
      data: data,
    });
  } catch (error) {
    await session.abortTransaction();
    session.endSession();
    logger.error(error);
    next(error);
  }
}

// TODO: validate the requesting user is owner within this workspace
export async function deleteWorkspace(req, res, next) {
  try {
    const user_id = req.user.id;
    const { workspaceId } = req.params;

    const data = await Workspace.findOneAndDelete({
      _id: workspaceId,
      creatorId: user_id,
    });

    if (!data) {
      const error = new Error(
        `Workspace with ID ${workspaceId} not found or you don't have permission to delete it`,
      );
      error.name = "notFound";
      throw error;
    }

    res.status(200).json({
      message: `Successfully deleted workspace with ID ${workspaceId}`,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}

export async function getWorkspaceMembers(req, res, next) {
  try {
    const { workspaceId } = req.params;
    const { page, limit } = req.query;

    //TODO: bind member data from Mysql's User to get name, nickName, email, phone; and also to the User_Infos using the user_id FK to get avatar
    const result = await paginateArrayField({
      model: Workspace,
      pipeline: [
        {
          $match: {
            _id: toObjectId(workspaceId),
          },
        },
      ],
      arrayField: "members",
      page,
      limit,
    });

    const memberIds = result.data.map((member) => member._id);

    const members = await User.findAll({
      where: { id: memberIds },
      include: [
        {
          model: User_info,
          as: "userInfo",
          attributes: ["avatar"],
          required: false,
        },
      ],
      attributes: ["id", "name", "nickName", "email", "phone"],
    });

    // Combine workspace member data with detailed information
    const enrichedMembers = result.data.map((workspaceMember) => {
      const memberDetail = members.find((m) => m.id === workspaceMember._id);
      return {
        ...workspaceMember,
        email: memberDetail?.email,
        name: memberDetail?.name,
        nickName: memberDetail?.nickName,
        phone: memberDetail?.phone,
        avatar: memberDetail?.userInfo?.avatar,
      };
    });

    res.status(200).json({
      message: "Successfully retreived all workspace member",
      data: enrichedMembers,
      pagination: result.pagination,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}

export async function getWorkspaceTeams(req, res, next) {
  try {
    const { workspaceId } = req.params;
    const { page, limit } = req.query;

    //TODO: bind team data (name, description, logo)
    const result = await paginateArrayField({
      model: Workspace,
      pipeline: [
        {
          $match: {
            _id: toObjectId(workspaceId),
          },
        },
      ],
      arrayField: "teams",
      page,
      limit,
    });
    res.status(200).json({
      message: "Successfully retreived all workspace teams",
      data: result.data || [],
      pagination: result.pagination,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}

// TODO: validate the requesting user is admin within this workspace
export async function addWorkspaceMember(req, res, next) {
  try {
    const user_id = req.user.id;
    const { workspaceId } = req.params;
    const { memberId, role } = req.body;

    //TODO: validate memberId is exist in the mysql db
    const member = {
      _id: memberId,
      role,
      joinedAt: new Date(),
    };

    const data = await Workspace.findOneAndUpdate(
      {
        _id: workspaceId,
        creatorId: user_id,
        "members._id": { $ne: memberId },
      },
      { $push: { members: member } },
      { new: true },
    );

    if (!data) {
      const error = new Error(
        `Workspace with ID ${workspaceId} not found or member already exists or you don't have permission to add it`,
      );
      error.name = "notFound";
      throw error;
    }

    res.status(200).json({
      message: "Successfully added member to the workspace",
      data,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}

export async function addWorkspaceMembers(req, res, next) {
  try {
    const user_id = req.user.id;
    const { workspaceId } = req.params;
    const { memberEmails, role } = req.body; //TODO: ensure memberEmails is an array using middleware

    const emails = Array.isArray(memberEmails) ? memberEmails : [memberEmails];
    const members = await User.findAll({
      where: {
        email: emails,
      },
    });

    // Check if all emails were found
    const foundEmails = members.map((m) => m.email);
    const missingEmails = emails.filter(
      (email) => !foundEmails.includes(email),
    );

    if (missingEmails.length > 0) {
      const error = new Error(
        `Members with emails not found: ${missingEmails.join(", ")}`,
      );
      error.name = "notFound";
      throw error;
    }

    // Prepare members to add
    const membersToAdd = members.map((member) => ({
      _id: member.id,
      role,
      joinedAt: new Date(),
    }));

    // Update workspace with new members
    const data = await Workspace.findOneAndUpdate(
      {
        _id: workspaceId,
        creatorId: user_id,
        "members._id": { $nin: members.map((m) => m.id) },
      },
      { $push: { members: { $each: membersToAdd } } },
      { new: true },
    );

    if (!data) {
      const error = new Error(
        `Workspace with ID ${workspaceId} not found or all members already exist or you don't have permission to add them`,
      );
      error.name = "notFound";
      throw error;
    }

    res.status(200).json({
      message: "Successfully added members to the workspace",
      data,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}

// TODO: validate the requesting user is admin within this workspace
export async function addWorkspaceTeam(req, res, next) {
  try {
    const user_id = req.user.id;
    const { workspaceId } = req.params;
    const { teamId, role } = req.body;

    //TODO: validate teamId is exist in the mongo db
    const team = {
      _id: teamId,
      role,
      joinedAt: new Date(),
    };

    const data = await Workspace.findOneAndUpdate(
      {
        _id: workspaceId,
        creatorId: user_id,
        "teams._id": { $ne: teamId },
      },
      { $push: { teams: team } },
      { new: true },
    );

    if (!data) {
      const error = new Error(
        `Workspace with ID ${workspaceId} not found or team already exists or you don't have permission to add it`,
      );
      error.name = "notFound";
      throw error;
    }

    res.status(200).json({
      message: "Successfully added team to the workspace",
      data,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}

// TODO: validate the requesting user is admin within this team
export async function removeWorkspaceMember(req, res, next) {
  try {
    const user_id = req.user.id;
    const { workspaceId, memberId } = req.params;

    const data = await Workspace.findOneAndUpdate(
      { _id: workspaceId, creatorId: user_id, "members._id": memberId },
      { $pull: { members: { _id: memberId } } },
      { new: true },
    );

    if (!data) {
      const error = new Error(
        `Workspace with ID ${workspaceId} or member with ID ${memberId} not found or you don't have permission to remove it`,
      );
      error.name = "notFound";
      throw error;
    }

    res.status(200).json({
      message: "Successfully removed member from the workspace",
      data,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}

// TODO: validate the requesting user is admin within this team
export async function removeWorkspaceTeam(req, res, next) {
  try {
    const user_id = req.user.id;
    const { workspaceId, teamId } = req.params;

    const data = await Workspace.findOneAndUpdate(
      { _id: workspaceId, creatorId: user_id, "teams._id": teamId },
      { $pull: { teams: { _id: teamId } } },
      { new: true },
    );

    if (!data) {
      const error = new Error(
        `Workspace with ID ${workspaceId} or team with ID ${teamId} not found or you don't have permission to remove it`,
      );
      error.name = "notFound";
      throw error;
    }

    res.status(200).json({
      message: "Successfully removed team from the workspace",
      data,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}

// TODO: validate the requesting user is admin within this team
export async function updateWorkspaceMemberRole(req, res, next) {
  try {
    const user_id = req.user.id;
    const { workspaceId, memberId } = req.params;
    const { role } = req.body;

    const data = await Workspace.findOneAndUpdate(
      { _id: workspaceId, creatorId: user_id, "members._id": memberId },
      { $set: { "members.$.role": role } },
      { new: true },
    );

    if (!data) {
      const error = new Error(
        `Workspace with ID ${workspaceId} or member with ID ${memberId} not found or you don't have permission to update it`,
      );
      error.name = "notFound";
      throw error;
    }

    res.status(200).json({
      message: "Successfully updated member role",
      data,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}

// TODO: validate the requesting user is admin within this team
export async function updateWorkspaceTeamRole(req, res, next) {
  try {
    const user_id = req.user.id;
    const { workspaceId, teamId } = req.params;
    const { role } = req.body;

    const data = await Workspace.findOneAndUpdate(
      { _id: workspaceId, creatorId: user_id, "teams._id": teamId },
      { $set: { "teams.$.role": role } },
      { new: true },
    );

    if (!data) {
      const error = new Error(
        `Workspace with ID ${workspaceId} or team with ID ${teamId} not found or you don't have permission to update it`,
      );
      error.name = "notFound";
      throw error;
    }

    res.status(200).json({
      message: "Successfully updated team role",
      data,
    });
  } catch (error) {
    logger.error(error);
    next(error);
  }
}
```

### Reference

```js
const httpStatus = require("http-status");
const pick = require("../utils/pick");
const ApiError = require("../utils/ApiError");
const catchAsync = require("../utils/catchAsync");
const { userService } = require("../services");

const createUser = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  res.status(httpStatus.CREATED).send(user);
});

const getUsers = catchAsync(async (req, res) => {
  const filter = pick(req.query, ["name", "role"]);
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const result = await userService.queryUsers(filter, options);
  res.send(result);
});

const getUser = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.params.userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User not found");
  }
  res.send(user);
});

const updateUser = catchAsync(async (req, res) => {
  const user = await userService.updateUserById(req.params.userId, req.body);
  res.send(user);
});

const deleteUser = catchAsync(async (req, res) => {
  await userService.deleteUserById(req.params.userId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createUser,
  getUsers,
  getUser,
  updateUser,
  deleteUser,
};
```

```js
//ApiError & catchAsync impl
class ApiError extends Error {
  constructor(statusCode, message, isOperational = true, stack = "") {
    super(message);
    this.statusCode = statusCode;
    this.isOperational = isOperational;
    if (stack) {
      this.stack = stack;
    } else {
      Error.captureStackTrace(this, this.constructor);
    }
  }
}

module.exports = ApiError;

//catchAsync
const catchAsync = (fn) => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch((err) => next(err));
};

module.exports = catchAsync;
```

lastly here's the schema plugin impl

```js
//paginate.plugin.js
const paginate = (schema) => {
  /**
   * @typedef {Object} QueryResult
   * @property {Document[]} results - Results found
   * @property {number} page - Current page
   * @property {number} limit - Maximum number of results per page
   * @property {number} totalPages - Total number of pages
   * @property {number} totalResults - Total number of documents
   */
  /**
   * Query for documents with pagination
   * @param {Object} [filter] - Mongo filter
   * @param {Object} [options] - Query options
   * @param {string} [options.sortBy] - Sorting criteria using the format: sortField:(desc|asc). Multiple sorting criteria should be separated by commas (,)
   * @param {string} [options.populate] - Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,)
   * @param {number} [options.limit] - Maximum number of results per page (default = 10)
   * @param {number} [options.page] - Current page (default = 1)
   * @returns {Promise<QueryResult>}
   */
  schema.statics.paginate = async function (filter, options) {
    let sort = "";
    if (options.sortBy) {
      const sortingCriteria = [];
      options.sortBy.split(",").forEach((sortOption) => {
        const [key, order] = sortOption.split(":");
        sortingCriteria.push((order === "desc" ? "-" : "") + key);
      });
      sort = sortingCriteria.join(" ");
    } else {
      sort = "createdAt";
    }

    const limit =
      options.limit && parseInt(options.limit, 10) > 0
        ? parseInt(options.limit, 10)
        : 10;
    const page =
      options.page && parseInt(options.page, 10) > 0
        ? parseInt(options.page, 10)
        : 1;
    const skip = (page - 1) * limit;

    const countPromise = this.countDocuments(filter).exec();
    let docsPromise = this.find(filter).sort(sort).skip(skip).limit(limit);

    if (options.populate) {
      options.populate.split(",").forEach((populateOption) => {
        docsPromise = docsPromise.populate(
          populateOption
            .split(".")
            .reverse()
            .reduce((a, b) => ({ path: b, populate: a })),
        );
      });
    }

    docsPromise = docsPromise.exec();

    return Promise.all([countPromise, docsPromise]).then((values) => {
      const [totalResults, results] = values;
      const totalPages = Math.ceil(totalResults / limit);
      const result = {
        results,
        page,
        limit,
        totalPages,
        totalResults,
      };
      return Promise.resolve(result);
    });
  };
};

module.exports = paginate;

//toJson.plugin.js
/* eslint-disable no-param-reassign */

/**
 * A mongoose schema plugin which applies the following in the toJSON transform call:
 *  - removes __v, createdAt, updatedAt, and any path that has private: true
 *  - replaces _id with id
 */

const deleteAtPath = (obj, path, index) => {
  if (index === path.length - 1) {
    delete obj[path[index]];
    return;
  }
  deleteAtPath(obj[path[index]], path, index + 1);
};

const toJSON = (schema) => {
  let transform;
  if (schema.options.toJSON && schema.options.toJSON.transform) {
    transform = schema.options.toJSON.transform;
  }

  schema.options.toJSON = Object.assign(schema.options.toJSON || {}, {
    transform(doc, ret, options) {
      Object.keys(schema.paths).forEach((path) => {
        if (schema.paths[path].options && schema.paths[path].options.private) {
          deleteAtPath(ret, path.split("."), 0);
        }
      });

      ret.id = ret._id.toString();
      delete ret._id;
      delete ret.__v;
      delete ret.createdAt;
      delete ret.updatedAt;
      if (transform) {
        return transform(doc, ret, options);
      }
    },
  });
};

module.exports = toJSON;
```

can you help me to refactor from current implementation to the reference, no need for the model layer though except for the plugin impl there. just the routes (except the auth middleware), services and controllers. also no need for the pick utils impl and catch async impl
