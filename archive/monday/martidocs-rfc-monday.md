# Martidocs Monday Feature - RFC

## Schema

> cust_id at every collection

1. teams
   - id
   - name
   - owners
   - picture_url
   - users
2. workspaces
   - description
   - id
   - kind (open, closed)
   - name
   - owners_subscribers (users)
   - settings
   - state (active, archived, deleted)
   - team_owners_subscribers (teams)
   - teams_subscribers (team)
   - user_subscribers (users)
3. folders
   - color
   - id
   - name
   - owner (user)
   - parent_folder_id
   - workspace_id
4. boards
   - kind (public, private, share)
   - name
   - owner_ids (user)
   - subscriber_ids (user)
   - subscriber_teams_ids (user)
   - description
   - folder_id
   - workspace_id
   - creator (user)
   - id
   - permissions (everyone, collaborators, assignee, owners)
   - state (active, archived, deleted)
   - tags
5. groups
   - state (active, archived, deleted)
   - board_id
   - color
   - id
   - items
   - position (int)
   - name
6. column
   - board_id
   - state (active, archived, deleted)
   - description
   - id
   - defaults
   - title
   - type (column_type)
7. items
   - board_id
   - column_values
   - creator
   - group_id
   - id
   - linked_items
   - name
   - position
   - parent_item
   - state (active, archived, deleted)
   - subscriber_ids (user)
8. column_values
   - column
   - id
   - text
   - type (column_type)
   - value
