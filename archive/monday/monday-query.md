_this document was to proof the query to get the monday data from mongodb_

```javascript
//===================================================================insert customers
db.customers.insertMany([
  {
    _id: "c0",
    name: "MRTJ",
    url: "jakartamrt", //subdomain
    logoUrl: null, //optional
    headerUrl: null, //optional
  },
  {
    _id: "c1",
    name: "Google",
    url: "google", //subdomain
    logoUrl: null, //optional
    headerUrl: null, //optional
  },
]);

//===================================================================insert users
db.users.insertMany([
  {
    _id: "u1",
    name: "rismail",
    email: "rismail@jakartamrt.co.id",
    role: "member", // or member (customers scope)
    status: "active", //pending: email confirmation, active
    invitedBy: null, //user
    cust_id: "c0",
  },
  {
    _id: "u0",
    name: "rfikri",
    email: "rfikri@jakartamrt.co.id",
    role: "admin", // or member (customers scope)
    status: "active", //pending: email confirmation, active
    invitedBy: null, //user
    cust_id: "c0",
  },
  {
    _id: "u2",
    name: "lpage",
    email: "lpage@google.com",
    role: "admin", // or member (customers scope)
    status: "active", //pending: email confirmation, active
    invitedBy: null, //user
    cust_id: "c1",
  },
]);

//===================================================================insert teams
db.teams.insertMany([
  {
    _id: "t0",
    name: "DevSquad",
    description: "Lorem Ipsum Dolor",
    config: {
      color: "#FFFF00",
      asset: "fa-heart", //can be url to use uploaded asset instead
    },
    cust_id: "c0",
    //Use an array of references to the N-side objects combined with embedded fields
    members: [
      {
        user_id: "u0",
        role: "owner",
        joinedAt: "2024-07-15",
      },
    ],
  },
]);

//===================================================================update teams, add member
db.teams.updateOne(
  { _id: "t0" }, // Query to find the document
  {
    $push: {
      members: {
        user_id: "u1",
        role: "member",
        joinedAt: "2024-07-15",
      },
    },
  },
);

//===================================================================insert workspaces
db.workspaces.insertOne({
  _id: "w0",
  name: "CABIN",
  description: "Dev @ JB Tower",
  privacy: "open", //open, closed
  imageConfig: {
    color: "#FFFFFF",
    asset: "fa-heart", //can be url to use uploaded asset instead
  },
  coverConfig: {
    color: null, //optional
    asset: null, //optional
  },
  state: "active", //active, archived, deleted
  cust_id: "c0",
  members: [
    {
      user_id: "u0",
      role: "subscriber", // or "owner"
      joinedAt: "2024-07-15",
    },
  ],
  teams: [],
});

//===================================================================update workspaces, add member
db.workspaces.updateOne(
  { _id: "w0" }, // Query to find the document
  {
    $push: {
      members: {
        user_id: "u1",
        role: "subscriber",
        joinedAt: "2024-07-15",
      },
    },
  },
);

//===================================================================update workspaces, remove member
db.workspaces.updateOne(
  { _id: "w0" }, // Query to find the document
  {
    $pull: {
      members: { user_id: "u1" }, // Criteria to match the element to remove
    },
  },
);

//===================================================================update workspaces, add teams
db.workspaces.updateOne(
  { _id: "w0" }, // Query to find the document
  {
    $push: {
      teams: {
        user_id: "t0",
        role: "subscriber",
        joinedAt: "2024-07-15",
      },
    },
  },
);

//===================================================================insert boards
db.boards.insertOne({
  _id: "b0",
  name: "Martidocs",
  description: "Existing martidocs board",
  privacy: "open", //open, closed
  itemTerminology: "item", //or row, budget, project, ... custom text by user
  state: "active", //active, archived, deleted
  creator_id: "u1", //user
  workspace_id: "w0",
  cust_id: "c0",
  members: [
    {
      user_id: "u1",
      role: "subscriber", // or "owner"
      joinedAt: "2024-07-15",
    },
  ],
  teams: [
    {
      team_id: "t0",
      role: "subscriber", // or "owner"
      addedAt: "2024-07-15",
    },
  ],
  days: [
    {
      _id: "d0",
      name: "Day0",
      color: null,
      position: 0,
      state: "active", //active, archived, deleted
      head: "", //to help start traversing, rows_id
    },
    {
      _id: "d1",
      name: "Day1",
      color: null,
      position: 0,
      state: "active", //active, archived, deleted
      head: "", //to help start traversing, rows_id
    },
  ], //max 32
  mainColumn: [
    {
      _id: "mc0",
      name: "Status",
      description: "Task status",
      type: "status", //from ColumnType constraints, hardcoded from backend
      config: {
        kind: "enum",
        value: ["DRAFT", "WIP", "DONE"],
      },
      state: "active", //active, archived, deleted
      position: 1,
    },
    {
      _id: "mc1",
      name: "Assignee",
      description: null,
      type: "people", //from ColumnType constraints, hardcoded from backend
      config: {
        kind: "relations",
        value: "users",
      },
      state: "active", //active, archived, deleted
      position: 1,
    },
  ], //max 64
  subColumn: [
    {
      _id: "sc0",
      name: "Status",
      description: "Task status",
      type: "status", //from ColumnType constraints, to be discussed
      config: {
        kind: "enum",
        value: ["DISCUSSION", "DISCUSSED", "DONE"],
      },
      state: "active", //active, archived, deleted
      position: 0,
    },
  ], //max 64
});

//===================================================================insert rows
db.rows.insertOne({
  _id: "r0",
  name: "Peruri Digital Signature Implementation",
  prev: null, //head
  next: null, //tail
  board_id: "b0", //nested obj id in boards
  day_id: "d0", //nested obj id in boards
  parent_id: null, // if null, then it's a parent!
  child_head_id: null, // saving query
  user_id: "u0",
  cust_id: "c0",
  columnValues: [
    {
      column_id: "mc0",
      value: {
        stringValue: "WIP",
        rawValue: {
          status: "WIP",
        },
        previousValue: {},
      },
    },
    {
      column_id: "mc1",
      value: {
        stringValue: "rfikri",
        rawValue: {
          _id: "u0",
          name: "rfikri",
          email: "rfikri@jakartamrt.co.id",
        },
        previousValue: {},
      },
    },
  ], //max 64
});

db.rows.createIndex({ "columnValues.value.stringValue": 1 });

//then to update the board, set the head
db.boards.updateOne(
  { _id: "b0", "days._id": "d0" },
  { $set: { "days.$.head": "r0" } },
);

//===================================================================get rows
db.rows.aggregate([
  {
    $match: {
      board_id: "b0",
      day_id: "d0",
    },
  },
  {
    $lookup: {
      from: "boards",
      localField: "board_id",
      foreignField: "_id",
      as: "board_info",
    },
  },
  {
    $unwind: "$board_info",
  },
  {
    $lookup: {
      from: "users",
      localField: "creator_id",
      foreignField: "_id",
      as: "creator_info",
    },
  },
  {
    $unwind: "$creator_info",
  },
  {
    $addFields: {
      "creator._id": "$creator_info._id",
      "creator.name": "$creator_info.name",
      "creator.email": "$creator_info.email",
      type: {
        $cond: {
          if: { $eq: ["$parent_id", null] },
          then: "parent",
          else: "child",
        },
      },
    },
  },
  {
    $addFields: {
      columnValues: {
        $map: {
          input: "$columnValues",
          as: "cv",
          in: {
            $mergeObjects: [
              "$$cv",
              {
                $arrayElemAt: [
                  {
                    $filter: {
                      input: {
                        $concatArrays: [
                          "$board_info.mainColumn",
                          "$board_info.subColumn",
                        ],
                      },
                      as: "col",
                      cond: { $eq: ["$$col._id", "$$cv.column_id"] },
                    },
                  },
                  0,
                ],
              },
            ],
          },
        },
      },
    },
  },
  {
    $project: {
      creator_info: 0,
      board_info: 0,
    },
  },
]);

// or can be done without aggregate which to do it in node,
// first is to get the boards, which includes days and columns
// then get the row based on the requested board and its day

//===================================================================search row value
const searchTerm = "rismail"; //columnValues value

db.rows
  .aggregate([
    {
      $match: {
        "columnValues.value.stringValue": {
          $regex: searchTerm,
          $options: "i", // case-insensitive
        },
      },
    },
  ])
  .toArray();

db.rows
  .find({
    "columnValues.value.stringValue": {
      $regex: searchTerm,
      $options: "i",
    },
  })
  .toArray();

//===================================================================update row value
db.rows.updateOne(
  {
    _id: "r0",
    "columnValues.column_id": "mc0",
  },
  {
    $set: {
      "columnValues.$.value.stringValue": "DONE",
      "columnValues.$.value.rawValue.status": "DONE",
      "columnValues.$.value.previousValue": {
        status: "WIP",
      },
    },
  },
);

db.rows.updateOne(
  {
    _id: "r0",
    "columnValues.column_id": "mc1",
  },
  {
    $set: {
      "columnValues.$.value.stringValue": "rismail",
      "columnValues.$.value.rawValue._id": "u1", // Assuming u1 is the new user's id
      "columnValues.$.value.rawValue.name": "rismail",
      "columnValues.$.value.rawValue.email": "rismail@jakartamrt.co.id",
      "columnValues.$.value.previousValue": {
        _id: "u0",
        name: "rfikri",
        email: "rfikri@jakartamrt.co.id",
      },
    },
  },
);
```
