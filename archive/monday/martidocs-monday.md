# Martidocs monday collection schema

AI dashboard

this document is to help design the collection schema for martidocs monday

## overview

original design structure

- /workspaces
  - boards
    - days[32]
      - columns(row_1)[64]
      - columns(row_n)[64]
        - subdays (subitem)

proposed design structure

- /workspace
  - boards: days[32], mainColumn[64], subColumn[64]
    - rows(HEAD): columnValues[64]
    - rows(TAIL): columnValues[64]

### gambar diagram

### representasi excel

## notes

1. focus on MVP
2. column type, event type, trigger & condition is hardcoded
3. research on:
   4 query strategy:

   - get (compile value to (row, colType))
   - insert (col, row)
   - update (type, pos, val)
   - delete (col, row)

   logic:

   - switch pos arr (insert, del)
   - switch pos LL (insert, del)

## collection

1. customers (ALREADY EXIST USING MYSQL)

```javascript
{
  _id: "c0",
  name: "MRTJ",
  url: "jakartamrt", //subdomain
  logoUrl: null, //optional
  headerUrl: null, //optional
}
```

> customers collection here is functioning as an "Organization"

query:

- get `users` that are belongs to this customer
- get `teams` that are belongs to this customer
- get `workspaces` that are belongs to this customer
- get `boards` that are the belongs to this customer
- get `rows` that are the belongs to this customer
- get `board activith logs` that are belongs to this customer
- get `automations` that are belongs to this customer

2. users (ALREADY EXIST USING MYSQL)

```javascript
{
  _id: "u1",
  name: "rismail",
  email: "rismail@jakartamrt.co.id",
  role: "admin", // or member (customers scope)
  status: "active", //pending: email confirmation, active
  invitedBy: null, //user
  cust_id: "c0"
}
```

query:

- get `teams` where this user belongs to, along with its role
- get `workspaces` where this user belongs to, along with its role
- get `boards` where this user belongs to, along with its role
- get `rows` where this user is the creator
- get `board activity log` where this user is the creator

3. teams

```javascript
{
  _id: "t0",
  name: "DevSquad",
  description: "Lorem Ipsum Dolor",
  imageConfig: {
    color: "#FFFF00",
    asset: "fa-heart", //can be url to use uploaded asset instead
  },
  companyId: "c0",
  creatorId: "u0",
  //Use an array of references to the N-side objects combined with embedded fields
  members: [
    {
      _id: "u0",
      role: "subscriber", // or "owner"
      joinedAt: "2024-07-08T12:00:00Z"
    }
  ]
}
```

query:

- get `members` that are belong to this team
- register `members` to this team
- remove `members` from this team

```javascript
// Fetch the Team document identified by _id
team = db.teams.findOne({
  _id: "317ed8d9-62c7-43b0-a730-d5e037c7c826",
});

// Create an array of id`s containing *just* the user_id
user_ids = team.members.map(function (doc) {
  return doc.user_id;
});

// Fetch all the users that are a member of this team
team_members = db.users.find({ _id: { $in: user_ids } }).toArray();
```

<!--analyze the query perfornamce, from complexity aspects, frequency, and its impact on database-->

4. workspaces

> open: every team member in this account can join, closed: enterprise only

```javascript
{
  _id: "w0",
  name: "CABIN",
  description: "Dev @ JB Tower",
  privacy : "open", //open, closed
  imageConfig: {
    color: "#FFFFFF",
    asset: "fa-heart", //can be url to use uploaded asset instead
  },
  coverConfig: {
    color: null, //optional
    asset: null, //optional
  },
  state : "active", //active, archived, deleted
  companyId: "c0",
  creatorId: "u0",
  members: [
    {
      _id: "u0",
      role: "owner", // or "subscriber"
      joinedAt: "2024-07-08T12:00:00Z"
    }
  ],
  teams: [
    {
      _id: "t0",
      role: "subscriber", // or "owner"
      addedAt: "2024-07-08T12:00:00Z"
    }
  ]
}
```

query:

- get only the visible workspaces for the requesting user (customer scoped)
- get `members` and `teams` that are belong to this workspace
- register `members` and `teams` to this workspace
- remove `members` and `teams` from this workspace

```javascript
// Fetch the Workspace document identified by _id
workspace = db.workspaces.findOne({
  _id: "id",
});

// Create an array of id`s containing *just* the user_id
user_ids = workspace.members.map(function (doc) {
  return doc.user_id;
});

// Create an array of id`s containing *just* the team_id
team_ids = workspace.teams.map(function (doc) {
  return doc.team_id;
});

// Fetch all the child that are linked to this parent (workspace)
workspace_members = db.users.find({ _id: { $in: user_ids } }).toArray();
workspace_teams = db.teams.find({ _id: { $in: team_ids } }).toArray();
```

<!--analyze the query perfornamce, from complexity aspects, frequency, and its impact on database-->

5. boards
<!--
Need to access this object on its own,
thus seperate this into its own collection/document
-->

```javascript
{
  _id: "b0",
  name: "Martidocs",
  description: "Existing martidocs board",
  privacy : "open", //open, closed
  itemTerminology : "item", //or row, budget, project, ... custom text by user
  state : "active", //active, archived, deleted
  creatorId : "u0", //user
  workspaceId: "w0",
  companyId: "c0",
  members: [
    {
      _id: "u0",
      role: "owner", // or "subscriber"
      joinedAt: "2024-07-08T12:00:00Z"
    }
  ],
  teams: [
    {
      _id: "t0",
      role: "subscriber", // or "owner"
      addedAt: "2024-07-08T12:00:00Z"
    }
  ],
  daysConfig: [
    {
      _id: "d0",
      label: "Day0",
      color: null,
      position: 0,
      state : "active", //active, archived, deleted
    },
  ], //max 32
  mainColumn: [
    {
      _id: "mc0",
      label: "Status",
      description: "Task status",
      config: {
        //some values
      },
      state : "active", //active, archived, deleted
      position: 0,
    },
  ], //max 64
  subColumn: [
    {
      _id: "sc0",
      label: "Status",
      description: "Task status",
      configuration: {
        //some values
      },
      state : "active", //active, archived, deleted
      position: 0,
    },
  ] //max 64
}
```

query:

- get only the visible bards for the requesting user (workspace scoped)
- get `members` and `teams` that are belong to this board
- register `members` and `teams` to this board
- remove `members` and `teams` from this board
- get the board day's row, also to project the mainColumn/subColumn
  information into the columnValues item based on the nested column_id

```javascript
// Fetch the Team document identified by cust_id
board = db.boards.findOne({
  _id: "6231258f-50ac-4c0a-86fb-b6a9ffc1a5cd",
});

// Create an array of id`s containing *just* the user_id
user_ids = board.members.map(function (doc) {
  return doc.user_id;
});

// Create an array of id`s containing *just* the team_id
team_ids = board.teams.map(function (doc) {
  return doc.team_id;
});

// Fetch all the users that are a member of this board
board_members = db.users.find({ _id: { $in: user_ids } }).toArray();
board_teams = db.teams.find({ _id: { $in: team_ids } }).toArray();
```

```javascript
// column type sample, will be hardcoded in BE
{
  label: "Status",
  description: "Task status",
  config: {
    kind: "enum",
    value: [
      {
        name : "Default",
        color: "#001133",
      },
      {
        name : "Working On It",
        color: "#FF1100",
      },
      {
        name : "Stuck",
        color: "#FFF7F8",
      },
      {
        name : "Done",
        color: "#00FFFF",
      },
    ],
  },
},
{
  label: "Assignee",
  description: "Task assignee",
  type: "people",
  config: {
    kind: "relations",
    value: "users",
  },
}

const COLUMN_TYPES = {
  CHECKBOX: {
    label: 'Checkbox',
    description: 'Simple true or false value',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.BOOLEAN,
      defaultValue: false,
    },
  },
  CURRENCY: {
    label: 'Currency',
    description: 'Monetary value with a specific currency',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.DOUBLE,
      defaultValue: 0.0,
      defaultPrefix: 'Rp.',
    },
  },
  DATE: {
    label: 'Date',
    description: 'Represents a specific date (and potentially time)',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.DATE,
      defaultValue: new Date(),
      defaultFormat: 'DD/MM/YYYY',
      defaultTimeFormat: 'HH:mm',
    },
  },
  EMAIL: {
    label: 'Email',
    description:
      'Allows to attach an email address to a row and send emails to that contact with a single click',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.STRING,
      defaultValue: '',
      defaultInteraction: 'mailto:{value}',
    },
  },
  FILE: {
    label: 'File',
    description: 'Contains files attached to a board',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.OBJECT,
      defaultValue: null, // Represents a file object
    },
  },
  LINK: {
    label: 'Link',
    description: 'URL or hyperlink',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.STRING,
      defaultValue: '',
      defaultInteraction: 'href="{value}"',
    },
  },
  LOCATION: {
    label: 'Location',
    description: 'Stores a location with longitude and latitude precision',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.OBJECT,
      defaultValue: { lat: null, lng: null },
    },
  },
  NUMBER: {
    label: 'Number',
    description: 'Holds number values (Integer)',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.INT,
      defaultValue: 0,
    },
  },
  PEOPLE: {
    label: 'People',
    description: 'Represents a person(s) who is assigned to the row',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.ARRAY, // refer to array of mysql id
      defaultValue: [], // Array of user IDs or objects
    },
  },
  PERCENT: {
    label: 'Percent',
    description: 'Percentage value',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.DOUBLE,
      defaultValue: 0.0,
      suffix: '%',
    },
  },
  STATUS: {
    label: 'Status',
    description: 'Represents a label designation for your row(s)',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.OBJECT,
      defaultValue: 'Backlog', // Or other default status
      options: [
        {
          name: 'Backlog',
          color: '#FF1100',
        },
        {
          name: 'WIP',
          color: '#FFF7F8',
        },
        {
          name: 'Done',
          color: '#00FFFF',
        },
      ], // Example status options
    },
  },
  TAGS: {
    label: 'Tags',
    description:
      'Holds words and/or phrases that helps to group rows using these keywords.',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.ARRAY,
      defaultValue: [], // Array of tag strings
    },
  },
  TEXT: {
    label: 'Text',
    description: 'Simple text holder',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.STRING,
      defaultValue: '',
    },
  },
  TIMELINE: {
    label: 'Timeline',
    description: 'Timeline with start and end dates',
    config: {
      primitiveType: COLUMN_PRIMITIVE_TYPES.OBJECT,
      defaultValue: { start: null, end: null }, // Represents start and end dates
    },
  },
};
```

6. martidays/dayboard, could be hundreds of rows for each board's day

```javascript
{
  _id: "r0",
  name: "Peruri Digital Signature Implementation",
  boardId: "b0", //nested obj id in boards
  creatorId: "u0",
  companyId: "c0",
  columnValues: [
    {
      column_id: "mc0",
      value: {
        stringValue: "WIP",
        rawValue: {
          status: "WIP",
        },
        previousValue: {},
      },
    },
  ], //max 64
  children: [
    _id: "r0",
    name: "Peruri Digital Signature Implementation",
    boardId: "b0", //nested obj id in boards
    creatorId: "u0",
    companyId: "c0",
    columnValues: [
      {
        column_id: "mc0",
        value: {
          stringValue: "WIP",
          rawValue: {
            status: "WIP",
          },
          previousValue: {},
        },
      },
    ], //max 64
  ] // detect limit of 16 mb
}
```

query:

- insert row
- edit row columnValues
- search row columnValues
- delete row

4 data dalam satu day, bagaimana representasi di db, bagaimana di response
skema dari mulai insert, get, update, search
concern lebih di query

7. board activity log

Monday implementation on activity log:

- Basic plan holds activity from the past week only.
- Standard plan holds activity data for 6 months.
- Pro plan holds data for up to 1 year.
- Enterprise plan holds data for up to 5 years.

```javascript
{
  _id: "l0",
  timestamp: "2024-07-09T11:24:38",
  userId: "u0",
  boardId: "b0",
  event: "cv_updated", // row_created
  body: {
    rowId: "66cc2515d640d5837e9fc74f",
    columnId: "66c6e880b939e7056effffff",
    dayId: "d0",
    value: "DONE"
  }, // body used in the API
  params: {
    // any other params used in the API
  },
  additionalInfo: {
    ipAddress: "192.168.1.1",
    userAgent: "Chrome"
  },
  companyId: "c0",
}
```

8. automation

```javascript
{
  _id: "a0",
  userId: "u0",
  boardId: "b0",
  dayId: "d0",
  state : "active", //active, pending, deleted
  conditions: [
    {
      event: "cv_updated",
      conditions: {
        // depending on the data structure [row or column value]
        columnId: "mc0",
        value: "DONE"
    }, // <----------------------------- trigger
    {
      event: "cv_updated",
      conditions: {
        // depending on the data structure [row or column value]
        columnId: "mc2",
        value: "LOW"
      }
    },
  ], // max 64, and the first entry is the trigger, the rest are the conditions (and only)
  actions: [
    {
      type: "move_item", // service mapping
      params: {
        dayId: "d2"
      } // whatever params that are consumeable by service
    },
    {
      type: "send_notification",
      params: {
        user_ids: ["u0"],
        message: "A high priority task has been completed!"
      }
    },
    {
      type: "update_cv",
      params: {
        columnId: "mc1",
        value: "userA"
      }
    }
  ] //max 8
}
```
