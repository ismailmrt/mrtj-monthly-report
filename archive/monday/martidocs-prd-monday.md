# **PRD: Martidocs Expansion - Task & Project Management**

---

## **1. Introduction**

### **Purpose**

MRT currently uses monday.com for task management, but has an internal tool called Martidocs for document approval and signing. This project aims to consolidate these needs by expanding Martidocs to include task and project management features. This will not only reduce reliance on external tools but also offer a unified experience for document handling, task tracking, and project coordination. It will position Martidocs as a competitive SaaS product for external clients.

### **Scope**

Integration of task and project management features similar to monday.com, including Workspaces, Boards, Columns, Days, Rows, Subrows, Automation, and Rules. The scope of this project is limited to time and resource constraints. The goal is for this project to be demonstrated to user December this year and released in June next year.

### **Target Audience**

- **Internal Users**: MRT's employees and departments using Martidocs for task, project, and document management.
- **External Users**: Clients subscribed to Martidocs as a SaaS platform for task management, project coordination, and document handling.

---

## **2. Detailed Feature Breakdown**

### **Teams (Nice to Have)**

#### **Overview**

Teams represent organizational groups within a company (e.g., divisions, departments). Teams enable efficient collaboration across various boards and workspaces. Permissions and roles within the team structure will govern access to workspaces and boards.

#### **Functional Requirements**

- **Team Creation & Management**:
  - Users can create, update, or delete teams.
  - Team members can be added/removed, and their roles can be adjusted (admin/member).
- **Team Access & Permissions**:
  - Teams can be assigned to specific workspaces or boards.
  - Permission levels at the board and workspace level should allow the following:
    - **Owner**: Full control over data and settings.
    - **Member**: Modify data, invite member and assign roles.
    - **Viewer**: Read-only access.

#### **Edge Cases**

- How will roles inherited from teams with its individual members?

---

### **Workspaces**

#### **Overview**

Workspaces act as containers for boards. They represent high-level project environments and can host multiple boards. Workspaces help users organize boards into distinct groups and allow for collaboration across teams.

#### **Functional Requirements**

- **Workspace Creation & Permissions**:
  - Create, update, and delete workspaces.
  - Invite users or teams to workspaces and assign roles (owner, member, viewer).
  - Manage permissions scope
- **Workspace Customization**:
  - Customize workspace settings such as themes, names.
- **Limits**:
  - **Unlimited** number of boards can exist within a workspace, but ensure a cap of **50 concurrent users** per workspace to prevent overload.

---

### **Boards**

#### **Overview**

Boards are the core feature for organizing tasks and projects. They consist of days (groups), rows (tasks), and customizable columns. Boards represent a single project or task set that needs to be managed.

#### **Functional Requirements**

- **Board Creation & Configuration**:
  - Create, update, and delete boards.
  - Add, remove, or modify columns with different data types.
- **Columns**:
  - Up to **64 columns** per board.
  - Column types include:
    - **People**: Assign users to tasks (indexable entities).
    - **Date**: Set deadlines.
    - **String**: Free-text input.
    - **Number**: Numeric fields (e.g., currency, percentages).
    - **Url**: Can be used for file attachments or links, mailto.
    - **Status/Options**: Predefined options (e.g., status, priority, etc).
    - **Boolean**: Binary checkbox.
    - **Object**: Can be used for complex values such as location, timeline.
- **Views**:
  - Boards support **multiple views** (e.g., Kanban, Calendar).
  - Users can toggle between views to visualize tasks and deadlines differently.

---

### **Days (Groups)**

#### **Overview**

Days help organize tasks by grouping them based on specific criteria (e.g., workweek, priority). Days act as containers for rows (tasks) and can have associated automations.

#### **Functional Requirements**

- **Day Management**:
  - Create, update, delete, and rename days within a board.
  - Automations can trigger day-level rules (e.g., move completed tasks “Done” to day N, when "X" is assigned, move to day N).
- **Limits**:
  - Each board can have a maximum of **32 days**.
  - Days cannot be reordered, but rows (tasks) can move between days.

---

### **Rows (Items)**

#### **Overview**

Rows represent individual tasks. Rows are assigned to a day and contain data depending on the structure of the columns defined in the board.

#### **Functional Requirements**

- **Task Management**:
  - Create, update, delete rows.
  - Assign/update row column values.
  - Move row between days.
  - Create subrows: Allow tasks to be broken down into smaller steps. These subrows reside within a parent row and cannot be moved outside their parent row. The columns is defined in the board's sub-columns configuration
- **Unlimited** rows can be created under each day. This allows flexibility in task management at the granular level.

---

### **Automation & Rules**

#### **Overview**

Automations allow users to streamline repetitive tasks by triggering predefined actions based on specific events or conditions.

#### **Functional Requirements**

- **Custom Rules**:
  - Define automation rules at the board or day level.
  - Example rules:
    - Automatically move tasks to the “Archived” day when status is marked as completed.
    - Notify users when deadlines are approaching.
- **Integration**:
  - Automations should be processed asynchronously using RabbitMQ for reliability.

#### **Requirement Analysis**

- Triggers:
  - Value Based:
    - row_created
      - item created
      - item moved to another day
    - row_updated
      - item name changed
      - thread changed
    - cv_updated
      - status changes to something
      - column changes
      - button clicked
      - person assigned
      - status changes from someting to something
      - email changes
      - subitem status changes
  - Time Based:
    - date_arrives (on time, or H-/+days before + time)
    - time_period (daily, weekly, monthly)
- Conditions:
  - column is empty (daysSerevice: getColumnValue)
  - item is in a group (daysService: getItem)
  - status is something (daysService: getColumnValue)
  - dropdown meets condition (daysService: getColumnValue)
  - number meets condition (daysService: getColumnValue)
  - person is someone (daysService: getColumnValue)
- Actions:
  - move item to days (daysService: deleteRow & createRow)
  - notify (notificationService: sendGmail, sendWhatsapp)
  - change status (days service: updateColumnValue)
  - create subitem (days service: createSubItem)
  - set date (days service: updateColumnValue)
  - create item (days service: createItem)
  - duplicate item (days service: createItem)
  - archive item (days service: updateItem)
  - delete item (days service: deleteItem)
  - create thread (days service: createThread)
  - clear column (days service: updateColumnValue)
  - assign specified persion (days service: updateColumnValue)
  - assign the item creator (days service: updateColumnValue)
  - assign team member (days service: updateColumnValue)
  - clear assignees (days service: updateColumnValue)
  - push date to the next day (days service: updateColumnValue)
  - start time tracking (days service: updateColumnValue)
  - stop time tracking (days service: updateColumnValue)
  - set hour to current time (days service: updateColumnValue)
  - set number to current time (days service: updateColumnValue)
  - increase/decrease number by x (days service: updateColumnValue)
  - create days (board service: createDaysConfig)
  - duplicate days (board service: createDaysConfig)

---

## **3. Non-Functional Requirements**

### **Performance Requirements**

- **Response Time**:
  - API endpoints should respond within 200ms under normal load (500 requests per minute).
  - The UI must render boards, groups, items, and subitems within 2 seconds.
- **Scalability**:
  - The system must support up to 10,000 simultaneous users without noticeable performance degradation.
  - It should accommodate up to 1,000,000 boards, 10,000,000 items, and 50,000,000 subitems.
- **Throughput**:
  - Capable of processing 10,000 API requests per minute with 99.9% uptime.

---

### **Security Requirements**

- **Data Encryption**:
  - All data in transit must be encrypted using **TLS 1.2+**.
  - Sensitive data (passwords, tokens) must be encrypted using **AES-256**.
- **Authentication**:
  - Implement token-based authentication (JWT) for all endpoints.
  - Implement **Role-Based Access Control (RBAC)** for permissions at workspace, board, day, and row levels.

---

### **Scalability & Availability**

- **Horizontal Scaling**:
  - The system must support horizontal scaling to handle load peaks without downtime.
- **Uptime**:
  - Ensure **99.9% uptime** with a disaster recovery plan. Recovery Time Objective (RTO) of **1 hour**, and Recovery Point Objective (RPO) of **5 minutes**.

### **Usability Requirements**

- **User Interface**:
  - The UI must be intuitive, requiring minimal training for new users.
  - Accessibility features should comply with WCAG 2.1 standards, supporting screen readers and keyboard navigation.
- **Localization**:
  - The system should support multiple languages (bahasa & english) and be easily extendable to accommodate additional languages.

---

## **4. Success Metrics**

### **Adoption**:

- **Internal**: Achieve **80% adoption** of Martidocs task management internally by **August 2025**.
- **External**: Onboard **100 external clients** to Martidocs SaaS by **December 2025**.

### **User Satisfaction**:

- Maintain a user satisfaction score of **85% or higher** for ease of use.

### **Performance**:

- Ensure **99.9% uptime** with response times of less than **200ms** for 99% of requests.

---

## **5. Timeline & Milestones**

### **Development Phases**:

- **September 2024**: Backend MVP (including user authentication, basic board functionality).
- **November 2024**: Frontend MVP (basic UI and task management flow).
- **December 2024**: Demo to internal users.
- **April 2025**: Feature freeze for task and project management, final testing begins.
- **June 2025**: Full launch of Martidocs with monday-like features.

---
