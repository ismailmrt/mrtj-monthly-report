_this document is the very first draft of the MartinDocs monday schema_

1. customer
   - name
   - url (domain)
   - weekends
   - logo_url
   - header_url
   - users
2. users
   - name
   - email
   - role
   - status (pending: email confirmation, active)
   - invited_by (user)
3. teams
   - name
   - image{}
     - url: optional, override
     - color
     - img: favicon
   - members[] (user)
4. workspace
   - name
   - description
   - privacy (open, closed)
   - members[] (owner, subscriber)
   - teams[] (owner, subscriber)
   - image{}
     - url: optional, override
     - color
     - img: favicon
   - cover_url{}
     - url: optional, override
     - color: optional
     - img: asset
   - state (active, archived, deleted)
5. folders
   - name
   - color
   - parent_id: (default: null -> root)
   - workspace_id
6. boards
   - name
   - description
   - privacy (main, private, shareable)
   - configuration{}
     - item_terminology (item, budget, project, ...)
     - status []{}
       - name (working on it, stuck, done, default, [custom])
       - color
   - members[] (owner, subscriber)
   - state (active, archived, deleted)
   - creator_id (user)
   - folder_id
7. groups
   - name
   - color
   - prev
   - next
   - state (active, archived, deleted)
   - board_id
8. items
   - name
   - ~~comments~~
   - ~~files~~
   - state (active, archived, deleted)
   - prev
   - next
   - group_id
   - parent_id
   - creator_id
9. columns
   - name
   - description
   - type
   - is_main_column
   - configuration{}
     - type specific values: json
   - state (active, archived, deleted)
   - prev
   - next
   - board_id
10. column_values
    - item_id
    - column_id
    - value{}
      - type specific values: json
11. activity_log

> open: every team member in this account can join, closed: enterprise only
> main: visible to everyone in this account, private, shareable: working privately with guests outside this account
