currently this is my first home page, i want that when user clicking the ArrowRightOutlined button or hitting enter on keyboard it will navigate to the
navigate(`/assistant/chat/prompt?${the-prompt}`);

```jsx
import {
  ArrowRightOutlined,
  FileImageOutlined,
  FileOutlined,
  FilePdfOutlined,
  FileTextOutlined,
  FileZipOutlined,
  LineChartOutlined,
  PaperClipOutlined,
  SettingOutlined,
  TeamOutlined,
} from "@ant-design/icons";
import {
  Box,
  Card,
  CardContent,
  Grid,
  IconButton,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import { useTheme } from "@mui/system";
import { useState } from "react";
import Lottie from "lottie-react";
import simarti from "./simarti_greetings.json";

const prompts = [
  {
    label: "Operational data management strategies",
    icon: <FileTextOutlined style={{ fontSize: "20px", color: "#1677ff" }} />,
  },
  {
    label: "Improving collaboration between departments",
    icon: <TeamOutlined style={{ fontSize: "20px", color: "#52c41a" }} />,
  },
  {
    label: "Analyzing MRT Jakarta performance trends",
    icon: <LineChartOutlined style={{ fontSize: "20px", color: "#faad14" }} />,
  },
  {
    label: "Best practices for maintaining AI systems",
    icon: <SettingOutlined style={{ fontSize: "20px", color: "#ff4d4f" }} />,
  },
];

const getFileIcon = (fileName) => {
  const extension = fileName.split(".").pop().toLowerCase();
  const iconProps = { fontSize: "20px" };
  switch (extension) {
    case "pdf":
      return <FilePdfOutlined style={{ ...iconProps, color: "#ff4d4f" }} />;
    case "jpg":
    case "jpeg":
    case "png":
      return <FileImageOutlined style={{ ...iconProps, color: "#52c41a" }} />;
    case "zip":
    case "rar":
      return <FileZipOutlined style={{ ...iconProps, color: "#faad14" }} />;
    default:
      return <FileOutlined style={{ ...iconProps, color: "#1677ff" }} />;
  }
};

function DashboardPage() {
  const theme = useTheme();
  const [inputValue, setInputValue] = useState("");
  const [uploadedItems, setUploadedItems] = useState([]);
  const [hoveredIndex, setHoveredIndex] = useState(null);

  const handleCardClick = (label) => setInputValue(label);

  const handleAddItem = () => {
    const fileName = prompt(
      "Enter file name with extension (e.g., report.pdf)",
    );
    if (fileName) setUploadedItems([...uploadedItems, fileName]);
  };

  const handleDeleteItem = (index) => {
    setUploadedItems((prevItems) => prevItems.filter((_, i) => i !== index));
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: "70vh",
      }}
    >
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          gap: 2,
          mb: 2,
        }}
      >
        <Lottie
          animationData={simarti}
          style={{ width: 120, height: 120 }}
          loop={true}
        />
        <Typography variant="h2" sx={{ fontWeight: "bold" }}>
          What do you want to know?
        </Typography>
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          width: "100%",
          maxWidth: 800,
          border: "2px solid",
          borderColor: "rgb(227, 227, 221)",
          borderRadius: "8px",
          px: 4,
          py: 3,
        }}
      >
        <TextField
          fullWidth
          placeholder="Ask anything..."
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
          variant="standard"
          multiline
          minRows={1}
          maxRows={5}
          InputProps={{
            disableUnderline: true,
            style: { fontSize: "16px" },
          }}
          autoComplete="off"
          sx={{
            minHeight: 40,
            "& .MuiInputBase-root": {
              height: "auto",
              alignItems: "flex-start",
              padding: "8px 0",
            },
          }}
        />

        <Grid
          container
          spacing={2}
          sx={{
            mt: 2,
            width: "100%",
            maxWidth: 800,
          }}
        >
          {uploadedItems.map((item, index) => (
            <Grid
              item
              key={index}
              onMouseEnter={() => setHoveredIndex(index)}
              onMouseLeave={() => setHoveredIndex(null)}
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  backgroundColor: "#f0f0f0",
                  borderRadius: "8px",
                  cursor: "pointer",
                  p: 2,
                  position: "relative",
                }}
              >
                {getFileIcon(item)}
                <Typography variant="body1" sx={{ ml: 2 }}>
                  {item}
                </Typography>
                {hoveredIndex === index && (
                  <IconButton
                    sx={{
                      position: "absolute",
                      top: 4,
                      right: 4,
                      color: "#ff4d4f",
                      backgroundColor: "white",
                      borderRadius: "50%",
                      boxShadow: "0 0 5px rgba(0,0,0,0.2)",
                      "&:hover": {
                        backgroundColor: "#ffcccc",
                      },
                    }}
                    onClick={() => handleDeleteItem(index)}
                  >
                    X
                  </IconButton>
                )}
              </Box>
            </Grid>
          ))}
        </Grid>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            width: "100%",
            mt: 2,
          }}
        >
          <Tooltip title="Attach file" placement="left" arrow>
            <IconButton
              size="large"
              sx={{
                width: 40,
                height: 40,
                "&:hover": {
                  backgroundColor: "#e0e0e0",
                },
              }}
              onClick={handleAddItem}
            >
              <PaperClipOutlined />
            </IconButton>
          </Tooltip>
          <IconButton
            size="large"
            sx={{
              backgroundColor: "#f0f0f0",
              borderRadius: "50%",
              "&:hover": {
                backgroundColor: theme.palette.primary.main,
                "& svg": {
                  color: "white",
                },
              },
            }}
          >
            <ArrowRightOutlined />
          </IconButton>
        </Box>
      </Box>

      <Grid
        container
        spacing={2}
        sx={{
          mt: 2,
          width: "100%",
          maxWidth: 800,
          justifyContent: "center",
        }}
      >
        {prompts.map((item, index) => (
          <Grid item key={index}>
            <Card
              sx={{
                borderColor: "#edede7",
                borderRadius: "8px",
                cursor: "pointer",
                transition: "transform 0.3s, box-shadow 0.3s",
                boxShadow: "0 4px 5px rgba(0, 0, 0, 0.2)",
                "&:hover": {
                  transform: `translateY(-8px) rotate(${Math.random() > 0.5 ? "-2deg" : "2deg"})`,
                  boxShadow: "0 8px 20px rgba(0, 0, 0, 0.3)",
                },
              }}
              onClick={() => handleCardClick(item.label)}
            >
              <CardContent sx={{ display: "flex", alignItems: "center" }}>
                {item.icon}
                <Typography variant="body1" sx={{ ml: 2 }}>
                  {item.label}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
}

export default DashboardPage;
```

which is this page, but currently it's using anthropic client to consume the LLM, i want you to help me to refactor it to instead hit our own LLM API using react-query

```jsx
import {
  ArrowRightOutlined,
  CopyOutlined,
  DislikeOutlined,
  LikeOutlined,
  PaperClipOutlined,
  ReloadOutlined,
  ShareAltOutlined,
} from "@ant-design/icons";
import { Box, IconButton, InputBase, Paper, Typography } from "@mui/material";
import { Fragment, useState } from "react";

const anthropic = new Anthropic({
  apiKey:
    "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", // Replace with your actual API key
  dangerouslyAllowBrowser: true,
});

function TaskPage() {
  const [inputMessage, setInputMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [messages, setMessages] = useState([
    {
      user: "Hi there",
      assistant: "Hi there! How can I assist you today?",
    },
    // {
    //   user: 'Can you find the maintenance schedule for MRT trains in the technical documentation?',
    //   assistant:
    //     'Based on the Technical Maintenance Manual (TMM-2023), MRT Jakarta implements a tiered maintenance schedule:\n\n1. Daily Inspection (DI): Basic checks before first operation\n2. Monthly Maintenance (MM): Detailed systems check\n3. Semi-annual Overhaul (SO): Comprehensive mechanical inspection\n4. Annual Major Maintenance (AMM): Complete systems overhaul\n\nWould you like me to provide more specific details about any of these maintenance types?'
    // }
    // {
    //   user: 'Show me the safety protocols for emergency situations at underground stations',
    //   assistant:
    //     'According to the Emergency Response Protocol document (ERP-2023), here are the key safety protocols for underground stations:\n\n- Each station has 4 emergency exits clearly marked with illuminated signs\n- Fire suppression systems are installed every 50 meters\n- Emergency ventilation systems can clear smoke in under 3 minutes\n- Emergency response teams must arrive within 5 minutes of any incident\n\nThe document also specifies evacuation procedures and staff responsibilities. Would you like me to elaborate on any specific aspect?'
    // },
    // {
    //   user: 'What are the current passenger capacity statistics for rush hour?',
    //   assistant:
    //     'Based on the latest Operational Performance Report (Q4 2023), during rush hour (07:00-09:00):\n\n- Average passengers per train: 1,250 people\n- Peak station capacity: 3,500 people per hour\n- Most crowded stations: Bundaran HI and Dukuh Atas\n- Current utilization: 85% of maximum capacity\n\nThe system is designed to handle 20% more volume than current peak loads. Shall I provide more detailed statistics for specific stations?'
    // }
  ]);

  const handleSendMessage = async () => {
    if (!inputMessage.trim()) return;

    // Add user message to chat
    const newMessage = {
      user: inputMessage,
      assistant: "",
    };
    setMessages((prev) => [...prev, newMessage]);
    setIsLoading(true);

    try {
      const message = await anthropic.messages.create({
        model: "claude-3-opus-20240229",
        max_tokens: 100,
        messages: [
          {
            role: "user",
            content: inputMessage,
          },
        ],
      });

      if (message.content && message.content[0]?.text) {
        setMessages((prev) =>
          prev.map((msg, index) =>
            index === prev.length - 1
              ? { ...msg, assistant: message.content[0].text }
              : msg,
          ),
        );
      } else {
        throw new Error("Invalid response format");
      }
    } catch (error) {
      console.error("Error calling Anthropic API:", error);
      setMessages((prev) =>
        prev.map((msg, index) =>
          index === prev.length - 1
            ? {
                ...msg,
                assistant: "Sorry, there was an error processing your request.",
              }
            : msg,
        ),
      );
    } finally {
      setIsLoading(false);
      setInputMessage("");
    }
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter" && !e.shiftKey) {
      e.preventDefault();
      handleSendMessage();
    }
  };

  return (
    <Box sx={{ p: 2, mb: 8, color: "black" }}>
      <Box sx={{ maxWidth: "800px", mx: "auto", mb: 4 }}>
        {messages.map((message, index) => (
          <Fragment key={index}>
            <Box
              sx={{
                display: "flex",
                alignItems: "flex-start",
                gap: 2,
                mb: 3,
                justifyContent: "flex-end",
              }}
            >
              <Box
                sx={{
                  bgcolor: "#1a1a1a",
                  color: "white",
                  p: 2,
                  borderRadius: 2,
                  maxWidth: "80%",
                }}
              >
                <Typography>{message.user}</Typography>
              </Box>
            </Box>
            <Box
              sx={{ display: "flex", alignItems: "flex-start", gap: 2, mb: 2 }}
            >
              <Box
                component="img"
                src="https://gravatar.com/avatar/a1697a0873d095e3f7358229815315ed?s=400&d=robohash&r=x"
                alt="Assistant Avatar"
                sx={{ width: 32, height: 32, borderRadius: "50%" }}
              />
              <Box sx={{ flex: 1 }}>
                <Typography variant="body1" sx={{ mb: 1, color: "black" }}>
                  {message.assistant}
                </Typography>
                <Box sx={{ display: "flex", gap: 1 }}>
                  <IconButton
                    size="medium"
                    sx={{ color: "rgba(0, 0, 0, 0.6)" }}
                  >
                    <CopyOutlined style={{ fontSize: "20px" }} />
                  </IconButton>
                  <IconButton
                    size="medium"
                    sx={{ color: "rgba(0, 0, 0, 0.6)" }}
                  >
                    <LikeOutlined style={{ fontSize: "20px" }} />
                  </IconButton>
                  <IconButton
                    size="medium"
                    sx={{ color: "rgba(0, 0, 0, 0.6)" }}
                  >
                    <DislikeOutlined style={{ fontSize: "20px" }} />
                  </IconButton>
                  <IconButton
                    size="medium"
                    sx={{ color: "rgba(0, 0, 0, 0.6)" }}
                  >
                    <ShareAltOutlined style={{ fontSize: "20px" }} />
                  </IconButton>
                  <IconButton
                    size="medium"
                    sx={{ color: "rgba(0, 0, 0, 0.6)" }}
                  >
                    <ReloadOutlined style={{ fontSize: "20px" }} />
                  </IconButton>
                </Box>
              </Box>
            </Box>
          </Fragment>
        ))}
      </Box>
      <Paper
        elevation={1}
        sx={{
          position: "fixed",
          bottom: 80,
          left: "70%",
          transform: "translateX(-70%)",
          width: "90%",
          maxWidth: "800px",
          bgcolor: "white",
          borderRadius: 2,
          p: 1,
          border: "1px solid rgba(0, 0, 0, 0.12)",
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
          <InputBase
            placeholder={isLoading ? "Thinking..." : "Ask anything..."}
            value={inputMessage}
            onChange={(e) => setInputMessage(e.target.value)}
            onKeyPress={handleKeyPress}
            disabled={isLoading}
            sx={{
              flex: 1,
              color: "black",
              px: 2,
              py: 1,
              "& .MuiInputBase-input": {
                "&::placeholder": {
                  color: isLoading ? "rgba(0, 0, 0, 0.38)" : "inherit",
                },
              },
            }}
          />
          <IconButton
            size="large"
            sx={{
              width: 40,
              height: 40,
              "&:hover": { backgroundColor: "#e0e0e0" },
            }}
          >
            <PaperClipOutlined />
          </IconButton>
          <IconButton
            size="large"
            onClick={handleSendMessage}
            disabled={isLoading || !inputMessage.trim()}
            sx={{
              backgroundColor: "#1a1a1a",
              borderRadius: "50%",
              "&:hover": {
                backgroundColor: "#1a1a1a",
                "& svg": { color: "white" },
              },
              "&.Mui-disabled": {
                backgroundColor: "rgba(0, 0, 0, 0.12)",
              },
              mr: 2,
            }}
          >
            <ArrowRightOutlined style={{ color: "white" }} />
          </IconButton>
        </Box>
      </Paper>
    </Box>
  );
}

export default TaskPage;
```

this is the schema

```json
ENDPOINT: 10.1.69.52:3010/textai/genai
METHOD: POST
HEADER:
 - servicekey: HgXDpHb7k4wupVfsj8hZLUTq
 - Content-Type: application/json

RESPONSE:
{
    "status": "success",
    "value": [
        {
            "type": "text",
            "text": {
                "value": "Sebagai AI BIMA Assistant, saya tidak dapat membuat teks placeholder yang spesifik untuk produk tanpa konteks tertentu. Namun, saya bisa memberikan contoh paragraf panjang yang bisa dijadikan inspirasi untuk placeholder produk:\n\n\"Selamat datang di masa depan inovasi, di mana teknologi bertemu dengan gaya hidup modern untuk menghadirkan sebuah produk yang tidak hanya memudahkan, tapi juga memperkaya kehidupan Anda sehari-hari. Produk kami dirancang dengan perhatian terhadap detail dan komitmen untuk kualitas, memastikan bahwa setiap aspek dari pengalaman Anda dengannya adalah yang terbaik. Dengan fitur-fitur canggih yang dirancang untuk efisiensi dan kemudahan penggunaan, produk kami tidak hanya berfungsi sebagai alat, tapi juga sebagai partner yang dapat diandalkan di setiap langkah Anda. Dari desain yang elegan hingga performa yang tak tertandingi, kami memastikan produk ini menjadi pelengkap sempurna bagi gaya hidup Anda yang dinamis. Dukungan layanan pelanggan kami selalu siap membantu Anda kapan saja, memastikan bahwa setiap interaksi Anda dengan produk kami adalah sesuatu yang menyenangkan. Bergabunglah dengan komunitas pengguna kami yang terus berkembang dan jadilah bagian dari revolusi ini, di mana kenyamanan dan inovasi bersatu dalam harmoni.\"\n\nSemoga paragraf ini dapat membantu sebagai placeholder untuk produk Anda. Jika ada detail spesifik yang ingin ditambahkan, silakan beri tahu saya!",
                "annotations": []
            }
        }
    ]
}
```

i want you to also help me properly render the response such as the \n\n for paragraph or for bolding using 2x asterics (\*\*) such as a markdown file while also saving the chat in the state as for user and assistant entry
