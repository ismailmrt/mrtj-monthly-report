```js
const Renderers = {
  [COLUMN_PRIMITIVE_TYPES.ARRAY]: {
    default: renderArrayCell,
    overrides: {
      STATUS: renderStatusCell,
      TAGS: renderTagsCell,
    },
  },
  [COLUMN_PRIMITIVE_TYPES.BOOLEAN]: {
    default: renderCheckboxCell,
  },
  [COLUMN_PRIMITIVE_TYPES.DATE]: {
    default: renderDateCell,
  },
  [COLUMN_PRIMITIVE_TYPES.DOUBLE]: {
    default: renderNumberCell,
    overrides: {
      CURRENCY: renderCurrencyCell,
      PERCENT: renderPercentCell,
    },
  },
  [COLUMN_PRIMITIVE_TYPES.INT]: {
    default: renderNumberCell,
  },
  [COLUMN_PRIMITIVE_TYPES.OBJECT]: {
    default: renderObjectCell,
    overrides: {
      FILE: renderFileCell,
      LOCATION: renderLocationCell,
      TIMELINE: renderTimelineCell,
      APPROVAL: renderApprovalCell,
    },
  },
  [COLUMN_PRIMITIVE_TYPES.STRING]: {
    default: renderTextCell,
    overrides: {
      EMAIL: renderEmailCell,
      LINK: renderLinkCell,
    },
  },
  [COLUMN_PRIMITIVE_TYPES.TIMESTAMP]: {
    default: renderTimestampCell,
  },
};

function getRenderer(columnType) {
  const { config } = columnType;
  const primitiveRenderer = Renderers[config.primitiveType];

  if (!primitiveRenderer) {
    throw new Error(
      `No renderer found for primitiveType: ${config.primitiveType}`,
    );
  }

  // Check for specific overrides based on columnType
  return (
    primitiveRenderer.overrides?.[columnType.label.toUpperCase()] ||
    primitiveRenderer.default
  );
}

//usage
function renderCell(columnType, value) {
  const renderer = getRenderer(columnType);
  return renderer(value, columnType);
}

// Example usage
const renderedCell = renderCell(COLUMN_TYPES.CURRENCY, 100000.5);
console.log(renderedCell); // Outputs formatted currency cell
```

### other sample

### other sample

### other sample

### other sample

### other sample

### other sample

### other sample

```js
function renderCell(value, column) {
  const { config, type } = column;

  switch (config?.primitiveType) {
    case COLUMN_PRIMITIVE_TYPES.STRING: {
      switch (type) {
        case "EMAIL": {
          return (
            <a href={`mailto:${value || config.defaultValue}`}>
              {value || config.defaultValue}
            </a>
          );
        }
        case "LINK": {
          return (
            <a
              href={value || config.defaultValue}
              target="_blank"
              rel="noopener noreferrer"
            >
              {value || config.defaultValue}
            </a>
          );
        }
        case "TEXT": {
          return <span>{value || config.defaultValue}</span>;
        }
        default:
          return <span>{value || config.defaultValue}</span>;
      }
    }

    case COLUMN_PRIMITIVE_TYPES.BOOLEAN: {
      if (type === "CHECKBOX") {
        return (
          <input
            type="checkbox"
            checked={value ?? config.defaultValue}
            readOnly
          />
        );
      }
      return <span>{value ? "True" : "False"}</span>;
    }

    case COLUMN_PRIMITIVE_TYPES.DATE: {
      if (type === "DATE") {
        const formattedDate = value
          ? formatDate(value, config.defaultFormat || "DD/MM/YYYY")
          : "N/A";
        return <span>{formattedDate}</span>;
      }
      return <span>{value || config.defaultValue}</span>;
    }

    case COLUMN_PRIMITIVE_TYPES.DOUBLE: {
      switch (type) {
        case "CURRENCY": {
          const prefix = config.defaultPrefix || "";
          return <span>{`${prefix}${value ?? config.defaultValue}`}</span>;
        }
        case "PERCENT": {
          return (
            <span>{`${value ?? config.defaultValue}${config.suffix || "%"}`}</span>
          );
        }
        default:
          return <span>{value ?? config.defaultValue}</span>;
      }
    }

    case COLUMN_PRIMITIVE_TYPES.INT: {
      if (type === "NUMBERS") {
        return <span>{value ?? config.defaultValue}</span>;
      }
      return <span>{value || config.defaultValue}</span>;
    }

    case COLUMN_PRIMITIVE_TYPES.OBJECT: {
      switch (type) {
        case "FILE": {
          return (
            <span>{value ? JSON.stringify(value) : "No File Attached"}</span>
          );
        }
        case "LOCATION": {
          return (
            <span>
              {value
                ? `Lat: ${value.lat}, Lng: ${value.lng}`
                : "Location Not Set"}
            </span>
          );
        }
        case "TIMELINE": {
          return (
            <span>
              {value
                ? `Start: ${value.start || "N/A"}, End: ${value.end || "N/A"}`
                : "Timeline Not Set"}
            </span>
          );
        }
        case "APPROVAL": {
          return (
            <ul>
              {value?.map((approver, index) => (
                <li key={index}>
                  {`Approver: ${approver.approver || "Unknown"}, Status: ${
                    approver.status || "Pending"
                  }`}
                </li>
              )) || <li>No Approvals</li>}
            </ul>
          );
        }
        default:
          return <span>{JSON.stringify(value ?? config.defaultValue)}</span>;
      }
    }

    case COLUMN_PRIMITIVE_TYPES.ARRAY: {
      switch (type) {
        case "PEOPLE": {
          return (
            <ul>
              {value?.map((person, index) => (
                <li key={index}>{`Person ID: ${person}`}</li>
              )) || <li>No People Assigned</li>}
            </ul>
          );
        }
        case "STATUS": {
          const statusOption = config.options?.find(
            (opt) => opt.name === value,
          );
          return (
            <span style={{ color: statusOption?.color || "#000" }}>
              {value || config.defaultValue}
            </span>
          );
        }
        case "TAGS": {
          return (
            <ul>
              {value?.map((tag, index) => <li key={index}>{tag}</li>) || (
                <li>No Tags</li>
              )}
            </ul>
          );
        }
        default:
          return (
            <ul>
              {value?.map((item, index) => <li key={index}>{item}</li>) || (
                <li>No Items</li>
              )}
            </ul>
          );
      }
    }

    case COLUMN_PRIMITIVE_TYPES.OBJECT_ID: {
      return <span>{value || "Unknown ID"}</span>;
    }

    case COLUMN_PRIMITIVE_TYPES.TIMESTAMP: {
      const formattedTimestamp = value
        ? new Date(value).toLocaleString()
        : "Invalid Timestamp";
      return <span>{formattedTimestamp}</span>;
    }

    default:
      return <span>Unsupported Type</span>;
  }
}
```
