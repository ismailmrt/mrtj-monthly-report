Perubahan SO

1. Dep CRM (Dir Operasi & Pemeliharaan, Div CED) ID-25
   - move to:
     - dir UT
     - div CORSEC
     - dep Stakeholder Relationship Management (SRM)
     - member (add):
       - Agung Sampurno
   - member:
     - efrangky (Ka Dep)
     - afendy (GRC Officer)
2. Dep Program & Partnership Development (Dir Operasi & Pemeliharaan, Div CED) ID-116
   - move to:
     - dir BIDEV
     - div BE
     - dep Program & Partnership
   - member
     - psamuel (Ka Dep)
     - rtasya (GRC Officer)
3. Dep Supply Chain Management (Dir Operasi & Pemeliharaan, Div RSMTC) ID-62
   - move to:
     - dir Finance
     - div SPAM
     - dep Supply-chain Planning & Logistic
     - member (add):
       - Dhewi
   - member
     - dghea (GRC Officer)

- perpindahan dokumen risiko (risk assesment)
- list risiko di monitor
- approval pastikan pindah ke kadiv baru
