# Risk monitor bug

Case:

1. Departemen/divisi pindah direktorat
2. Officer pindah departemen/divisi

Reference:

- Blueprint MIRA
- Software Architecture MIRA
  - Risk Monitor

Analysis:

1. Case-1

- Initial Condition:
  - No Ka Div assigned at Finance > IST
  - Latest RSCA is (docs)[http://localhost:8080/risk/rcsa/detail/b332ab0c216dc776563bc63a4e287555:2df886ec]
    which is approved to the last Ka Div (Anggadanu Dwi) but the Ka Dep signature was NULL
  - One of the early RSCA is (docs)[http://localhost:8080/risk/rcsa/detail/b332ab0c216dc776563bc63a4e287555:2ef088]
    the status is already at final stage everything was approved but there's no Ka Dep/RMQA Admin/Da Dev signature
- Assign self to Finance > IST > IT Dev Department (agi as kadep) as GRC Officer (rismail)
- Create new user, assign to Finance > IST as Ka Div (pdian)
- Create new user, duplicated directly in the DB from above user (rfikri)
  - The user is also assigned as the Ka Div of IST
  - Then when checking the Department master data, division PIC is pdian & rfikri.
    Division PIC is queried (controller/master/divisi.js) based on the user role and the assigned Department
- Create new Department (Bisdev IT - BIT), assign to BISDEV > IST
  - There's no PIC assigned since it's new
  - There's `is_div` field, assuming that it's a dev defined field since there's nowhere to be found of its assignment
  - Assign (zarsy) as Ka Dep
  - However this new Department is only accessible trough dropdown when only selecting Finance Directorate even though
    it's assigned to BISDEV directorate
- Create Risk Assessment RCSA docs (rismail - Finance, IST, IT Dev Department)
  - Need review from Ka Dep (agi) and Admin RMQA (any: wlufta)
  - zarsy can see and comment, no comment
  - agi can see and comment, comment, reviewed by agi, user kadep [137]
  - zarsy then comment, user kadep is now array [137, 673]
  - wlufta then review, approved
  - rismail then revised and submitted, waiting for kadiv
  - pdian and rfikri can see
  - rfikri ask for revise, revised by rismail
  - risk is approved and cancelled by pdian
  - risk is approved and finalized by rfikri
  - Final document status is finalized. Inspected by both agi, zarsy. Reviewed by wlufta and approved by rfikri
- Raise Risk Monitor for the previously created Risk
  - select risk to raise, make necessary changes
  - review from Ka Dep and Admin RMQA
  - revised
  - Ka Div approval and finalization
  - document is accessible trough "List Dokumen" from GRC Officer role
- Raise another Risk monitor
  - select risk to raise, make necessary changes
  - !!!BREAKING CHANGES --> move IT Dev Department to BISDEV > BEI from Finance > IST
    - from: dir_id 5 (FCM), div_id 18 (IST), dep_id 144 (IST) -- 52 (ITD) | dep risk code is bounded to div risk code
    - to: dir_id 4 (BIDEV), div_id 13 (BEI), dep_id 139 (BEI) -- 52 (ITD) |
  - now GRC Officer can't access the raised Risk Monitor
  - Ka Dep and Admin RMQA still can review
    - Ka Dep is directly assigned to the corresponding Risk Monitor
    - Admin RMQA must select the raised Risk, and it's still registered at Finance Directorate
  - review from Ka Dep and Admin RMQA
  - GRC Officer can't make any revision or go back to its raised Risk because of diff in Directorate and Division
  - FIXING - try to change mrt risk monitor to new dir and div id
    main event: perubahan SO

2. Case-2

Strategy:

- Direct feature fix branch on repo, no strangler pattern
- Analyze dependency or intersecting features
- To patch data first then deploy the feature

Expected output:

- UX for notifying user that there's maintenance ongoing

Target release:
22 June 2024.

TODO:

- [ ] create scenario, steps to reproduce each case
- [ ] monitor data flow and analyze data dependency

- mira development server
- for more agile testing, UAT
- perubahan SO
- most likely repo MIRA ini untuk ngebuat draft enhancement
- target dari user bulan juli digunakan karena per semester
