# max_prepared_stmt_count Issue on Production

## Overview

There are two error logs provided, both relating to the `max_prepared_stmt_count` exception.
![Issue A](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2406/issue-A.jpg)
![Issue B](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2406/issue-B.jpg)

The current value of `max_prepared_stmt_count` is set to 16382 (default).

## Findings

### Issue A

- The stack trace shows the following SQL query:

  ```sql
  SELECT mt_rta_risiko.rta_risiko_id as risiko_id, ...
  FROM mt_rta_risiko
  WHERE 1 = 1
    AND mt_rta_risiko.rta_risiko_status = '7'
    AND mt_rta_risiko.rta_close_risk = '0'
    AND mt_rta_risiko.div = '18'
    AND mt_rta_risiko.dep_id = '144'
    AND mt_rta_risiko.istrash = '0'
  ORDER BY rta_risiko_rra_score DESC
  ```

  Using a `grep` search, this query is referenced in `models/rta.js` -> `getByParamScoreRisiko`.

- However, the stack trace above refers to its usage in `models/bra.js` -> `getByParamScoreRisiko`.
- It is implemented @ `controllers/risk/bra.js` -> `getDataListRisiko`, as also reported in the stack trace.
- The stack trace continues to `middleware/is-auth.js` -> `authLogin`.

### Issue B

- The stack trace shows two SQL queries:

  - `INSERT`:
    ```sql
    INSERT INTO mt_rta_log (rta_doc_id, rta_risiko_id, rta_mitigasi_id, user_id, log_action, log_status, log_comment, log_date)
    VALUES (...)
    ```
  - `SELECT`:
    ```sql
    SELECT * FROM mt_rta_mitigasi WHERE (rta_mitigasi_id = ?) LIMIT 1 OFFSET 0
    ```

  These two SQL statements are implemented in a helper/util class: `models/master.js` -> `findSingle`

  Stack trace calls:

  - `controllers/risk/rta.js` -> `postFormSaveRisiko`

## Expectation

The error is identified and resolved.

## Analysis

The error `max_prepared_stmt_count` indicates that the application is exceeding the limit of prepared statements allowed by MySQL
which is currently set to 16382 (default). This can occur when prepared statements are not being properly closed or
when there are too many simultaneous prepared statements.

There is an issue raised on [GitHub](https://github.com/sidorares/node-mysql2/issues/702) for `mysql2` implementation similar to this project.
The problem occurs when encountering a MySQL error due to reaching the maximum limit for prepared statements.

Their implementation of `mysql2` is similar to `MIRA` where they use the defined pool throughout their project.

There are at least 30 files in `MIRA` that call the implementation of `mysql2` pool from `utils/database`,
all wrapping the implementation with statements like:

```javascript
db.execute(query);

// Specifically in models/master.js with this block
static query(sql) {
  return db.execute(sql);
}
```

As the library author replied, the issue is likely caused by using `execute()` with inline variable injection
instead of placeholders and parameters, leading to the preparation of new statements each time.
Solutions suggested include:

1. Adjust the MySQL configuration by setting `max_prepared_stmt_count` to a higher value.
2. Refactor code to use placeholders for variables in prepared statements to prevent creating excessive unique statements.
3. Avoid using prepared statements by using `pool.query('SELECT * FROM ...')`.

## Comparison to current Implementation

Current implementation directly concatenates parameter values into the SQL string:

```javascript
if (p["rta_risk_id"] && p["rta_risk_id"] !== "") {
  sql += " AND mt_rta_risiko.rta_risk_id = '" + p["rta_risk_id"] + "'";
}
// Similar concatenation for other parameters
```

- Current implementation dynamically constructs an SQL query based on input parameters (p).
- The code does not use explicit placeholders (?) for parameter binding.
- Each time a query is constructed and executed with different parameter values,
  MySQL treats it as a new statement, which increases the count of prepared statements.
- The execution of the query is handled by db.execute.
- This approach is similar to the problematic example
  provided by the author (`conn.execute('select ' + (+new Date()) + ' as timestamp')`).
- Despite this, the code achieves a similar effect by dynamically constructing the SQL query based on the input parameters.

## Enhancement

The short and simple solution is to **_increase the limit_**.
However, this does not guarantee that this issue will not occur again.
Therefore, a code refactor must take place to ensure long-term stability.

After discussing with the Development Lead @ Mas Fikri,
we have identified that refactoring the code might present significant challenges.
The current implementation spans over at least 30 files, each containing hundreds
or even thousands of lines of code. Therefore, refactoring would be a substantial
undertaking and involve extensive changes.

To address this issue @ Mas Fikri suggested enhancing the efficiency of our SQL query execution.
One potential solution we could explore is as follows:

```javascript
async function executeQuery(query, params) {
  const connection = await db.getConnection();
  try {
    const [rows, fields] = await connection.execute(query, params);
    return rows;
  } finally {
    connection.release();
  }
}
```

To validate whether this approach can effectively solve our problem, we first need to measure
the increase in the stmt_count associated with user actions. For instance, we should determine
the stmt_count when a user navigates to the risk monitor menu. Using this data, we can benchmark
the new solution to demonstrate its improved efficiency.

### Prepared Statement Count Log

```sql
SHOW GLOBAL STATUS LIKE 'prepared_stmt_count';
```

| User                    | Action                         | Count Change | Total Count |
| ----------------------- | ------------------------------ | ------------ | ----------- |
| **super-admin**         | /login                         | +1           | 1           |
|                         | /dashboard                     | +14          | 15          |
|                         | - Refresh 3x                   | 0            | 15          |
|                         | /risk-monitor/fmea             | +3           | 18          |
|                         | - Refresh 3x                   | 0            | 18          |
|                         | /risk-monitor/rta              | +1           | 19          |
|                         | /risk-monitor/rcsa             | +1           | 20          |
|                         | /logout                        | 0            | 20          |
|                         | /login                         | 0            | 20          |
|                         | /logout                        | 0            | 20          |
|                         | /login                         | 0            | 20          |
|                         | /logout                        | 0            | 20          |
| **wlufta (admin RMQA)** | /login                         | +1           | 21          |
|                         | /dashboard                     | +25          | 46          |
|                         | - Refresh 3x                   | +52          | 98          |
|                         | /risk-monitor/fmea             | +19          | 117         |
|                         | /risk-monitor/rcsa             | +13          | 130         |
|                         | /risk-monitor/rcsa/list-risiko | +49          | 179         |
|                         | - Refresh 1x                   | +23          | 202         |
|                         | - Apply filter (status)        | +6           | 208         |
|                         | - Remove filter (status)       | +26          | 234         |
|                         | - Apply filter (divisi)        | +11          | 245         |
|                         | /logout                        | +2           | 247         |
|                         | /login                         | +23          | 270         |
|                         | /logout                        | +1           | 271         |
|                         | /login                         | +15          | 286         |
|                         | /logout                        | +1           | 287         |
|                         | After idle for 2 days          | -287         | 0           |

This table summarizes the changes in the `prepared_stmt_count` status based on the actions
taken by the users "super-admin" and "wlufta (admin RMQA)".
