```json
{
  "label": "Text",
  "type": "TEXT",
  "description": "Simple text holder",
  "config": {
    "primitiveType": 7,
    "defaultValue": ""
  },
  "state": 0,
  "position": 0,
  "_id": "6761431af4e4e7f696c7cad9",
},
{
  "state": 0,
  "_id": "6761431af4e4e7f696c7cada",
  "label": "People",
  "description": "Represents a person(s) who is assigned to the row",
  "config": {
    "primitiveType": 0,
    "defaultValue": []
  },
  "position": 1,
  "type": "PEOPLE"
},

<!--PREVIOUS VER-->
{
  "label": "Approval",
  "type": "APPROVAL",
  "description": "Represents a person(s) who should approve the row",
  "config": {
    "primitiveType": 5,
    "defaultValue": {
      "method": 0,
      "approvers": []
    }
  },
  "state": 0,
  "position": 0,
  "_id": "676279094e73c71edbfa4b52"
},
<!--PREVIOUS VER-->

<!--NEW VER-->
{
  "label": "Approval",
  "type": "APPROVAL",
  "description": "Represents a person(s) who should approve the row",
  "config": {
    "primitiveType": 5,
    "defaultValue": {
      "method": 0,
      "approvers": []
    },
    "options": [
      "6765551c4e73c71edbfa539d"
    ]
  },
  "state": 0,
  "position": 0,
  "_id": "676279094e73c71edbfa4b52"
},
<!--NEW VER-->

{
  "label": "Status",
  "type": "STATUS",
  "description": "Represents a label designation for your row(s)",
  "config": {
    "primitiveType": 0,
    "defaultValue": "Backlog",
    "options": [
      {
          "name": " ",
          "color": "#cbcbcc"
      },
      {
          "name": "Backlog",
          "color": "#ff6666"
      },
      {
          "name": "To Do",
          "color": "#3f51b5"
      },
      {
          "name": "In Progress",
          "color": "#ffc107"
      },
      {
          "name": "Done",
          "color": "#8bc34a"
      }
    ]
  },
  "state": 0,
  "position": 0,
  "_id": "6765551c4e73c71edbfa539d"
}







<!--COLUMN CONFIG-->
[
    {
        "state": 0,
        "_id": "0",
        "label": "Task",
        "description": "Row name",
        "type": "ROW",
        "config": {
            "primitiveType": 7,
            "defaultValue": "Task"
        }
    },
    {
        "state": 0,
        "_id": "6761430ef4e4e7f696c7cac3",
        "label": "Text",
        "description": "Simple text holder",
        "config": {
            "primitiveType": 7,
            "defaultValue": ""
        },
        "position": 0,
        "type": "TEXT"
    },
    {
        "state": 0,
        "_id": "6761430ef4e4e7f696c7cac4",
        "label": "People",
        "description": "Represents a person(s) who is assigned to the row",
        "config": {
            "primitiveType": 0,
            "defaultValue": []
        },
        "position": 1,
        "type": "PEOPLE"
    },
    {
        "label": "Status",
        "type": "STATUS",
        "description": "Represents a label designation for your row(s)",
        "config": {
            "primitiveType": 0,
            "defaultValue": "Backlog",
            "options": [
                {
                    "name": " ",
                    "color": "#cbcbcc"
                },
                {
                    "name": "Backlog",
                    "color": "#ff6666"
                },
                {
                    "name": "To Do",
                    "color": "#3f51b5"
                },
                {
                    "name": "In Progress",
                    "color": "#ffc107"
                },
                {
                    "name": "Done",
                    "color": "#8bc34a"
                }
            ]
        },
        "state": 0,
        "position": 0,
        "_id": "6776436875db2e1223d707bd"
    },
    {
        "label": "Date",
        "type": "DATE",
        "description": "Represents a specific date (and potentially time)",
        "config": {
            "primitiveType": 2,
            "defaultValue": "2025-01-02T02:07:13.802Z",
            "defaultFormat": "DD/MM/YYYY",
            "defaultTimeFormat": "HH:mm"
        },
        "state": 0,
        "position": 0,
        "_id": "677643dd75db2e1223d707de"
    },
    {
        "label": "S2J",
        "type": "APPROVAL",
        "description": "Represents a person(s) who should approve the row",
        "config": {
            "primitiveType": 5,
            "defaultValue": {
                "method": 0,
                "approvers": []
            },
            "options": [
                "678486fa154a531c2c106891",
                "67848889154a531c2c1069a9"
            ]
        },
        "state": 0,
        "position": 0,
        "_id": "678486f3154a531c2c106879"
    },
    {
        "label": "Numbers",
        "type": "NUMBERS",
        "description": "Holds number values (Integer)",
        "config": {
            "primitiveType": 4,
            "defaultValue": 0
        },
        "state": 0,
        "position": 0,
        "_id": "678486fa154a531c2c106891"
    },
    {
        "label": "Timeline",
        "type": "TIMELINE",
        "description": "Timeline with start and end dates",
        "config": {
            "primitiveType": 5,
            "defaultValue": {
                "start": null,
                "end": null
            }
        },
        "state": 0,
        "position": 0,
        "_id": "67848889154a531c2c1069a9"
    },
    {
        "label": "Numbers",
        "type": "NUMBERS",
        "description": "Holds number values (Integer)",
        "config": {
            "primitiveType": 4,
            "defaultValue": 0
        },
        "state": 0,
        "position": 0,
        "_id": "67848898154a531c2c1069fe"
    },
    {
        "label": "Approval",
        "type": "APPROVAL",
        "description": "Represents a person(s) who should approve the row",
        "config": {
            "primitiveType": 5,
            "defaultValue": {
                "method": 0,
                "approvers": []
            },
            "options": [
                "6784894c154a531c2c106a6a"
            ]
        },
        "state": 0,
        "position": 0,
        "_id": "67848948154a531c2c106a4a"
    },
    {
        "label": "Teeeext",
        "type": "TEXT",
        "description": "Simple text holder",
        "config": {
            "primitiveType": 7,
            "defaultValue": ""
        },
        "state": 0,
        "position": 0,
        "_id": "6784894c154a531c2c106a6a"
    },
    {
        "label": "Text",
        "type": "TEXT",
        "description": "Simple text holder",
        "config": {
            "primitiveType": 7,
            "defaultValue": ""
        },
        "state": 0,
        "position": 0,
        "_id": "67848950154a531c2c106a8c"
    }
]






<!--THE DISABLED COLUMN ID-->
[
    "678486fa154a531c2c106891",
    "67848889154a531c2c1069a9",
    "6784894c154a531c2c106a6a"
]









<!--COLUMN VALUE-->
{
    "_id": "6777459a75db2e1223d708b8",
    "values": [
        {
            "id": "6777459a75db2e1223d708b8",
            "type": "ROW",
            "config": {
                "primitiveType": 7,
                "defaultValue": "Task"
            },
            "value": "Task satu",
            "previousValue": null
        },
        {
            "id": "6761430ef4e4e7f696c7cac3",
            "type": "TEXT",
            "config": {
                "primitiveType": 7,
                "defaultValue": ""
            },
            "value": "",
            "previousValue": ""
        },
        {
            "id": "6761430ef4e4e7f696c7cac4",
            "type": "PEOPLE",
            "config": {
                "primitiveType": 0,
                "defaultValue": []
            },
            "value": [],
            "previousValue": null
        },
        {
            "id": "6776436875db2e1223d707bd",
            "type": "STATUS",
            "config": {
                "primitiveType": 0,
                "defaultValue": "Backlog",
                "options": [
                    {
                        "name": " ",
                        "color": "#cbcbcc"
                    },
                    {
                        "name": "Backlog",
                        "color": "#ff6666"
                    },
                    {
                        "name": "To Do",
                        "color": "#3f51b5"
                    },
                    {
                        "name": "In Progress",
                        "color": "#ffc107"
                    },
                    {
                        "name": "Done",
                        "color": "#8bc34a"
                    }
                ]
            },
            "value": "Backlog",
            "previousValue": null
        },
        {
            "id": "677643dd75db2e1223d707de",
            "type": "DATE",
            "config": {
                "primitiveType": 2,
                "defaultValue": "2025-01-02T02:07:13.802Z",
                "defaultFormat": "DD/MM/YYYY",
                "defaultTimeFormat": "HH:mm"
            },
            "value": "2025-01-02T02:07:13.802Z",
            "previousValue": null
        },
        {
            "id": "678486f3154a531c2c106879",
            "type": "APPROVAL",
            "config": {
                "primitiveType": 5,
                "defaultValue": {
                    "method": 0,
                    "approvers": []
                },
                "options": [
                    "678486fa154a531c2c106891",
                    "67848889154a531c2c1069a9"
                ]
            },
            "value": {
                "method": 0,
                "approvers": [
                    {
                        "email": "johndoe@mail.com",
                        "name": "John Doe",
                        "avatar": "https://gravatar.com/avatar/a1697a0873d095e3f7358229815315ed?s=400&d=robohash&r=x",
                        "status": false
                    }
                ]
            },
            "previousValue": null
        },
        {
            "id": "678486fa154a531c2c106891",
            "type": "NUMBERS",
            "config": {
                "primitiveType": 4,
                "defaultValue": 0
            },
            "value": 1000,
            "previousValue": 1000
        },
        {
            "id": "67848889154a531c2c1069a9",
            "type": "TIMELINE",
            "config": {
                "primitiveType": 5,
                "defaultValue": {
                    "start": null,
                    "end": null
                }
            },
            "value": {
                "start": null,
                "end": null
            },
            "previousValue": null
        },
        {
            "id": "67848898154a531c2c1069fe",
            "type": "NUMBERS",
            "config": {
                "primitiveType": 4,
                "defaultValue": 0
            },
            "value": 0,
            "previousValue": 0
        },
        {
            "id": "67848948154a531c2c106a4a",
            "type": "APPROVAL",
            "config": {
                "primitiveType": 5,
                "defaultValue": {
                    "method": 0,
                    "approvers": []
                },
                "options": [
                    "6784894c154a531c2c106a6a"
                ]
            },
            "value": {
                "method": 0,
                "approvers": []
            },
            "previousValue": null
        },
        {
            "id": "6784894c154a531c2c106a6a",
            "type": "TEXT",
            "config": {
                "primitiveType": 7,
                "defaultValue": ""
            },
            "value": "",
            "previousValue": ""
        },
        {
            "id": "67848950154a531c2c106a8c",
            "type": "TEXT",
            "config": {
                "primitiveType": 7,
                "defaultValue": ""
            },
            "value": "",
            "previousValue": null
        }
    ]
}
```
