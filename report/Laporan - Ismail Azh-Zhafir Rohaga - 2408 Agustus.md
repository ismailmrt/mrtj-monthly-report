# August 2024 Report

_Reported by: [Ismail Azh-Zhafir Rohaga](https://ismailrohaga.github.io)_

## Summary

In August 2024, significant progress was made across various project aspects. Key achievements included the successful implementation and integration of essential services such as board, workspace, column, row, and team, with a rocus on refining CRUD functionality and routing. Code quality was notably enhanced through refactoring efforts and the addition of comprehensive JSDoc comments. The PRD for Monday Martidocs was drafted, reviewed, and finalized. Noteworthy improvements were also made to schema and service architecture, including the definition and implementation of the ColumnTypes schema.

## Key Achievements

1. Successful Implementation:

   - Completed and integrated key services (board, workspace, column, row, team) with refined routing and CRUD operations.

![Postman](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2408/postman.png)
Fig. 1: Postman Collection

2. Enhanced Code Quality:

   - Refactored multiple code components for improved maintainability and readability.
   - Added comprehensive documentation (JSDoc) across the codebase.

![The Code](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2408/code.png)
Fig. 2: Sample JSDoc (on the left pane) and adding constants.js (on the right)

3. Documentation and Reporting Excellence:

   - Finalized and documented the PRD for Monday Martidocs.

![PRD Artifacts](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2408/prd.png)
Fig. 3: Access the prd [here](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/blob/main/analysis/martidocs-prd-monday.md)

4. Schema and Architecture Improvements:
   - Defined and implemented the ColumnTypes schema effectively.
   - Proposed the enhanced service layer architecture to support new features and improvements.

![Diagram](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2408/arch.png)
Fig. 3: Draft service layer architecture diagram

## Notes

## Detailed Daily Activities

### Thu, 01/08/24

1. 08:00 - 09:00 = Initialize timesheet
, Bind board routing to controller
3. 10:00 - 11:00 = Temporarily remove board validator
4. 11:00 - 12:00 = Finalize workspace service implementation
5. 13:00 - 14:00 = Include days routing at board routes
6. 14:00 - 15:00 = Implement days service at board controller
7. 15:00 - 16:00 = Use req.params for boards routing
8. 16:00 - 17:00 = Finalize board service implementation

### Fri, 02/08/24

1. 08:00 - 09:00 = Draft column service
, Define column service routes
3. 10:00 - 11:00 = Define column service at board controller
4. 11:00 - 12:00 = Define column service schema
5. 13:00 - 14:00 = Refactor board and workspace services
6. 14:00 - 15:00 = Apply validation tweaks to existing routes
7. 15:00 - 16:00 = Integrate column service with board controllers
8. 16:00 - 17:00 = Finish all models, routes and controllers

### Mon, 05/08/24

1. 08:00 - 09:00 = Add rows service impl
, Refactor routes and models for consistency
3. 10:00 - 11:00 = Update controllers to handle new schema changes
4. 11:00 - 12:00 = Debug and test all routes
5. 13:00 - 14:00 = fix query destructure
6. 14:00 - 15:00 = Debug and test all routes
7. 15:00 - 16:00 = remove mongoose validId
8. 16:00 - 17:00 = Debug and test all routes

### Tue, 06/08/24

1. 08:00 - 09:00 = Validate created service
, Apply changes to the previously implemented routes
3. 10:00 - 11:00 = Refactor controllers
4. 11:00 - 12:00 = Fix bugs from testing phase
5. 13:00 - 14:00 = Apply minor tweaks to models and controllers
6. 14:00 - 15:00 = Review implementations
7. 15:00 - 16:00 = Debug and test all routes
8. 16:00 - 17:00 = Final review of all implementations

### Wed, 07/08/24

1. 08:00 - 09:00 = Adjust board service
, Review code for consistency and quality
3. 10:00 - 11:00 = Fix any found issues during testing
4. 11:00 - 12:00 = Conduct review for board controllers
5. 13:00 - 14:00 = List possible improvements for board controllers
6. 14:00 - 15:00 = Weekly meeting
7. 15:00 - 16:00 = Timesheet
8. 16:00 - 17:00 = Timesheet

### Thu, 08/08/24

1. 08:00 - 09:00 = Bug fix
, Refactor code to improve maintainability
3. 10:00 - 11:00 = Refactor code to improve maintainability
4. 11:00 - 12:00 = Refactor controllers to improve readability
5. 13:00 - 14:00 = Test and verify all fixes
6. 14:00 - 15:00 = Review workspace service integration
7. 15:00 - 16:00 = Refactor board controller functions
8. 16:00 - 17:00 = Refactor board controller functions

### Fri, 09/08/24

1. 08:00 - 09:00 = Work on Row service
, Debug issues in row service
3. 10:00 - 11:00 = Test row service implementation
4. 11:00 - 12:00 = Bind row service with routes
5. 13:00 - 14:00 = Refactor existing code for better structure
6. 14:00 - 15:00 = Apply fixes to related routes and controllers
7. 15:00 - 16:00 = Final testing of row service
8. 16:00 - 17:00 = Submit contract

### Mon, 12/08/24

1. 08:00 - 09:00 = Review previous week's work
, Meeting with Pak Dian and Mas Fikri for Martidocs Monday DB Schema
3. 10:00 - 11:00 = Writing meeting recap
4. 11:00 - 12:00 = Update models based on new schema
5. 13:00 - 14:00 = Permission to father's office for family urgency
6. 14:00 - 15:00 = Adjust collection
7. 15:00 - 16:00 = Review meeting recap
8. 16:00 - 17:00 = Timesheet

### Tue, 13/08/24

1. 08:00 - 09:00 = Timesheet
, Update PPT to include meeting recap
3. 10:00 - 11:00 = Assist mas Najmy in running MIRA project
4. 11:00 - 12:00 = Help to upgrade docker engine
5. 13:00 - 14:00 = Weekly meeting
6. 14:00 - 15:00 = List all last week's activities
7. 15:00 - 16:00 = Review all last week's activities
8. 16:00 - 17:00 = Timesheet

### Wed, 14/08/24

1. 08:00 - 09:00 = Timesheet
, Draft refactor list
3. 10:00 - 11:00 = Refactor rows modles as days
4. 11:00 - 12:00 = Research mongoose dynamic collection schema for days
5. 13:00 - 14:00 = Adding JSDoc to all models
6. 14:00 - 15:00 = Adding JSDoc to all models
7. 15:00 - 16:00 = Update planner
8. 16:00 - 17:00 = Timesheet

### Thu, 15/08/24

1. 08:00 - 09:00 = Code review & Define today's goals
, Implement `secure.js` from `TemplateCodeBE`
3. 10:00 - 11:00 = Review Implementation
4. 11:00 - 12:00 = Review CRUD Implementation (Team and Workspace done)
5. 13:00 - 14:00 = Seperate pagination functionality from controller
6. 14:00 - 15:00 = Implement pagination functionality
7. 15:00 - 16:00 = Review teamController implementation
8. 16:00 - 17:00 = Add RFC for error handling

### Fri, 16/08/24

1. 08:00 - 09:00 = Timesheet
, Review team CRUD Ipmlementation
3. 10:00 - 11:00 = Adjust constants values, update models and controllers
4. 11:00 - 12:00 = Review Team `getTeamMembers` implementation
5. 13:00 - 14:00 = Research schema pagination for field typed arrays
6. 14:00 - 15:00 = Implement pagination for field typed arrays
7. 15:00 - 16:00 = Review teamController implementation
8. 16:00 - 17:00 = Timesheet

### Mon, 19/08/24

1. 08:00 - 09:00 = Timesheet
, Review workspace service implementation
3. 10:00 - 11:00 = Review and refactor workspace service
4. 11:00 - 12:00 = Review board service implementation
5. 13:00 - 14:00 = Review and refactor board service
6. 14:00 - 15:00 = Review board columns and days service implementation
7. 15:00 - 16:00 = Review and refactor board columns service
8. 16:00 - 17:00 = Review and refactor board days service

### Tue, 20/08/24

1. 08:00 - 09:00 = Timesheet
, Review row service implementation
3. 10:00 - 11:00 = Review and refactor row service
4. 11:00 - 12:00 = Continue refactoring row service
5. 13:00 - 14:00 = Adjust row service implementation
6. 14:00 - 15:00 = Review row service with adjustment
7. 15:00 - 16:00 = Finalize row service refactoring
8. 16:00 - 17:00 = Share progress with lead (Mas Fikri)

### Wed, 21/08/24

1. 08:00 - 09:00 = Timesheet and review tasks
, Implement changes to row service
3. 10:00 - 11:00 = Implement changes to row service
4. 11:00 - 12:00 = Progress adjustment row get & create
5. 13:00 - 14:00 = Finalize adjustments on row service
6. 14:00 - 15:00 = Weekly meeting
7. 15:00 - 16:00 = Review remaining tasks for row service
8. 16:00 - 17:00 = Plan next steps for row service

### Thu, 22/08/24

1. 08:00 - 09:00 = Mapping arch, row collection
, Draft app arch to add service layer
3. 10:00 - 11:00 = Identify service layer
4. 11:00 - 12:00 = Draft service layer
5. 13:00 - 14:00 = Finish CRUD MVP Implementation
6. 14:00 - 15:00 = Test all API
7. 15:00 - 16:00 = Review CRUD MVP Implementation
8. 16:00 - 17:00 = Timesheet

### Fri, 23/08/24

1. 08:00 - 09:00 = Define PRD for monday martidocs
, Define PRD for monday martidocs
3. 10:00 - 11:00 = Review PRD for monday martidocs
4. 11:00 - 12:00 = Outline PRD for monday martidocs
5. 13:00 - 14:00 = Review and refine PRD for monday martidocs
6. 14:00 - 15:00 = Work on feature specification details
7. 15:00 - 16:00 = Document feature specification details
8. 16:00 - 17:00 = Research potential improvements

### Mon, 26/08/24

1. 08:00 - 09:00 = Timesheet
, Reporting
3. 10:00 - 11:00 = Continue working on PRD for monday martidocs
4. 11:00 - 12:00 = Continue working on PRD for monday martidocs
5. 13:00 - 14:00 = Review PRD for monday martidocs
6. 14:00 - 15:00 = Finalize PRD for monday martidocs
7. 15:00 - 16:00 = Row service bug fixing
8. 16:00 - 17:00 = Finalize bug fixing row service

### Tue, 27/08/24

1. 08:00 - 09:00 = Timesheet
, Reporting
3. 10:00 - 11:00 = Continue working on PRD for monday martidocs
4. 11:00 - 12:00 = Continue working on PRD for monday martidocs
5. 13:00 - 14:00 = Weekly meeting
6. 14:00 - 15:00 = Convert md to spreadsheet
7. 15:00 - 16:00 = Review timesheet
8. 16:00 - 17:00 = Review reporting

### Wed, 28/08/24

1. 08:00 - 09:00 = Timesheet
, Draft ColumnTypes primitive types
3. 10:00 - 11:00 = Draft ColumnTypes schema
4. 11:00 - 12:00 = Analyze monday work management ColumnTypes schema
5. 13:00 - 14:00 = Define ColumnType schema
6. 14:00 - 15:00 = Draft ColumnTypes implementation
7. 15:00 - 16:00 = Review monday service routing
8. 16:00 - 17:00 = Timesheet

### Thu, 29/08/24

1. 08:00 - 09:00 = Timesheet
, Draft ColumnTypes primitive types
3. 10:00 - 11:00 = Draft ColumnTypes schema
4. 11:00 - 12:00 = Analyze monday work management ColumnTypes schema
5. 13:00 - 14:00 = Define ColumnType schema
6. 14:00 - 15:00 = Draft ColumnTypes implementation
7. 15:00 - 16:00 = Review monday service routing
8. 16:00 - 17:00 = Timesheet

### Fri, 30/08/24

1. 08:00 - 09:00 = Finalize timesheet and reporting
, Submit timesheet and monthly report
3. 10:00 - 11:00 = Review monday service routing
4. 11:00 - 12:00 = Draft ColumnTypes implementation
5. 13:00 - 14:00 = Implement add column type across days
6. 14:00 - 15:00 = Implement delete column type across days
7. 15:00 - 16:00 = Review implementation
8. 16:00 - 17:00 = Wrapup development

## TODO - Previous

1. [ ] Update `Martidocs` readme.md file to include project overview, converting tacit to explicit knowledge
2. [ ] Create centralized development environment articles
3. [ ] Benchmark basic queries

## TODO

1. [ ] Finish monday proposed schema service impl
2. [ ] Implement response handling to all monday routes
3. [ ] Define column types schema
4. [ ] Automations rules
