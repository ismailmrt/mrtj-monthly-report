# August 2024 Report

_Reported by: [Ismail Azh-Zhafir Rohaga](https://ismailrohaga.github.io)_

## Summary

## Key Achievements

## Detailed Daily Activities

### Mon, 02/09/24

1. 08:00 - 09:00 = Catch up with last week progress
, Determine this week's todos and goals
3. 10:00 - 11:00 = Standardize monday teams service routing
4. 11:00 - 12:00 = Define monday automation rules features
5. 13:00 - 14:00 = Identify monday automation triggers
6. 14:00 - 15:00 = Define monday user request types
7. 15:00 - 16:00 = Define monday system event types
8. 16:00 - 17:00 = Timesheet

### Tue, 03/09/24

1. 08:00 - 09:00 = Review monday automation logic documentation
, Identify automation triggers
3. 10:00 - 11:00 = Explore monday.com's website for automation features
4. 11:00 - 12:00 = Explore monday.com's website for automation features
5. 13:00 - 14:00 = Research reverse engineering automation features in monday work management
6. 14:00 - 15:00 = Analyze monday's automation event flow
7. 15:00 - 16:00 = Analyze monday's automation event flow
8. 16:00 - 17:00 = Experiment with monday's UI to understand user event triggers

### Wed, 04/09/24

1. 08:00 - 09:00 = Review workspace-related service layer implementation
, Research monday workspace automation integration
3. 10:00 - 11:00 = adjust workspaces routes and controllers impl
4. 11:00 - 12:00 = Refactor service layer for workspace and board integration
5. 13:00 - 14:00 = Experiment with monday workspace creation and management features
6. 14:00 - 15:00 = Analyze how workspace configuration affects automation triggers
7. 15:00 - 16:00 = adjust board route and controller impl
8. 16:00 - 17:00 = Refactor code to improve workspace service layer structure

### Thu, 05/09/24

1. 08:00 - 09:00 = Explore monday user-triggered automation events
, Research existing user event and request types in monday
3. 10:00 - 11:00 = Analyze service layer improvements for capturing user event types
4. 11:00 - 12:00 = Refactor service layer for better handling of user request flows
5. 13:00 - 14:00 = Research reverse engineering for monday
6. 14:00 - 15:00 = Read monday documentation and blog posts
7. 15:00 - 16:00 = Watch monday work management video tutorials
8. 16:00 - 17:00 = Experiment with monday configurations and automation

### Fri, 06/09/24

1. 08:00 - 09:00 = Research automation features in monday.com
, Research automation features in monday.com
3. 10:00 - 11:00 = Analyze existing error-handling strategies in automation systems
4. 11:00 - 12:00 = Analyze service layer improvements for capturing user event types
5. 13:00 - 14:00 = Read monday documentation and blog posts
6. 14:00 - 15:00 = Read monday documentation and blog posts
7. 15:00 - 16:00 = Watch monday work management video tutorials
8. 16:00 - 17:00 = Watch monday work management video tutorials

### Mon, 09/09/24

1. 08:00 - 09:00 = Timesheets
, Review martidocs monday development and design progress
3. 10:00 - 11:00 = Research layered design pattern/architecture implementation on nodejs
4. 11:00 - 12:00 = Identify potential mvc features to implement service layer design pattern/architecture implementation
5. 13:00 - 14:00 = Review service layer design pattern approach relative to the designed automation features
6. 14:00 - 15:00 = Review service layer design pattern approach relative to the designed automation features
7. 15:00 - 16:00 = Implement base repository layer
8. 16:00 - 17:00 = Implement basic CRUD service & repository layer for boards

### Tue, 10/09/24

1. 08:00 - 09:00 = Research service layer optimizations based on monday automation
, Analyze the integration of automation rules within service layer logic
3. 10:00 - 11:00 = Analyze the integration of automation rules within service layer logic
4. 11:00 - 12:00 = Explore service layer adjustments for seamless automation integration
5. 13:00 - 14:00 = Experiment with monday's automation UI to test various configurations
6. 14:00 - 15:00 = Draft monday automation rules documentation
7. 15:00 - 16:00 = Summarize experiment and research results
8. 16:00 - 17:00 = Weekly meeting

### Wed, 11/09/24

1. 08:00 - 09:00 = Organize research findings from previous days
, Review key aspects of monday automation
3. 10:00 - 11:00 = Begin drafting the analysis document
4. 11:00 - 12:00 = Compile research into cohesive sections
5. 13:00 - 14:00 = Refine analysis and document structure
6. 14:00 - 15:00 = Continue analysis and include detailed sections on system triggers
7. 15:00 - 16:00 = Review and edit document for clarity and coherence
8. 16:00 - 17:00 = Finalize analysis document

### Thu, 12/09/24

1. 08:00 - 09:00 = Review the compiled analysis document
, Research best practices for architecture and sequence diagrams
3. 10:00 - 11:00 = Start creating the architecture diagram
4. 11:00 - 12:00 = Refine architecture diagram, finalize key components
5. 13:00 - 14:00 = Create a simple flowchart for automation flows
6. 14:00 - 15:00 = Begin working on the detailed sequence diagram
7. 15:00 - 16:00 = Continue working on the sequence diagram
8. 16:00 - 17:00 = Finalize diagrams and integrate with analysis document

### Fri, 13/09/24

1. 08:00 - 09:00 = Finalize for PPT
, Implement base repository class
3. 10:00 - 11:00 = Implement repository layer for boards and days
4. 11:00 - 12:00 = Work on service layer implementation
5. 13:00 - 14:00 = Refactor service layer and ensure compatibility with previous implementation
6. 14:00 - 15:00 = Integrate repository and service layers into the controller
7. 15:00 - 16:00 = Test and validate new implementation with controller
8. 16:00 - 17:00 = Review code and prepare for deployment

### Tue, 17/09/24

1. 08:00 - 09:00 = Catch up with last week progress
, Determine this week's todos and goals
3. 10:00 - 11:00 = Catch up with lead for development progress
4. 11:00 - 12:00 = Catch up with timesheet tracking
5. 13:00 - 14:00 = Explore Monday.com website features
6. 14:00 - 15:00 = Research additional documentation for advanced automation
7. 15:00 - 16:00 = Experiment with new configurations on Monday.com
8. 16:00 - 17:00 = Summarize findings and plan next steps

### Wed, 18/09/24

1. 08:00 - 09:00 = Clone Martidocs-FE repository
, Review project structure and repository setup
3. 10:00 - 11:00 = Research and review mantis web template documentation
4. 11:00 - 12:00 = Explore live preview mantis web template
5. 13:00 - 14:00 = Experiment with mantis template in the Martidocs-FE project
6. 14:00 - 15:00 = Timesheet
7. 15:00 - 16:00 = Timesheet
8. 16:00 - 17:00 = Mind mapping on automation analysis presentation flow

### Thu, 19/09/24

1. 08:00 - 09:00 = Review task to work on
, Prepping tomorrow's meeting
3. 10:00 - 11:00 = Refine presentation for tomorrow
4. 11:00 - 12:00 = Review Monday automation documentation
5. 13:00 - 14:00 = Mind mapping on presentation flow
6. 14:00 - 15:00 = Finalize any outstanding tasks
7. 15:00 - 16:00 = Finalize any outstanding tasks
8. 16:00 - 17:00 = Timesheet

### Fri, 20/09/24

1. 08:00 - 09:00 = Review and finalize presentation materials
, Explore Martidocs-FE repo (Mantis template)
3. 10:00 - 11:00 = Explore Mantis template features
4. 11:00 - 12:00 = Research Mantis customization and component usage
5. 13:00 - 14:00 = Research MUI components
6. 14:00 - 15:00 = Analyze monday UI
7. 15:00 - 16:00 = Analyze monday UI
8. 16:00 - 17:00 = Wrap up weekly progress and plan for next week

### Mon, 23/09/24

1. 08:00 - 09:00 = Explore Martidocs FE repo
, Continue exploring Martidocs FE repo
3. 10:00 - 11:00 = Research component usage
4. 11:00 - 12:00 = Dentist appoinment done
5. 13:00 - 14:00 = Explore martidocs fe repo
6. 14:00 - 15:00 = Analyze integration points for the Mantis template
7. 15:00 - 16:00 = Analyze integration points for the Mantis template
8. 16:00 - 17:00 = Timesheet

### Tue, 24/09/24

1. 08:00 - 09:00 = Explore martidocs fe repo
, Investigate the flexibility of Mantis components and how they might fit Martidocs
3. 10:00 - 11:00 = Review mantis documentation
4. 11:00 - 12:00 = Review mantis documentation
5. 13:00 - 14:00 = Review drawer implementation
6. 14:00 - 15:00 = Review drawer header implementation
7. 15:00 - 16:00 = Review MUI components
8. 16:00 - 17:00 = Review MUI components

### Wed, 25/09/24

1. 08:00 - 09:00 = Review codebase
, Code drawer header
3. 10:00 - 11:00 = Continue working on drawer header
4. 11:00 - 12:00 = Adjust component styles
5. 13:00 - 14:00 = Debug issues
6. 14:00 - 15:00 = Debug issues
7. 15:00 - 16:00 = Test implementation
8. 16:00 - 17:00 = Finalize implementation

### Thu, 26/09/24

1. 08:00 - 09:00 = Update timesheet
, Reviewing implementation details (focusing on recent changes)
3. 10:00 - 11:00 = Reviewing implementation details (focusing on recent changes)
4. 11:00 - 12:00 = Testing and verifying the implementation fixes
5. 13:00 - 14:00 = Continued testing and running final checks
6. 14:00 - 15:00 = Continued testing and running final checks
7. 15:00 - 16:00 = Wrapping up review tasks
8. 16:00 - 17:00 = Wrapping up review tasks

### Fri, 27/09/24

1. 08:00 - 09:00 = Updating timesheet with detailed activity logs
, Compiling data for the monthly report
3. 10:00 - 11:00 = Drafting and organizing the monthly report content
4. 11:00 - 12:00 = Refining the monthly report for submission
5. 13:00 - 14:00 = Adding new API endpoint collection to the shared Postman environment
6. 14:00 - 15:00 = Testing the endpoint collection and updating related Postman documentation
7. 15:00 - 16:00 = Testing the endpoint collection and updating related Postman documentation
8. 16:00 - 17:00 = Timesheet & Monthly Report

### Mon, 30/09/24

1. 08:00 - 09:00 = Reviewing timesheet
, Reviewing monthly report
3. 10:00 - 11:00 = Dentist appoinment
4. 11:00 - 12:00 = Reviewing assigned tasks
5. 13:00 - 14:00 = Finalize timesheet
6. 14:00 - 15:00 = Finalize monthly report
7. 15:00 - 16:00 = Submit monthly report
8. 16:00 - 17:00 = Catch up with coworker on Martidocs project
