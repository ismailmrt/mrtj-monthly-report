# November 2024 Report

_Reported by: [Ismail Azh-Zhafir Rohaga](https://ismailrohaga.github.io)_

## Summary

In November 2024, the focus was primarily on frontend development for a project involving workspace and board management, with significant time dedicated to refactoring existing implementations, adding new features, and preparing for an AI workshop presentation. The month was characterized by continuous improvement of the application's navigation, UI components, and state management, alongside in-depth research and preparation for an AI-related presentation to MRT BoD and employees.

## Key Achievements

1.  Development Timeline Accomplishments

    - Completed the frontend slicing and backend branch merging process for the monday feature by November 2024.
    - Successfully refactored multiple components including NavCollapse, productMenu, and sidebar implementations
    - Developed and implemented workspace, board management features, and board table views
    - Implemented create workspace and board modals with improved navigation and responsiveness

2.  AI Workshop Preparation
    - Conducted extensive research on AI technologies, platforms, and use cases
    - Developed a comprehensive AI workshop presentation deck
    - Prepared live demo scenarios and resources for MRT employees
    - Received and incorporated feedback from key stakeholders, including Pak Roy and Pak Dian

## Detailed Daily Activities

### Fri, 01/11/24

1. 08:00 - 09:00 = refactor navCollapse implementation, refactor productMenu implementation
2. 10:00 - 11:00 = commit various weekly progress
3. 11:00 - 12:00 = add workspace create routes
4. 13:00 - 15:00 = add board create routes, add workRoutes implementation
4. 15:00 - 17:00 = update menu-items entry for workflow, timesheet

### Mon, 05/11/24

1. 08:00 - 09:00 = timesheet, add search board list implementation
2. 10:00 - 11:00 = add board list implementation
3. 11:00 - 12:00 = fix handleClsoe bug
4. 13:00 - 15:00 = perform merge feat/drawer-nav-switcher into develop-v2, board's table view implementation
4. 15:00 - 17:00 = adjust auth guard to throw the correct path, resolve merge feat/drawer-nav-switcher into develop-v2

### Tue, 05/11/24

1. 08:00 - 09:00 = timesheet, fix workflow task page routes
2. 10:00 - 11:00 = add base modal implementation
3. 11:00 - 12:00 = handle proper navigation rendering based on the current route
4. 13:00 - 15:00 = add create workspace modal implementation, add create board modal implementation
4. 15:00 - 17:00 = refactor and finalize sidebar implementation, refactor and finalize sidebar implementation

### Wed, 06/11/24

1. 08:00 - 09:00 = timesheet, review mantis documentation
2. 10:00 - 11:00 = weekly meeting
3. 11:00 - 12:00 = refactor navCollapse implementation
4. 13:00 - 15:00 = add workspace management hover menu implementation, add board header section
4. 15:00 - 17:00 = adjust responsiveness of board header section, finalize board header section

### Thu, 07/11/24

1. 08:00 - 09:00 = timesheet, create createBoardModal implementation
2. 10:00 - 11:00 = enhance workspace setting popper and board header
3. 11:00 - 12:00 = add createWorkspaceModal implementation to NavCollapse
4. 13:00 - 15:00 = research redux and react-query implementation, read articles on redux and react-query implementation
4. 15:00 - 17:00 = read articles on redux and react-query implementation, briefing workshop AI for BoD

### Fri, 08/11/24

1. 08:00 - 09:00 = start researching about what is AI and how it works, reading articles online about AI
2. 10:00 - 11:00 = watching videos about AI
3. 11:00 - 12:00 = drafting workshop AI materials
4. 13:00 - 15:00 = listdown task slicing martidocs workflow/monday, discuss monday feature with Mas Fikri & Mas Age
4. 15:00 - 17:00 = writing meeting feedback from Mas Age and revise the listdown documents, timesheet

### Mon, 11/11/24

1. 08:00 - 09:00 = drafting workshop AI presentation deck, research AI platforms (OpenAI, Anthropic, Google Vertex)
2. 10:00 - 11:00 = watch tutorials and explainer videos about AI technologies
3. 11:00 - 12:00 = outline key points for AI workshop presentation
4. 13:00 - 15:00 = continue reading AI research articles and technical blogs, start designing presentation slides=
4. 15:00 - 17:00 = prepare live demo scenarios and test AI platforms, timesheet

### Tue, 12/11/24

1. 08:00 - 09:00 = continue AI workshop presentation deck design=, research AI use cases and practical application
2. 10:00 - 11:00 = refine presentation slide content
3. 11:00 - 12:00 = add board table implementation
4. 13:00 - 15:00 = implement mantis template table, implement simple redux state for board table logic
4. 15:00 - 17:00 = debug redux state, register to redux store and test implementation

### Wed, 13/11/24

1. 08:00 - 09:00 = practice AI workshop presentation, prepare demo accounts
2. 10:00 - 11:00 = review and test live demo scenarios
3. 11:00 - 12:00 = finalize presentation deck
4. 13:00 - 15:00 = weekly meeting, prepare backup materials and resources
4. 15:00 - 17:00 = review and finalize presentation deck, timesheet

### Thu, 14/11/24

1. 08:00 - 09:00 = prepare presentation for workshop AI, review presentation deck with Pak Dian
2. 10:00 - 11:00 = research additional AI case studies
3. 11:00 - 12:00 = refine presentation slides with new insights
4. 13:00 - 15:00 = practice presentation delivery, prepare demo materials for AI platforms
4. 15:00 - 17:00 = create backup slides and resources, timesheet

### Fri, 15/11/24

1. 08:00 - 09:00 = prepare presentation for workshop AI, present draft presentation deck to Pak Roy, finance director of MRT
2. 10:00 - 11:00 = gather feedback from Pak Roy and Pak Dian
3. 11:00 - 12:00 = incorporate feedback into presentation
4. 13:00 - 15:00 = revise slide design and content, review and finalize presentation deck
4. 15:00 - 17:00 = share ppt materials to Mas Fikri, timesheet

### Mon, 18/11/24

1. 08:00 - 09:00 = timesheet, review monday feature implementation
2. 10:00 - 11:00 = adjust NavCollapse implementation
3. 11:00 - 12:00 = adjust navigation routes in board page implementation
4. 13:00 - 15:00 = delete workspace create routes and pages, adjust workspace view implementation
4. 15:00 - 17:00 = various workspace view implementation, timesheet

### Tue, 19/11/24

1. 08:00 - 09:00 = timesheet, design create column popup menu
2. 10:00 - 11:00 = debugging popup menu
3. 11:00 - 12:00 = finalize create column popup menu implementation
4. 13:00 - 15:00 = weekly meeting, bug fixing on popup menu implementation
4. 15:00 - 17:00 = responsive popup menu implementation, various board view implementation

### Wed, 20/11/24

1. 08:00 - 09:00 = timesheet, review overall implementation
2. 10:00 - 11:00 = enhancing table head hover state
3. 11:00 - 12:00 = various bug fix
4. 13:00 - 15:00 = design AI workshop poster, collaborate with Pak Agi in designing AI workshop poster
4. 15:00 - 17:00 = finalize AI workshop poster, fix table horizontal scroll render

### Thu, 21/11/24

1. 08:00 - 09:00 = timesheet, add groups redux store
2. 10:00 - 11:00 = remove unused component
3. 11:00 - 12:00 = various fix
4. 13:00 - 15:00 = various view adjustments, debugging on container width
4. 15:00 - 17:00 = use fit-content props for width, timesheet

### Fri, 22/11/24

1. 08:00 - 09:00 = timesheet, review AI workshop presentation
2. 10:00 - 11:00 = update presentation with recent AI developments
3. 11:00 - 12:00 = prepare additional demo scenarios=
4. 13:00 - 15:00 = base cell editable text implementation, merge backend monday feature to develop branch
4. 15:00 - 17:00 = adjust merge conflicts, briefing workshop AI for MRT employees

### Mon, 25/11/24

1. 08:00 - 09:00 = timesheet, fullWidth row view adjustment
2. 10:00 - 11:00 = finalize groupContentHead implementation
3. 11:00 - 12:00 = groupContentBody implementation
4. 13:00 - 15:00 = refactor filename and board header implementation, refactor folder structure
4. 15:00 - 17:00 = refactor cell view implementation, add missing import & remove deprecated routes

### Tue, 26/11/24

1. 08:00 - 09:00 = revise AI workshop poster, finalize AI workshop presentation details
2. 10:00 - 11:00 = practice final presentation run-through=
3. 11:00 - 12:00 = review and update presentation materials
4. 13:00 - 15:00 = copy flows hooks implementation, add workspace hooks implementation
4. 15:00 - 17:00 = various implementation, weekly meeting

### Wed, 27/11/24

1. 08:00 - 09:00 = timesheet, final review of AI workshop presentation
2. 10:00 - 11:00 = final review of AI workshop presentation
3. 11:00 - 12:00 = various UI implementation
4. 13:00 - 15:00 = adding todo, testing documentflow implementation on the merged backend branch
4. 15:00 - 17:00 = adjust constants values, adjust route conflicts

### Thu, 28/11/24

1. 08:00 - 09:00 = timesheet, continue Monday feature implementation
2. 10:00 - 11:00 = review and test Monday feature
3. 11:00 - 12:00 = finalize AI workshop presentation slides
4. 13:00 - 15:00 = review Monday feature implementation, briefing workshop AI for MRT employees
4. 15:00 - 17:00 = compose whatsapp message blast for workshop AI, workspace hooks implenentation

### Fri, 29/11/24

1. 08:00 - 09:00 = timesheet activity recap, finalizing timesheet
2. 10:00 - 11:00 = finalizing monthly report
3. 11:00 - 12:00 = submit timesheet and monthly report
4. 13:00 - 15:00 = finalize workspace add & delete hook implementation, wokspace hook debugging
4. 15:00 - 17:00 = wokspace hook bug fixing, wrapping up monthly development
