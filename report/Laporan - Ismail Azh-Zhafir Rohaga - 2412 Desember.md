# December 2024 Report

_Reported by: [Ismail Azh-Zhafir Rohaga](https://ismailrohaga.github.io)_

## Summary

Throughout December 2024, significant progress was made in developing and enhancing various features of the project. The month focused on API implementations, workspace and board functionalities, and member management systems. Key focus areas included transitioning from Redux to React Query, implementing approval workflows, and developing workspace member invitation features. The latter part of the month saw substantial work on Monday API integration and modal implementations.

## Key Achievements

1. Successfully transitioned from Redux to React Query implementation for improved state management
1. Completed workspace and board page API implementations, including side navigation
1. Implemented comprehensive workspace member invitation system with role-based permissions
1. Developed and integrated approval cell functionality with workflow architecture
1. Created table view with group functionality and row management features
1. Implemented column mapping system and cell value rendering
1. Successfully integrated Monday API with todo feature structure
1. Developed base modal implementation for enhanced user interface
1. Completed column types adjustments and number cell implementation
1. Implemented move row functionality with API integration
1. Created and delivered IT Talk presentation materials
1. Established comprehensive documentation for implemented features

## Detailed Daily Activities

### Mon, 02/12/24

1. 08:00 - 10:00 = Timesheet, Review AI workshop presentation materials & demo
2. 10:00 - 11:00 = Workspace page & Side Nav API Impl, Board page & Side Nav API Impl
3. 11:00 - 12:00 = API Impl debugging
4. 13:00 - 15:00 = Bug fixing, Table view (Groups) API Impl
5. 15:00 - 17:00 = Finalizing changes and reviewing implementation, Timesheet

### Tue, 03/12/24

1. 08:00 - 10:00 = Reviewing redux menu refactor.
2. 10:00 - 11:00 = Planning adjustments for labels and hooks, Addressing initial label value fixes.
3. 11:00 - 12:00 = Prepare for weekly report
4. 13:00 - 15:00 = Weekly meeting, Working on API-related fixes.
5. 15:00 - 17:00 = Practice IT Talk presentation @ Teams, Process feedback from team

### Wed, 04/12/24

1. 08:00 - 10:00 = IT Talk presentation @ Teams, Timesheet
2. 10:00 - 11:00 = Implementing various API functionalities.
3. 11:00 - 12:00 = Documentation and minor refactoring, Revamp current redux implementation to react-query implementation
4. 13:00 - 15:00 = Implement react-query to workspace modules
5. 15:00 - 17:00 = Bug fixing, Debugging API implementation

### Thu, 05/12/24

1. 08:00 - 10:00 = Review and planning API tasks., Address initial API fixes.
2. 10:00 - 11:00 = Implementing various API functionalities.
3. 11:00 - 12:00 = Documentation and minor refactoring, Break or discussions for enhancements.
4. 13:00 - 15:00 = Preparing workflow merges.
5. 15:00 - 17:00 = Finalizing feature workflows, Wrapping up remaining merge-related tasks.

### Fri, 06/12/24

1. 08:00 - 10:00 = Reviewing API workflows, Preparing for feature implementations.
2. 10:00 - 11:00 = Addressing various API fixes.
3. 11:00 - 12:00 = Break or reviewing adjustments, Implementing API fixes and enhancements.
4. 13:00 - 15:00 = Refining API implementations.
5. 15:00 - 17:00 = Reviewing workflow and feature merges, Debugging and quality assurance.

### Mon, 09/12/24

1. 08:00 - 10:00 = Reviewing workspace features, Enhancements for editable text.
2. 10:00 - 11:00 = Adjustments for response and hooks.
3. 11:00 - 12:00 = Documenting feature adjustments, UI adjustments and enhancements.
4. 13:00 - 15:00 = Planning board feature adjustments.
5. 15:00 - 17:00 = Implementing and testing board features, Finalizing adjustments for board views.

### Tue, 10/12/24

1. 08:00 - 10:00 = Reviewing board view enhancements, Planning row management features.
2. 10:00 - 11:00 = Implementing row management features.
3. 11:00 - 12:00 = Finish implement column mapping, Weekly meeting.
4. 13:00 - 15:00 = Planning additional board adjustments.
5. 15:00 - 17:00 = Refining board adjustments, Reviewing and debugging board view features.

### Wed, 11/12/24

1. 08:00 - 10:00 = Planning and reviewing previous work, Working on fixes and hiding unused button
2. 10:00 - 11:00 = Working on various fixes
3. 11:00 - 12:00 = Working on base column to cell mapping, Working on base column to cell mapping
4. 13:00 - 15:00 = Removing unused redux state
5. 15:00 - 17:00 = Working on hooks implementation, Enhancing GroupContent and CellView components

### Thu, 12/12/24

1. 08:00 - 10:00 = Planning and setup, Working on bug fixes and refactoring
2. 10:00 - 11:00 = Working on bug fixes and refactoring
3. 11:00 - 12:00 = Completing minor bug fixing and refactoring, Documentation and testing
4. 13:00 - 15:00 = Team sync and planning
5. 15:00 - 17:00 = Code review, Testing and documentation

### Fri, 13/12/24

1. 08:00 - 10:00 = Planning and setup, Working on cell mapper implementation
2. 10:00 - 11:00 = Working on droppableId fixes
3. 11:00 - 12:00 = Working on various enhancements, Working on status cell implementation
4. 13:00 - 15:00 = Completing status cell and delete row hook implementation
5. 15:00 - 17:00 = Working on update row hook implementation, Completing update row value hook implementation

### Mon, 16/12/24

1. 08:00 - 10:00 = Planning and setup, Working on row cellValue render and status cell hook
2. 10:00 - 11:00 = Working on workspace member invitation
3. 11:00 - 12:00 = Working on workspace member invitation, Working on workspace member invitation
4. 13:00 - 15:00 = Completing invite workspace member implementation and fixes
5. 15:00 - 17:00 = Adding approval type and column types adjustments, Documentation and testing

### Tue, 17/12/24

1. 08:00 - 10:00 = Planning and review, Taking code of conduct quiz
2. 10:00 - 11:00 = Working on board member permissions logic
3. 11:00 - 12:00 = Implementing workspace member roles, Developing invitation system backend integration
4. 13:00 - 15:00 = Creating member management UI components
5. 15:00 - 17:00 = Testing and debugging member authentication, Implementing real-time member status updates

### Wed, 18/12/24

1. 08:00 - 10:00 = Working on moveRow API implementation, Weekly meeting
2. 10:00 - 11:00 = Working on moveRow implementation
3. 11:00 - 12:00 = Drafting Knowledge sharing materials, Implementing numberCell and fixing undefined rows
4. 13:00 - 15:00 = Working on approval cell implementation
5. 15:00 - 17:00 = Working on approval cell implementation, Timesheet

### Thu, 19/12/24

1. 08:00 - 10:00 = Planning and review, Completing WIP approval cell implementation
2. 10:00 - 11:00 = Completing WIP approval cell implementation
3. 11:00 - 12:00 = Drafting Knowledge sharing materials, Review all-time monthly report
4. 13:00 - 15:00 = Review assigned project (MIRA & Martidocs)
5. 15:00 - 17:00 = Finalize knowledge sharing materials, Timesheet

### Fri, 20/12/24

1. 08:00 - 10:00 = Planning and setup, Designing approval workflow architecture
2. 10:00 - 11:00 = Implementing approval state management
3. 11:00 - 12:00 = Creating approval UI components, Developing approval validation logic
4. 13:00 - 15:00 = Integrating approval notifications system
5. 15:00 - 17:00 = Testing multi-step approval flows, Adjusting text color and approval view

### Mon, 23/12/24

1. 08:00 - 10:00 = Meeting with mas fikri regarding progress of Monday, Developing todo feature structure
2. 10:00 - 11:00 = Working on Monday API integration
3. 11:00 - 12:00 = Testing API implementations, Debugging API responses
4. 13:00 - 15:00 = Code review and adjustments
5. 15:00 - 17:00 = Completing Monday API implementation and adding todos, Fixing modal issues and board implementation

### Tue, 24/12/24

1. 08:00 - 10:00 = Working on Monday API integration
2. 10:00 - 11:00 = Code review and adjustments
3. 11:00 - 12:00 = Working on moveRow implementation
4. 13:00 - 15:00 = Working on approval cell implementation
5. 15:00 - 17:00 = Weekly meeting

### Wed, 25/12/24

libur

### Thu, 26/12/24

libur

### Fri, 27/12/24

1. 08:00 - 10:00 = Meeting with mas fikri regarding progress of Monday, Developing todo feature structure
2. 10:00 - 11:00 = Working on Monday API integration
3. 11:00 - 12:00 = Testing API implementations, Debugging API responses
4. 13:00 - 15:00 = Code review and adjustments
5. 15:00 - 17:00 = Completing Monday API implementation and adding todos, Fixing modal issues and board implementation

### Mon, 30/12/24

1. 08:00 - 10:00 = Finalize knowledge sharing documents, Finalize knowledge sharing approval documents
2. 10:00 - 11:00 = Timesheet, Research modal best practice implementation
3. 11:00 - 12:00 = Timesheet, Research modal best practice implementation
4. 13:00 - 15:00 = Review current modal implementation
5. 15:00 - 17:00 = Draft modal implementation, WIP base modal implementation

### Tue, 31/12/24

1. 08:00 - 10:00 = Finalize timesheet, Working on monthly report
2. 10:00 - 11:00 = Finalize monthly report and review
3. 11:00 - 12:00 = Working on Edit column modal feature implenentation, Working on Edit column modal feature implenentation
4. 13:00 - 15:00 = Review edit column modal feature implementation
5. 15:00 - 17:00 = Review base modal implementation, Review Martidocs monday feature implementation
