# July 2024 Report

_Reported by: [Ismail Azh-Zhafir Rohaga](https://ismailrohaga.github.io)_

## Summary

In July 2024, significant progress was made on the Martidocs project, with a focus on integrating new features inspired by monday.com, enhancing MongoDB schema design, and implementing CRUD, input validation and sanitization. Key activities included schema drafting, MongoDB research and optimization, CRUD operations implementation, and preparation for the Martidocs backend revamp.

## Key Achievements

1. Martidocs Monday Feature Development:

- Drafted and finalized the schema for integrating Monday-like functionalities into Martidocs.
- Conducted thorough research and reverse engineering on monday.com to replicate essential features.
- Created and tested MongoDB collections based on the new schema.

2. MongoDB Schema Optimization:

- Designed and implemented a robust schema for Martidocs, focusing on best practices for heavy read/write operations.
- Conducted experiments with embedding and denormalization to enhance query performance.
- Developed and documented schema improvements and querying techniques.

3. CRUD Operations Implementation:

- Successfully implemented basic CRUD operations for teams and workspace collections.
- Added auxiliary functionalities such as member management and role updates.
- Ensured all operations were thoroughly tested and debugged for optimal performance.

4. Input Validation and Sanitization:

- Planned and implemented comprehensive input validation and sanitization processes.
- Researched and applied best practices for error handling and input sanitization using express-validator.

## Notes

- [Collection Schema Analysis](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/blob/main/analysis/martidocs-monday.md)
- [Collection Schema Basic Query](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/blob/main/analysis/martidocs-monday-query.md)
- [Collection Schema Design - Figma](https://www.figma.com/slides/thSDz9xbXzdVazzTThpCvt/Monday-Collection-Structure-Analysis?node-id=1-70&t=JX8A9XivRQyEuDWl-1)
- [Collection Schema Design - Drawio](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/blob/main/erddrawio)

## Detailed Daily Activities

### Mon, 01/07/24

1. 08:00 - 09:00 = Izin ke Binus untuk menghadiri acara briefing wisuda
, Hadir di Binus
3. 10:00 - 11:00 = Acara briefing di Binus
4. 11:00 - 12:00 = Pengambilan Toga
5. 13:00 - 14:00 = Analyze `Martidocs` functionality
6. 14:00 - 15:00 = Read `Martidocs` manualbook
7. 15:00 - 16:00 = Draft SRS document for `Martidocs` monday feature
8. 16:00 - 17:00 = Briefing monday feature on `Martidocs`

### Tue, 02/07/24

1. 08:00 - 09:00 = Draft RFC document for `Martidocs` monday feature
, Read monday.com knowledge base
3. 10:00 - 11:00 = Read monday.com platform API
4. 11:00 - 12:00 = Draft `Martidocs` monday schema
5. 13:00 - 14:00 = Weekly meeting
6. 14:00 - 15:00 = Reverse engineering analysis of monday.com monday dev product
7. 15:00 - 16:00 = Trying out monday.com features and functionality, taking 14 day trial on personal account
8. 16:00 - 17:00 = Reverse engineering analysis of monday.com work management product

### Wed, 03/07/24

1. 08:00 - 09:00 = Update `MIRA` SO changes patching SQL to spreadsheet
, Draft `Martidocs` monday schema
3. 10:00 - 11:00 = Compare drafted schema with official platform API
4. 11:00 - 12:00 = Research mongodb modelling best practice
5. 13:00 - 14:00 = Create mongodb collections based on drafted schema
6. 14:00 - 15:00 = Run some queries to the created collections
7. 15:00 - 16:00 = Benchmark queries
8. 16:00 - 17:00 = Timesheet

### Thu, 04/07/24

1. 08:00 - 09:00 = Create json from drafted schema
, Learn MongoDB schema design principles
3. 10:00 - 11:00 = Experiment with embedding documents in MongoDB
4. 11:00 - 12:00 = Experiment with denormalization
5. 13:00 - 14:00 = Learn best practice schema design
6. 14:00 - 15:00 = Relearn MongoDB basic query
7. 15:00 - 16:00 = Read Mongosh Documentations
8. 16:00 - 17:00 = Document findings

### Fri, 05/07/24

1. 08:00 - 09:00 = Review KaDiv original design analysis
, Develop proposed schema improvement based on MongoDB research
3. 10:00 - 11:00 = Draft initial collections
4. 11:00 - 12:00 = Implement a few basic queries
5. 13:00 - 14:00 = Research additional querying techniques
6. 14:00 - 15:00 = Draft documentation on schema improvements and querying techniques
7. 15:00 - 16:00 = Review and refine all draft and findings
8. 16:00 - 17:00 = Timesheet

### Mon, 08/07/24

1. 08:00 - 09:00 = Finalizing monday schema
, Review and refine schema documentation
3. 10:00 - 11:00 = Integrate schema with existing codebase
4. 11:00 - 12:00 = Test basic queries
5. 13:00 - 14:00 = Research MongoDB best practice for heavy read/write ops
6. 14:00 - 15:00 = Document optimizations based on research
7. 15:00 - 16:00 = Apply optimizations
8. 16:00 - 17:00 = Timesheet

### Tue, 09/07/24

1. 08:00 - 09:00 = Review and refine automation requirements
, Analyze activity log features
3. 10:00 - 11:00 = Analyze basic automation features
4. 11:00 - 12:00 = Analyze automation workflows
5. 13:00 - 14:00 = Plan and design automation workflows
6. 14:00 - 15:00 = Draft basic collection for workflows
7. 15:00 - 16:00 = Discuss with Lead about the progress so far
8. 16:00 - 17:00 = Timesheet

### Wed, 10/07/24

1. 08:00 - 09:00 = Timesheet
, Prepare presentation topic & resource
3. 10:00 - 11:00 = Work on presentation topic & resource
4. 11:00 - 12:00 = Review presentation topic & resource
5. 13:00 - 14:00 = Finalize presentation topic & resource
6. 14:00 - 15:00 = Pairing with lead to discuss Monday
7. 15:00 - 16:00 = Create meeting recap and feedback
8. 16:00 - 17:00 = Timesheet

### Thu, 11/07/24

1. 08:00 - 09:00 = Timesheet
, Prepare presentation topic & resource
3. 10:00 - 11:00 = Work on presentation topic & resource
4. 11:00 - 12:00 = Review presentation topic & resource
5. 13:00 - 14:00 = Finalize presentation topic & resource
6. 14:00 - 15:00 = Pairing with lead to discuss Monday
7. 15:00 - 16:00 = Create meeting recap and feedback
8. 16:00 - 17:00 = Timesheet

### Fri, 12/07/24

1. 08:00 - 09:00 = Timesheet
, Reporting
3. 10:00 - 11:00 = Apply meeting discussion result on the proposed collection structure
4. 11:00 - 12:00 = Apply embed & denormalization
5. 13:00 - 14:00 = Identify queries for the core functionality
6. 14:00 - 15:00 = Query breakdown
7. 15:00 - 16:00 = Compose query
8. 16:00 - 17:00 = Benchmark query

### Mon, 15/07/24

1. 08:00 - 09:00 = Timesheet
, Create get queries use case for each collections
3. 10:00 - 11:00 = Create sample query for insert and delete row
4. 11:00 - 12:00 = Create sample query for edit row columnValues
5. 13:00 - 14:00 = Request for dotenv file to Mas Zakiy
6. 14:00 - 15:00 = Discuss to lead about query progress
7. 15:00 - 16:00 = Discuss to lead about MIRA
8. 16:00 - 17:00 = Wrapping up today's session

### Tue, 16/07/24

1. 08:00 - 09:00 = Timesheet
, Get medical treatment
3. 10:00 - 11:00 = Medical checkup
4. 11:00 - 12:00 = Pick up and pay for medicine
5. 13:00 - 14:00 = Weekly meeting
6. 14:00 - 15:00 = Bed rest
7. 15:00 - 16:00 = Read the provided sample TemplateBECode repo
8. 16:00 - 17:00 = Read current martidocs backend revamp repo

### Wed, 17/07/24

1. 08:00 - 09:00 = Timesheet
, Review martidocs backend revamp implementation plan
3. 10:00 - 11:00 = Identify potential issues and dependencies for martidocs backend revamp
4. 11:00 - 12:00 = Draft implementation steps for the martidocs backend revamp
5. 13:00 - 14:00 = Break down implementation steps into actionable tasks
6. 14:00 - 15:00 = Create Todo
7. 15:00 - 16:00 = Start working on martidocs backend revamp
8. 16:00 - 17:00 = Request for db dump to Mas Zakiy

### Thu, 18/07/24

1. 08:00 - 09:00 = Timesheet
, Restore martidocs-revamp db dump
3. 10:00 - 11:00 = Execute martidocs-revamp db migrate & seed
4. 11:00 - 12:00 = Review and update db migration and seeding scripts
5. 13:00 - 14:00 = Test db migration and seeding scripts
6. 14:00 - 15:00 = Debug issues with db migration and seeding
7. 15:00 - 16:00 = Validate db migration and seeding process
8. 16:00 - 17:00 = Read reference repo CRUD implementation

### Fri, 19/07/24

1. 08:00 - 09:00 = Timesheet
, Send monday collection schema design to lead
3. 10:00 - 11:00 = Initialize teams collection service
4. 11:00 - 12:00 = Refactor collection field name to adjust with current naming
5. 13:00 - 14:00 = Create sample ColumnType entries (status, assignee)
6. 14:00 - 15:00 = Add index and apply denormalization
7. 15:00 - 16:00 = Update `martidocs-monday.md`
8. 16:00 - 17:00 = Draft search row's columnValues value

### Mon, 22/07/24

1. 08:00 - 09:00 = Timesheet
, Finalize search row's columnValues value
3. 10:00 - 11:00 = Implement basic CRUD operations for martidocs-revamp
4. 11:00 - 12:00 = Test CRUD operations
5. 13:00 - 14:00 = Debug and refine CRUD operations
6. 14:00 - 15:00 = Implement get teams collection service
7. 15:00 - 16:00 = Test get teams collection service
8. 16:00 - 17:00 = Debug and refine get teams collection service

### Tue, 23/07/24

1. 08:00 - 09:00 = Timesheet
, Reporting
3. 10:00 - 11:00 = Add delete and team-member related functionality implementation
4. 11:00 - 12:00 = Bug fix and lint warnings
5. 13:00 - 14:00 = Enhance service request processing
6. 14:00 - 15:00 = Add default values schema options
7. 15:00 - 16:00 = Add todos and `express-validator` deps
8. 16:00 - 17:00 = Weekly meeting, sharing progress

### Wed, 24/07/24

1. 08:00 - 09:00 = Timesheet
, Planning on the input sanitization, error handling, and TDD implementation
3. 10:00 - 11:00 = Research on similar repo for input sanitization
4. 11:00 - 12:00 = Research on similar repo for error handling
5. 13:00 - 14:00 = Research on similar repo for TDD
6. 14:00 - 15:00 = Plan input sanitization and validation
7. 15:00 - 16:00 = Read TemplateCodeBE sanitization and validation impl
8. 16:00 - 17:00 = Draft

### Thu, 25/07/24

1. 08:00 - 09:00 = Timesheet
, Decluttering windows, setting up todos
3. 10:00 - 11:00 = Read `express-validator` documentation and articles
4. 11:00 - 12:00 = Read node-express error handling practice articles
5. 13:00 - 14:00 = Write implementation strategy
6. 14:00 - 15:00 = Implement input sanitization and validation
7. 15:00 - 16:00 = Implement error handling
8. 16:00 - 17:00 = Add workspace & boards service implementation

### Fri, 26/07/24

1. 08:00 - 09:00 = Timesheet
, Reporting
3. 10:00 - 11:00 = Finalize Timesheet and Reporting
4. 11:00 - 12:00 = Implement get and create workspace collection service
5. 13:00 - 14:00 = Implement update and delete workspace collection service
6. 14:00 - 15:00 = Implement workspace auxiliary service (members, teams)
7. 15:00 - 16:00 = Implement add and remove workspace auxiliary service (members, teams)
8. 16:00 - 17:00 = Implement update role workspace auxiliary service (members, teams)

### Mon, 29/07/24

1. 08:00 - 09:00 = Timesheet
, Implement input validation on workspace get and create
3. 10:00 - 11:00 = Update teams and workspace schema
4. 11:00 - 12:00 = Implement input validation on workspace update and drop
5. 13:00 - 14:00 = Implement input validation on workspace auxiliary add
6. 14:00 - 15:00 = Implement input sanitation on workspace auxiliary remove
7. 15:00 - 16:00 = Implement input sanitation on workspace auxiliary update
8. 16:00 - 17:00 = Finalize input validation and sanitation for workspace

### Tue, 30/07/24

1. 08:00 - 09:00 = Timesheet
, Create workspace routing
3. 10:00 - 11:00 = Bind workspace routing to validator
4. 11:00 - 12:00 = Bind workspace routing to controller
5. 13:00 - 14:00 = Weekly meeting
6. 14:00 - 15:00 = Implement board CRUD service
7. 15:00 - 16:00 = Implement board CRUD service
8. 16:00 - 17:00 = Timesheet

### Wed, 31/07/24

1. 08:00 - 09:00 = Timesheet
, Monthly Report
3. 10:00 - 11:00 = Finalizing Timesheet and Monthly Report
4. 11:00 - 12:00 = Submit Timesheet and Monthly Report
5. 13:00 - 14:00 = Implement add and remove board auxiliary service
6. 14:00 - 15:00 = Implement add and remove board auxiliary service
7. 15:00 - 16:00 = Implement update board auxiliary service
8. 16:00 - 17:00 = Implement update board auxiliary service

## TODO - Previous

1. [ ] Deep dive understanding of `MIRA` business process and app flow
2. [ ] Update `MIRA` readme.md file to include project overview, converting tacit to explicit knowledge
3. [ ] Update `Martidocs` readme.md file to include project overview, converting tacit to explicit knowledge
4. [ ] `Martidocs` monday feature requirement analysis
5. [ ] Mira docker to use mysql:8.0
6. [ ] Create centralized development environment articles

## TODO

1. [x] Apply proposed schema to MongoDB
2. [x] Create basic queries for the proposed schema
3. [ ] Benchmark basic queries
