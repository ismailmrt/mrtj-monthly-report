# May 2024 Report (20/05/24 - 31/05/24)

_Reported by: [Ismail Azh-Zhafir Rohaga](https://ismailrohaga.github.io)_

## Summary

Joining PT. MRT Jakarta @ 20/05/2024. The initial phase involved comprehensive onboarding, which showed the bird-eye-view project overview and workflow briefing. Essential devices and accounts were set up, and a suite of critical applications was installed on both `Windows` and `WSL` platforms.

Then I was assigned to the `MIRA` project, where I successfully cloned and set up the project repository and reviewed the provided documentation. Additionally, I created a new branch for chore-related adjustments and assisted peers with technical issues, including dotenv detection and GitLab SSH registration.

Efforts were also directed towards documenting and creating a centralized development environment using `WSL`. This practice aimed to convert tacit knowledge into explicit documentation, ensuring the smooth transfer of information within the team. The month concluded with the creation of the monthly report and timesheet, encapsulating the activities and progress made.

## Key Achievements

### Windows development environment

During this period, I set up a comprehensive Windows development environment, successfully installing `WSL` on `Windows` and utilizing `Chocolatey` as a dependency manager. This setup enabled seamless integration between Windows and Linux tools, fostering a robust development environment.

![successfully installed `WSL` on `Windows`,
also using `Chocolatey` as dependency manager in `Windows`](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2405/windows-wsl-env-overview.png)
Fig. 1: Windows & WSL environment

### WSL development environment

In the WSL environment, I installed and configured critical development tools such as `VSCode` plugins on both local `Windows` and `WSL` environments, adhering to development guidelines. Additionally, `NeoVim` plugins were installed from the `LazyVim` distro to enhance the coding experience.

![Installed `VSCode` plugins both on local `Windows` and `WSL` environment](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2405/vscode-plugin.png)

Fig. 2: VSCode plugin configured as per dev guidelines

![Installed `NeoVim` plugins, default from `LazyVim` distro](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2405/nvim-plugin-wsl.png)
Fig. 3: NeoVim plugin configured as per dev guidelines

---

Centralizing development dependencies in WSL offers consistency, ease of management, access to powerful Linux tools, better Docker integration, isolation of dependencies, and potentially improved performance. This approach aligns closely with the company's requirements of installing `Docker` only in `WSL` environment and also this approach aligns with many modern development workflows, especially when dealing with Linux-based production environments.

![DBeaver connection schema for `MySQL` to `WSL` environment](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2405/dbeaver-mysql-to-wsl.png)
Fig. 4: DBeaver `Windows` client configured to `WSL` MySQL

![DBeaver connection schema for `PostgreSQL` to `WSL` environment](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2405/dbeaver-psql-to-wsl.png)
Fig. 5: DBeaver `Windows` client configured to `WSL` PostgreSQL

![Compass connection schema to `WSL` and `remote` environment](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2405/compass-to-wsl-and-remote.png)
Fig. 6: MongoDBCompass `Windows` client configured to `WSL` and `remote` `grups.cc` MongoDB

![Docker is available](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2405/docker-wsl.png)
Fig. 7: Docker running in `WSL` environment

### Sample repo: `grups.cc` as a demonstration of centralized `WSL` dev env

For the sample repo `grups.cc`, I created a `docker-compose.yml` file to simplify running instances (backend and frontend). This allowed for seamless local development and testing, demonstrating the efficiency of a centralized `WSL` development environment.

![`docker` `ps` result showing running `grups.cc` containers](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2405/grups-docker.png)
Fig. 8: Sample repo instance on `Docker`

![The site is available @ port 3000 locally](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2405/grups-fe.png)
Fig. 9: Running instance of `grups.cc` FE client

### Assigned repo: explore `MIRA` and read provided docs

Exploring the `MIRA` project involved cloning the repository, setting up the environment, and reviewing the documentation.

![Exploring `MIRA` menu from dashboard to master feature](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2405/mira-explore.png)
Fig. 10: Running instance of `MIRA`

I identified some areas for improvement and created several commits. Although it's generally advised not to modify legacy code, these enhancements will promote greater consistency across the development environment, simplify setup for new developers, and ensure the project environment remains isolated.

> Should the initiatives be deemed unnecessary, I will delete the branch.

![LazyGit showing pushed commit to `f` branch](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2405/mira-chore.png)
Fig. 11: Suggested initiatives for `MIRA` repo

## Detailed Daily Activities

### Mon, 20/05/24

1. **Onboarding, project overview, workflow briefing** (architecture & development)
2. **Device & account setup**:
   - [x] Lenovo Ideapad Slim 5i
   - [x] Outlook (<rismail@jakartamrt.co.id>)
3. **Installed the following apps/utils on `Windows`:**
   - [x] Chrome
   - [x] oh-my-posh
   - [x] Git
   - [x] WSL
   - [x] Vim
   - [x] LazyVim
   - [x] VSCode
   - [x] Chocolatey
   - [x] NVM
   - [x] NPM (latest)
   - [x] XAMPP
   - [x] Postman
   - [x] DBeaver
4. Sent Lenovo Laptop BAST form via WhatsApp

### Tue, 21/05/24

1. **Account setup**:
   - [x] Outlook, change pwd
   - [x] GitLab
   - [x] Teams
2. **Installed the following app/utils on `Windows`:**
   - [x] MongoDB Compass
3. **Issue on installing `WSL` Ubuntu distro**, solved by restarting device
   & using the user's terminal environment instead of an elevated admin profile
4. Reviewed sample backend repo
5. Sent Offering Letter form by email
6. Weekly meeting

### Wed, 22/05/24

1. **`WSL` setup:** using wsl2
2. **Installed the following app/utils on `WSL`:**
   - [x] Zsh
   - [x] oh-my-zsh
   - [x] Vim
   - [x] LazyVim
   - [x] NVM
   - [x] Node (latest)
   - [x] MongoDB
   - [x] PostgreSQL
   - [x] MySQL
3. Configured VSCode (install plugins: [Prettier, WSL, ...])
4. Configured LazyVim on `Windows` & `WSL`
   (install plugins: [LSP, formatter (Prettier), ...])
5. Resolved LazyVim `LazyHealth` issue
6. Interviewed fellow Green Squad peers:
   - Green Squad mostly working on existing web/app
     - **Tasks:** [bug fix, new features]
     - **Projects:** [eproc, CP207, ...]
   - **Tech stack:** [React, Node, Redis, RabbitMQ, .NET, PHP (Laravel/CI)]
   - **Task types (example):** [fix logger issues, fix WhatsApp template]
   - **Note:** Prettier set as default config

### Thu 23/04/24 to Fri 24/05/24 (Public Holiday)

1. Researched `WSL` usage and best practices
2. Set `WSL` as the centralized development environment
3. Installed the following app/utils on `Windows`: [Arc Browser]
4. Practiced Vim motions

### Mon, 27/05/24

1. Configured DBeaver to connect to `Windows` MySQL `XAMPP` databases
2. Configured DBeaver to connect to `WSL` MySQL databases
3. Created sample Docker Compose for the provided sample FE/BE `grups.cc` repo
4. Ran sample repo Docker Compose on `WSL` using `docker compose up`
5. Configured DBeaver to connect to `WSL` PostgreSQL databases
6. Configured MongoDB Compass to connect to `WSL` MongoDB instances
7. Coordinated with lead and supervisor regarding personal matters
   (rekrutmen bersama) for the following day
8. Briefed and assigned to `MIRA` project and invited to GitLab repo
9. Printed and signed "Pernyataan Pajak Non Pegawai"
   & "Pakta Integritas Non Pegawai" form

### Tue, 28/05/24

1. Configured DBeaver to connect to `Windows` MySQL `XAMPP` databases
2. Configured DBeaver to connect to `WSL` MySQL databases
3. Created sample Docker Compose for the provided sample FE/BE `grups.cc` repo
4. Ran sample repo Docker Compose on `WSL` using `docker compose up`
5. Configured DBeaver to connect to `WSL` PostgreSQL databases
6. Configured MongoDB Compass to connect to `WSL` MongoDB instances
7. Coordinated with lead and supervisor regarding personal matters
   (rekrutmen bersama) for the following day
8. Briefed and assigned to `MIRA` project and invited to GitLab repo
9. Printed and signed "Pernyataan Pajak Non Pegawai"
   & "Pakta Integritas Non Pegawai" form

### Wed, 29/05/24

1. Sent "Pernyataan Pajak Non Pegawai" & "Pakta Integritas Non Pegawai" form in person
2. Cloned, setup, and served `MIRA` repo
3. Reviewed the provided `MIRA` documentation
4. Created new `f` branch for chore repo and added chore-related adjustments
5. Assisted fellow Green Squad peers:
   - [x] Issue with dotenv not detected on Docker Compose
   - [x] Issue with GitLab SSH registration

### Thu, 30/05/24

1. Drafted monthly report and timesheet
2. Drafted centralized development environment using `WSL` as part
   of converting tacit to explicit knowledge practice
3. Explore `MIRA` feature/menu

### Fri, 31/05/24

1. Finalizing monthly report
2. Reviewing `MIRA` `Risk Monitor` feature

## TODO

1. [ ] CP207 research (business process, business requirements)
2. [ ] Research assigned bugfix `MIRA`
3. [ ] Deep dive understanding of `MIRA` business process and app flow
4. [ ] Dockerize `MIRA`?
5. [ ] Explore `MIRA` on `Risk Monitor` as per Mas Fikri's request
6. [ ] Ask whether to create new service or not (pairing with Mas Fikri)
