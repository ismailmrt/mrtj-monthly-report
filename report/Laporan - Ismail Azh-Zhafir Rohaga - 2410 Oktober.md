# October 2024 Report

_Reported by: [Ismail Azh-Zhafir Rohaga](https://ismailrohaga.github.io)_

## Summary

Throughout October 2024, progress was made in several key areas of the project, primarily focusing on the implementation of the Monday.com- features for Martidocs.

The month began with extensive research and preparation work, including studying MERN coding practices and React state management. A major focus was placed on UI/UX development, starting with thorough analysis of Monday.com's interface and its adaptation using the Mantis template system.

The latter half of the month saw substantial progress in implementing core functionalities, particularly the app switcher feature and navigation system. Time was also dedicated to documentation, including PRD creation and component mapping, as well as resolving various technical challenges such as merge conflicts and routing issues.

## Key Achievements

1. Successfully mapped Monday.com UI components to Figma for reference, Created and organized detailed PRD documentation for Martidocs Monday features

2. Successfully integrated Mantis Figma files into the project, Completed component mapping between Mantis and Monday.com requirements, Implemented custom assets for the App switcher feature

3. Successfully implemented workspace popper functionality, Completed app switcher implementation with navigation capabilities,Established mock session key implementation for product availability, Created and implemented all required routes for workflow features

4. Successfully resolved major merge conflicts in the develop-v2 branch, Implemented proper routing structure and conflict resolution, Enhanced secure.js and reorganized app.js structure, Completed code cleanup by removing unused components and optimizing imports

5. Established organized folder structure for new implementations, Successfully implemented routes, pages, components, and sections, Maintained regular progress reporting and documentation, Completed integration of navigation hooks and components

## Detailed Daily Activities

### Tue, 01/10/24

1. 08:00 - 10:00 = Review weekly task & Research MERN coding practices
2. 10:00 - 11:00 = Research React State Management practices
3. 11:00 - 12:00 = Compare Mantis state management implementation
4. 13:00 - 15:00 = Research React DOM & Drag/drop actions
5. 15:00 - 17:00 = Timesheet

### Wed, 02/10/24

1. 08:00 - 10:00 = Review presentation materials for Friday's meeting
2. 10:00 - 11:00 = Redesign presentation deck
3. 11:00 - 12:00 = Mapping out presentation flow/materials
4. 13:00 - 15:00 = Discuss with mas Oki regarding Automations feature
5. 15:00 - 17:00 = Finalize Monday progress meeting report

### Thu, 03/10/24

1. 08:00 - 10:00 = Review Martidocs Monday Feature Requirements
2. 10:00 - 11:00 = Update presentation deck timeline
3. 11:00 - 12:00 = Discuss with mas Oki regarding Automations feature
4. 13:00 - 15:00 = Prepare weekly meeting progress report & Weekly Meeting
5. 15:00 - 17:00 = Timesheet

### Fri, 04/10/24

1. 08:00 - 10:00 = Final check of Monday progress meeting report
2. 10:00 - 11:00 = Meeting progress report with Ka Div (Pak Dian) and User (Mas Fikri)
3. 11:00 - 12:00 = Meeting progress report with Ka Div (Pak Dian) and User (Mas Fikri)
4. 13:00 - 15:00 = Write and review meeting recap
5. 15:00 - 17:00 = Coordinate with Hylmi for "Pengarsipan" features

### Mon, 07/10/24

1. 08:00 - 10:00 = Review weekly task & start mapping Monday.com UI to Figma
2. 10:00 - 11:00 = UI Listing for reference
3. 11:00 - 12:00 = Request for Mantis figma files
4. 13:00 - 15:00 = Screenshoot all Monday.com core functionality to Figma
5. 15:00 - 17:00 = Organize figma files & Timesheet

### Tue, 08/10/24

1. 08:00 - 10:00 = Download Mantis Figma files and Import to the project's Figma
2. 10:00 - 11:00 = Analyze mantis Figma files
3. 11:00 - 12:00 = Compare Mantis figma files with Monday.com site
4. 13:00 - 15:00 = Prepare weekly meeting progress report & Weekly Meeting
5. 15:00 - 17:00 = Mapping mantis figma component to the project's identified components

### Wed, 09/10/24

1. 08:00 - 10:00 = Review existing draft PRD & Usreq documents for Martidocs Monday features
2. 10:00 - 11:00 = Create clean PRD outline
3. 11:00 - 12:00 = Review Mantis components for PRD implementation
4. 13:00 - 15:00 = Generate PRD details
5. 15:00 - 17:00 = Continue working on PRD documentation and component mapping

### Thu, 10/10/24

1. 08:00 - 10:00 = Update Planner
2. 10:00 - 11:00 = Review Mantis template implementation details
3. 11:00 - 12:00 = Analyze component structure for planned features
4. 13:00 - 15:00 = Review and bug fixing for existing implementation
5. 15:00 - 17:00 = Continue debugging and implementation refinement

### Fri, 11/10/24

1. 08:00 - 10:00 = Review and analyze project codebase
2. 10:00 - 11:00 = Debug app switcher implementation issues
3. 11:00 - 12:00 = Work on component styling improvements
4. 13:00 - 15:00 = Continue component implementation and testing
5. 15:00 - 17:00 = Timesheet

### Mon, 14/10/24

1. 08:00 - 10:00 = Review weekly task
2. 10:00 - 11:00 = Review current implementation structure
3. 11:00 - 12:00 = Review Mantis template documentation
4. 13:00 - 15:00 = Work on component implementation
5. 15:00 - 17:00 = Debug and fix styling issues

### Tue, 15/10/24

1. 08:00 - 10:00 = Review component implementation progress
2. 10:00 - 11:00 = Debug asset integration issues
3. 11:00 - 12:00 = Change asset icon dir and create custom asset for App switcher
4. 13:00 - 15:00 = Prepare weekly meeting progress report & Weekly Meeting
5. 15:00 - 17:00 = Adjust custom asset styling

### Wed, 16/10/24

1. 08:00 - 10:00 = Discuss with mas Aidil & mas Zakiy regarding of session key for available products (documentflow & workflow)
2. 10:00 - 11:00 = Pull latest develop-v2 branch
3. 11:00 - 12:00 = Review code changes in latest pull
4. 13:00 - 15:00 = Work on mock session key implementation
5. 15:00 - 17:00 = Timesheet

### Thu, 17/10/24

1. 08:00 - 10:00 = Review pending merge conflicts
2. 10:00 - 11:00 = Analyze routing structure
3. 11:00 - 12:00 = Analyze routing structure
4. 13:00 - 15:00 = Merge branch develop into feat/Monday and fix routing conflict @ be-revamp repo
5. 15:00 - 17:00 = Test merged changes and fix conflicts

### Fri, 18/10/24

1. 08:00 - 10:00 = Review pending code changes and conflicts
2. 10:00 - 11:00 = Analyze merge conflicts and prepare resolution strategy
3. 11:00 - 12:00 = Resolve merge conflict and cherry pick merge hunk
4. 13:00 - 15:00 = Enhance secure.js, reorganize app.js, run npm audit fix and prettier formatting
5. 15:00 - 17:00 = Remove unused components, imports and run various enhancements

### Mon, 21/10/24

1. 08:00 - 10:00 = Review weekly task
2. 10:00 - 11:00 = Analyze current menu implementation
3. 11:00 - 12:00 = Research menu switcher practices
4. 13:00 - 15:00 = Review Mantis menu component structure
5. 15:00 - 17:00 = Menu switcher logic implementation

### Tue, 22/10/24

1. 08:00 - 10:00 = Review popper component documentation
2. 10:00 - 11:00 = Add base popper implementation
3. 11:00 - 12:00 = Debug popper positioning issues
4. 13:00 - 15:00 = Test popper interactions
5. 15:00 - 17:00 = Workspace popper implementation

### Wed, 23/10/24

1. 08:00 - 10:00 = Debug workspace popper issues
2. 10:00 - 11:00 = Finalize workspace popper implementation
3. 11:00 - 12:00 = Test popper functionality
4. 13:00 - 15:00 = Prepare weekly meeting progress report & Weekly Meeting
5. 15:00 - 17:00 = Review Implementation

### Thu, 24/10/24

1. 08:00 - 10:00 = Refactor drawer header app switcher implementation
2. 10:00 - 11:00 = Analyze drawer content default (Mantis) component implementation
3. 11:00 - 12:00 = Revert Mas Aidil implementation to use reusable default (Mantis) component
4. 13:00 - 15:00 = Resolve work routes and package-lock conflict
5. 15:00 - 17:00 = Bug fixing

### Fri, 25/10/24

1. 08:00 - 10:00 = Adjust navigation hooks and components
2. 10:00 - 11:00 = Copy default NavCollapse, NavGroup & NavItem
3. 11:00 - 12:00 = Restructure folder
4. 13:00 - 15:00 = Implement default components
5. 15:00 - 17:00 = Implement Mas Aidil component to default components

### Mon, 28/10/24

1. 08:00 - 10:00 = Review weekly task
2. 10:00 - 11:00 = Review refactored app switcher implementation
3. 11:00 - 12:00 = Rollback to first unrefactored app switcher implementation
4. 13:00 - 15:00 = Branch out from reflog and fix rollback actions
5. 15:00 - 17:00 = Bug fixing

### Tue, 29/10/24

1. 08:00 - 10:00 = Review app switcher implementation
2. 10:00 - 11:00 = Debug navigation issues
3. 11:00 - 12:00 = Test route changes
4. 13:00 - 15:00 = Prepare weekly meeting progress report & Weekly Meeting
5. 15:00 - 17:00 = Continue testing and debugging routes

### Wed, 30/10/24

1. 08:00 - 10:00 = Review app switcher implementation
2. 10:00 - 11:00 = Create all required routes for workflow features
3. 11:00 - 12:00 = Prepare folder structure
4. 13:00 - 15:00 = Implement routes, pages, components, sections
5. 15:00 - 17:00 = Add navigate on app switched to default dashboard (Naïve approach)

### Thu, 31/10/24

1. 08:00 - 10:00 = Finalize Timesheet
2. 10:00 - 11:00 = Monthly Report
3. 11:00 - 12:00 = Finalize Monthly Report
4. 13:00 - 15:00 = Review routes, pages, components, sections and app switcher implementation
5. 15:00 - 17:00 = Push all progress to remote
