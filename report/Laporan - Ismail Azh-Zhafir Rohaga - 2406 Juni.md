# June 2024 Report

_Reported by: [Ismail Azh-Zhafir Rohaga](https://ismailrohaga.github.io)_

## Summary

The focus in June 2024 was on enhancing system reliability, optimizing performance,
and fostering collaboration through detailed analysis, strategic planning, and meticulous execution.
Throughout the month, attention was devoted to the MIRA platform, where efforts primarily centered
on resolving critical issues and improving infrastructure. This included reconfiguring the MySQL
environment on WSL, adjusting ports, and integrating Docker for streamlined deployment.
Notably, the resolution of data patching for `SO Changes` event and `max_prepared_stmt_count` issue on MIRA was a key achievement.
This involved detailed benchmarking, implementing efficient solutions, and refining Docker
Compose configurations to enhance scalability and reliability.

Development initiatives extended to enhancing development environments by installing essential
tools like Flutter, Android Studio, and JetBrains Toolbox. These installations were pivotal in
supporting mobile development efforts. The integration of PERURI Digital Signature into Martidocs
represented another milestone achievement. This involved detailed study sessions, group discussions,
and the drafting of implementation plans.

## Key Achievements

### Finished data patching for `SO Changes` event on `MIRA`

- Analyzed and validated the `Risk Monitor` feature flow, its data flow and dependencies.
- Conducted thorough manual reviews and simulations to ensure accurate role functionality.
- Created patching SQL for SO changes event
- Presented the solution to user and asked for feedback

![Mira SO Changes Bug](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2406/mira-so-changes-analysis.png)
Fig. 1: Mira SO Changes Bug [Access Link](https://app.eraser.io/workspace/UcwFhmCyEAnteJHj8pyc?origin=share)

```sql
USE mira_dev_01;

SET @risk_monitor_id := NULL;
SET @dir_id := 4;
SET @div_id := 13;
SET @dep_id := 116;
SET @dep_name := "Program & Partnership Department";
SET @dep_kode := "PP";
SET @grc_officer = "rtasya";

-- use this to revert back the SO changes
-- SET @risk_monitor_id := NULL;
-- SET @dir_id := 2;
-- SET @div_id := 8;
-- SET @dep_id := 116;
-- SET @dep_name := "Program & Partnership Development Department";
-- SET @dep_kode := "PPD";
-- SET @grc_officer = "rtasya";

-- -- -- Step 1: Update mt_departement table
UPDATE mt_departemen
SET dep_nama = @dep_name,
	dep_kode = @dep_kode,
    dep_direktorat_ref = @dir_id,
    dep_divisi_ref = @div_id
WHERE dep_id = @dep_id;

-- -- -- Step 2: Update mt_app_user table
UPDATE mt_app_user
SET user_direktorat = @dir_id,
    user_divisi = @div_id
WHERE user_name = @grc_officer;

-- End of SO Changes

-- -- -- Step 3: Update mt_risk_monitor table
-- assign @risk_monitor_id variable
SELECT risk_monitor_id INTO @risk_monitor_id
FROM mt_risk_monitor
WHERE risk_monitor_status != 5
  AND dep_id = @dep_id
LIMIT 1;

UPDATE mt_risk_monitor
SET dir_id = @dir_id,
    div_id = @div_id
WHERE risk_monitor_id = @risk_monitor_id;

-- option 2, apply option 2 to adjust ALL dep's risiko list to SO Changes event
UPDATE mt_risk_monitor
SET dir_id = @dir_id,
    div_id = @div_id
WHERE dep_id = @dep_id;

-- -- -- Step 4: Update mt_risk_monitor_detail table
UPDATE mt_risk_monitor_detail
SET dir_id = @dir_id,
    div_id = @div_id
WHERE risk_monitor_id = @risk_monitor_id;

-- option 2
UPDATE mt_risk_monitor_detail
SET dir_id = @dir_id,
    div_id = @div_id
WHERE dep_id = @dep_id;

-- -- -- Step 5: Update mt_risk_monitor_mitigasi table
UPDATE mt_risk_monitor_mitigasi
SET div_id = @div_id
WHERE risk_monitor_detail_id IN (
    SELECT risk_monitor_detail_id
    FROM mt_risk_monitor_detail
    WHERE risk_monitor_id = @risk_monitor_id
);

-- option 2
UPDATE mt_risk_monitor_mitigasi
SET div_id = @div_id
WHERE risk_monitor_detail_id IN (
    SELECT risk_monitor_detail_id
    FROM mt_risk_monitor_detail
    WHERE dep_id = @dep_id
);

-- -- -- Step 6: Update mt_[type]_risiko
-- option 1 is to update based on rcsa_risiko_id
-- this will only apply to the "on going" risk monitor
UPDATE mt_rcsa_risiko
SET div_id = @div_id
WHERE rcsa_risiko_id IN (
	SELECT risiko_id
	FROM mt_risk_monitor_detail
	WHERE risk_monitor_id = @risk_monitor_id
);

-- option 2 is to update based on dep_id
-- this adjust ALL dep's risiko list to SO Changes event
UPDATE mt_rcsa_risiko
SET div_id = @div_id
WHERE dep_id = @dep_id;

-- -- -- Step 7: Update mt_[type]_doc
-- option 1 is to update based on rcsa_doc_id
-- this will only apply to the "on going" risk monitor
UPDATE mt_rcsa_doc
SET dir_id = @dir_id,
	div_id = @div_id
WHERE rcsa_doc_id IN (
	SELECT rcsa_doc_id
	FROM mt_rcsa_risiko
	WHERE rcsa_risiko_id IN (
		SELECT risiko_id
		FROM mt_risk_monitor_detail
		WHERE risk_monitor_id = @risk_monitor_id
	)
);

-- option 2 is to use WHERE dep_id
-- this will adjust ALL dep's risiko list to SO Changes event
UPDATE mt_rcsa_doc
SET dir_id = @dir_id,
	div_id = @div_id
WHERE dep_id = @dep_id;

```

### Finished bug `fixing max_prepared_stmt_count` issue on `MIRA`

- Analyzed and benchmarked the max_prepared_stmt_count issue.
- Implemented and tested a solution, reducing statement count and enhancing efficiency.
- Updated Docker Compose configuration to use environment variable interpolation.

![MIRA max_prepared_stmt_count Bug](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2406/mira-stmt-analysis.png)
Fig. 2: MIRA max_prepared_stmt_count Bug [Access Link](https://gitlab.com/-/snippets/3718300)

![MIRA max_prepared_stmt_count MR](https://gitlab.com/ismailmrt/mrtj-monthly-report/-/raw/main/Images/2406/mira-stmt-mr.png)
Fig. 3: MIRA max_prepared_stmt_count MR

### Chore branches for `MIRA` & `Dinamiq` Repo

- Set up Dockerfile and docker-compose for `MIRA` and `Dinamiq`.
- Reduced Docker image sizes using a multi-stage build approach.

### Requirement Analysis for Implementing PERURI Digital Signature Service in `Martidocs`

- Studied PERURI Digital Signature and participated in group discussions for its implementation in Martidocs.
- Drafted a comprehensive plan for integrating digital signatures into Martidocs.

## Notes

- Finished 4/6 TODO task defined in the previous month
- Finished 3/7 TODO task defined in this month

## Detailed Daily Activities

### Mon, 03/06/24

1. Revise Timesheet Report (May)
2. **`MIRA` Development Env Fix**:
   - [x] Reconfig `MySQL` `PORT` on `WSL` to `6606`
   - [x] Reconfig `MIRA` `ENV` to listen to `PORT` `6606`
   - [x] Reconfig `DBeaver` `WSL` `MySQL` connection schema
   - [x] ALTER `MIRA` `mrt_app_user` user (rismail@jakartamrt.co.id) for dev purposes
3. Draft personal reporting system for fellow "Tenaga Ahli" at MRTJ
4. Install `Flutter` on `WSL`
5. Install `Android Studio` on `WSL`
6. Risk Assessment menu - FMEA
   - [x] Read Manual Book
   - [x] Simulate `GRC Officer` role flow
   - [x] Simulate `Ka Dep` role flow
   - [x] Simulate `Admin RQMA` role flow
   - [x] Simulate `Ka Div` role flow
7. Risk Monitor menu - FMEA
   - [x] Read Manual Book
   - [x] Simulate `GRC Officer` role flow
   - [x] Simulate `Ka Dep` role flow
   - [x] Simulate `Admin RQMA` role flow
   - [x] Simulate `Ka Div` role flow
8. Install `Jetbrains Toolbox` > `Android Studio` on `Windows`
9. Timesheet

### Tue, 04/06/24

1. Draft monthly timesheet excel
2. serve `MIRA` on local
3. Pairing with `Lead` about `bugfix` task on `MIRA`
4. Drafting `bugfix` task requirement
5. Weekly meeting
6. Analyze `MIRA` flow and its data dependencies
7. Laptop Audit
8. Timesheet

### Wed, 05/06/24

1. Create new `MIRA` user (Admin RMQA, Ka Div, Ka Dep, GRC Officer) for Finance and Corporate Management Directorate > IST
2. Attempt to replicate case 1 (Division/Department move Directorate)
3. Timesheet

### Thu, 06/06/24

1. Continue working on case 1
2. Timesheet

### Fri, 07/06/24

1. Creating analysis
2. Timesheet

### Mon, 10/06/24

1. Creating analysis
2. Weekly meeting
3. Timesheet

### Tue, 11/06/24

1. Setup Dockerfile and docker-compose for MIRA
2. Analyze `dep_id` implementation on risk monitor data
3. Validate excel SO schema
4. Timesheet

### Wed, 12/06/24

1. Analyze `max_prepend_stmt_count` issue
2. Update Docker Compose to use env variable interpolation and add port config to .env.example
3. Wrote the `max_prepend_stmt_count` issue analysis to a markdown file and export it to PDF
4. Timesheet

### Thu, 13/06/24

1. Benchmark `prepend_stmt_count` on user event
2. Confirmation on tax status: TK/0 & send KK document

### Fri, 14/06/24

1. Update Docker Compose configuration restart: always
2. Benchmark `prepend_stmt_count` on user event

### Wed, 19/06/24

1. Timesheet
2. Weekly meeting
3. Clone `Dinamiq` repo
4. Read `Dinamiq` documentation
5. `MIRA` Repo chore (gitignore & docker)
6. `Dinamiq` Repo chore (docker)

### Thu, 20/06/24

1. Timesheet
2. Worked on `hf1/max-prepared-stmt-count` issue
3. Benchmark proposed solution

### Fri, 21/06/24

1. Timesheet
2. Benchmark proposed solution
3. Open merge request `hf1/max-prepared-stmt-count` to `f1/chore` then to `dev`
4. Handover `Dinamiq` to Hanif
5. Worked on `f1/chore` `Dinamiq`

### Mon, 24/06/24

1. Finishing `f1/chore` `Dinamiq`
2. Issue on docker compose, web service container unable to communicate to db container
3. Pairing with mas Nawaf setting up docker wsl2

### Tue, 25/06/24

1. Publish branch `f1/chore` `Dinamiq` for mas Nawaf to continue TODO mark
2. Weekly Meeting Green
3. Review `MIRA` data patching strategy, prepping for this week meeting with user
4. Inquire Blue Squad team member
5. Weekly Meeting Blue
6. Martidocs blue
7. Reduce Docker image size with Multi-stage build approach for
   `MIRA` @ `hf1/max-prepared-stmt-count` branch and `Dinamiq` @ `f1/chore` branch
   Reduced from 865MB down to 743MB for `MIRA`
   Reduced from 1.48GB down to 447MB for `Dinamiq`
8. Stateless container!
9. Timesheet & Monthly Report

### Wed, 26/06/24

1. Present to user for `MIRA` data patching on SO changes occurs
2. Pairing with lead for `Martidocs` `Monday` feature
3. Send screenshot to Mas Age
4. Revamp SQL Patching
5. Working on setting up centralized dev env article

### Thu, 27/06/24

1. Finalize SQL Patching
2. Study PERURI Digital Signature
3. Blue Squad Group Discussion on PERURI Digital Signature Implementation on `Martidocs`
4. Drafting PERURI Digital Signature Implementation on `Martidocs`
5. Timesheet & Monthly Report

## TODO - Previous

1. [ ] CP207 research (business process, business requirements)
2. [x] Research assigned bugfix `MIRA`
3. [ ] Deep dive understanding of `MIRA` business process and app flow
4. [x] Dockerize `MIRA`?
5. [x] Explore `MIRA` on `Risk Monitor` as per Mas Fikri's request
6. [x] Ask whether to create new service or not (pairing with Mas Fikri)

## TODO

1. [ ] Update `MIRA` readme.md file to include project overview, converting tacit to explicit knowledge
2. [x] Measure the increase in stmt_count for user actions: - Determine stmt_count when a user navigates to the risk monitor menu. - Record this data to establish a baseline.
3. [x] Benchmark the efficiency of the new solution: - Implement the new approach. - Measure stmt_count associated with user actions again. - Compare the new data with the baseline to demonstrate efficiency improvements.
4. [x] Assess `Dinamiq` interpolation env
5. [ ] Update `Martidocs` readme.md file to include project overview, converting tacit to explicit knowledge
6. [ ] `Martidocs` monday feature requirement analysis
7. [ ] Mira docker to use mysql:8.0
8. [ ] Create centralized development environment articles
